-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2020 at 08:51 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `brand_name` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `brand_name`, `category_id`, `category_name`, `created_at`, `updated_at`) VALUES
(1, 'Symphony', 1, 'Mobile phone', '2020-10-21 18:37:20', '2020-10-21 18:37:12');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `created_at`, `updated_at`) VALUES
(1, 'Mobile phone', '2020-10-21 18:36:52', '2020-10-21 18:36:52');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(11) NOT NULL,
  `color_name` varchar(255) NOT NULL,
  `color_code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `color_name`, `color_code`) VALUES
(1, 'White', NULL),
(2, 'Black', NULL),
(3, 'Red', NULL),
(4, 'Silver', NULL),
(5, 'Ash', NULL),
(6, 'Blue', NULL),
(7, 'Green', NULL),
(8, 'Pink', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `customer_reference` varchar(255) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `customer_phone` varchar(255) NOT NULL,
  `customer_address` varchar(255) NOT NULL,
  `customer_details` longtext DEFAULT NULL,
  `auth_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `customer_reference`, `customer_name`, `customer_phone`, `customer_address`, `customer_details`, `auth_id`, `created_at`, `updated_at`) VALUES
(2, 'dfa22389-c60d-4202-8c2d-fd66df6e708f', 'Customer 1', '01234567890', '62/B Lichubagan road\nGulshan 2', NULL, 1, '2020-10-27 20:04:43', '2020-10-27 20:04:43'),
(3, 'f470ae1f-35cd-4617-b675-113b9e98728d', 'Raja', '01234567890', 'Shymoly', NULL, 1, '2020-10-31 20:09:18', '2020-10-31 20:09:18');

-- --------------------------------------------------------

--
-- Table structure for table `dealers`
--

CREATE TABLE `dealers` (
  `id` int(11) NOT NULL,
  `dealer_reference` varchar(255) NOT NULL,
  `dealer_name` varchar(255) NOT NULL,
  `dealer_phone` varchar(255) NOT NULL,
  `dealer_address` varchar(255) NOT NULL,
  `dealer_details` longtext DEFAULT NULL,
  `auth_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `dealers`
--

INSERT INTO `dealers` (`id`, `dealer_reference`, `dealer_name`, `dealer_phone`, `dealer_address`, `dealer_details`, `auth_id`, `created_at`, `updated_at`) VALUES
(1, '58c36c72-b228-4069-9b0f-eed0a786e9cb', 'Dealer 1', '111', '62/B Lichubagan road\nGulshan 2', NULL, 1, '2020-10-27 20:24:20', '2020-10-23 11:40:34'),
(2, 'da02f305-34a0-4fc7-8072-324a0a01550e', 'Dealer 2', '222', '62/B Lichubagan road\nGulshan 2', NULL, 1, '2020-10-27 20:24:17', '2020-10-23 11:40:45');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ledger`
--

CREATE TABLE `ledger` (
  `id` int(11) NOT NULL,
  `ledger_reference` varchar(255) NOT NULL,
  `ps_reference` varchar(255) NOT NULL COMMENT 'one reference for total purchase/sale info',
  `object_type_id` int(11) NOT NULL COMMENT 'product or suppliers or dealers',
  `object_reference` varchar(255) NOT NULL,
  `object_name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `imei` varchar(255) DEFAULT NULL,
  `credit` varchar(255) DEFAULT NULL COMMENT 'joma/sale',
  `debit` varchar(255) DEFAULT NULL COMMENT 'khoroch/purchase',
  `stock_out` int(11) DEFAULT NULL,
  `stock_in` int(11) DEFAULT NULL,
  `unit_price` varchar(255) DEFAULT NULL,
  `dealer_price` varchar(255) NOT NULL DEFAULT '0',
  `comment` longtext DEFAULT NULL,
  `consumer_reference` varchar(255) DEFAULT NULL,
  `consumer_name` varchar(255) DEFAULT NULL,
  `date` date NOT NULL DEFAULT current_timestamp(),
  `action_name` varchar(255) DEFAULT NULL COMMENT 'payment,receive,sale,purchase (different form)',
  `auth_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `ledger`
--

INSERT INTO `ledger` (`id`, `ledger_reference`, `ps_reference`, `object_type_id`, `object_reference`, `object_name`, `sku`, `imei`, `credit`, `debit`, `stock_out`, `stock_in`, `unit_price`, `dealer_price`, `comment`, `consumer_reference`, `consumer_name`, `date`, `action_name`, `auth_id`, `created_at`, `updated_at`) VALUES
(65, '88fe05ff-d93f-4847-a4b1-dc785b5b6c9b', 'f27217fb-aae4-49aa-908d-6350339bcfea', 101, '3d3c8dc3-efeb-45a4-bd15-962b7b07c373', 'i10 White', NULL, NULL, '0', '70000.00', 0, 20, '3500', '3700', 'Supplier 1 (Qty 0; Price 161250.00Tk)', 'cf2da3bc-a99f-46c6-b31c-481fa65cdde4', 'Supplier 1', '2020-11-02', 'purchase', 1, '2020-11-01 18:15:44', '2020-11-01 18:15:44'),
(66, 'c84c2d0a-c416-4ae1-a123-4c56fa640b2b', 'f27217fb-aae4-49aa-908d-6350339bcfea', 101, '6ea62a82-7528-4ecb-85cd-32e0b8bc60e1', 'i10 Black', NULL, NULL, '0', '91250.00', 0, 25, '3650', '3800', 'Supplier 1 (Qty 20; Price 161250.00Tk)', 'cf2da3bc-a99f-46c6-b31c-481fa65cdde4', 'Supplier 1', '2020-11-02', 'purchase', 1, '2020-11-01 18:15:44', '2020-11-01 18:15:44'),
(67, 'ca7e2317-4116-4c4d-ace4-551731089961', 'f27217fb-aae4-49aa-908d-6350339bcfea', 1, 'cf2da3bc-a99f-46c6-b31c-481fa65cdde4', 'Supplier 1', NULL, NULL, '161250.00', '0', 0, 0, '0', '0', 'Qty 45; Price 161250.00Tk', NULL, NULL, '2020-11-02', 'purchase', 1, '2020-11-01 18:15:44', '2020-11-01 18:15:44'),
(68, '312c6aef-c24f-445b-a505-55611525632f', '3db84d5f-5272-4893-be33-d355708cc3cf', 101, '3d3c8dc3-efeb-45a4-bd15-962b7b07c373', 'i10 White', NULL, NULL, '0', '24150.00', 0, 7, '3450', '0', 'Supplier 1 (Qty 0; Price 24150.00Tk)', 'cf2da3bc-a99f-46c6-b31c-481fa65cdde4', 'Supplier 1', '2020-11-02', 'purchase', 1, '2020-11-01 18:16:08', '2020-11-01 18:16:08'),
(69, '9cca65ea-e554-410d-913f-9630508dc190', '3db84d5f-5272-4893-be33-d355708cc3cf', 1, 'cf2da3bc-a99f-46c6-b31c-481fa65cdde4', 'Supplier 1', NULL, NULL, '24150.00', '0', 0, 0, '0', '0', 'Qty 7; Price 24150.00Tk', NULL, NULL, '2020-11-02', 'purchase', 1, '2020-11-01 18:16:08', '2020-11-01 18:16:08'),
(70, 'e77814bf-b799-4489-8160-a8c9dbdd8f93', '0e39fd76-ff2a-4782-9a31-359a4635578f', 101, '3d3c8dc3-efeb-45a4-bd15-962b7b07c373', 'i10 White', 'SBUUOKW8', '111111', '4500.00', '0', 1, 0, '4500', '0', 'Raja (Qty 0; Price 4500.00Tk)', 'f470ae1f-35cd-4617-b675-113b9e98728d', 'Raja', '2020-11-02', 'sale', 1, '2020-11-01 18:41:36', '2020-11-01 18:41:36'),
(71, '7af7c957-bc31-472a-9c8a-6b667a15e1af', '0e39fd76-ff2a-4782-9a31-359a4635578f', 3, 'f470ae1f-35cd-4617-b675-113b9e98728d', 'Raja', NULL, NULL, '0', '4500.00', 0, 0, '0', '0', 'Qty 1; Price 4500.00Tk', NULL, NULL, '2020-11-02', 'sale', 1, '2020-11-01 18:41:36', '2020-11-01 18:41:36'),
(72, 'b66f199d-bf35-472f-9e20-c2e4fb075d58', '7a566745-ebfa-4ad4-937e-9c0c4d2234e4', 101, '3d3c8dc3-efeb-45a4-bd15-962b7b07c373', 'i10 White', 'XPIXWGMY', '321122', '4550.00', '0', 1, 0, '4550', '0', 'Customer 1 (Qty 0; Price 4550.00Tk)', 'dfa22389-c60d-4202-8c2d-fd66df6e708f', 'Customer 1', '2020-11-02', 'sale', 1, '2020-11-01 18:44:09', '2020-11-01 18:44:09'),
(73, '305f59db-5898-45ce-be80-5fac3441e2ee', '7a566745-ebfa-4ad4-937e-9c0c4d2234e4', 3, 'dfa22389-c60d-4202-8c2d-fd66df6e708f', 'Customer 1', NULL, NULL, '0', '4550.00', 0, 0, '0', '0', 'Qty 1; Price 4550.00Tk', NULL, NULL, '2020-11-02', 'sale', 1, '2020-11-01 18:44:09', '2020-11-01 18:44:09'),
(74, '7578aa52-2bee-446d-85bc-f5c59efaae34', '', 3, 'dfa22389-c60d-4202-8c2d-fd66df6e708f', 'Customer 1', NULL, NULL, '4500.00', '0', 0, 0, '0', '0', 'Received 4550.00tk from Customer 1', NULL, NULL, '2020-11-02', 'receive', 1, '2020-11-01 18:44:09', '2020-11-01 18:44:09');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(5, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(6, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(7, '2016_06_01_000004_create_oauth_clients_table', 2),
(8, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `models`
--

CREATE TABLE `models` (
  `id` int(11) NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `brand_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `models`
--

INSERT INTO `models` (`id`, `model_name`, `brand_id`, `brand_name`, `created_at`, `updated_at`) VALUES
(1, 'i10', 1, 'Symphony', '2020-10-21 18:37:55', '2020-10-21 18:37:55'),
(2, 'i20', 1, 'Symphony', '2020-10-21 18:37:55', '2020-10-21 18:37:55'),
(3, 'i30', 1, 'Symphony', '2020-10-21 18:37:55', '2020-10-21 18:37:55');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'wEQwklhMEmdBgvrdfWSK3z9DTF4pZjZgqzYOmKrY', NULL, 'http://localhost', 1, 0, 0, '2020-10-22 14:57:38', '2020-10-22 14:57:38'),
(2, NULL, 'Laravel Password Grant Client', 'lQZNlCfEHcJz5KDsUNDpKvN7kXntgBtVy4ww7sfx', 'users', 'http://localhost', 0, 1, 0, '2020-10-22 14:57:38', '2020-10-22 14:57:38');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-10-22 14:57:38', '2020-10-22 14:57:38');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `object_types`
--

CREATE TABLE `object_types` (
  `id` int(11) NOT NULL,
  `object_type_name` varchar(255) NOT NULL COMMENT '"dealer", "seller", "mobile_phone", "furniture" etc.',
  `product` int(1) NOT NULL COMMENT 'this type is product or not?'
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `object_types`
--

INSERT INTO `object_types` (`id`, `object_type_name`, `product`) VALUES
(1, 'supplier', 0),
(2, 'dealer', 0),
(3, 'customer', 0),
(101, 'mobile_phone', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `object_type_id` int(11) DEFAULT NULL,
  `object_type_name` varchar(255) DEFAULT NULL,
  `product_reference` varchar(255) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `purchase_qty` int(11) NOT NULL DEFAULT 0,
  `stock_qty` int(11) NOT NULL DEFAULT 0,
  `category_id` int(11) DEFAULT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `brand_name` varchar(255) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `model_name` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `auth_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `object_type_id`, `object_type_name`, `product_reference`, `product_name`, `purchase_qty`, `stock_qty`, `category_id`, `category_name`, `brand_id`, `brand_name`, `model_id`, `model_name`, `color`, `auth_id`, `created_at`, `updated_at`) VALUES
(18, 101, 'mobile_phone', '3d3c8dc3-efeb-45a4-bd15-962b7b07c373', 'i10 White', 27, 0, 1, 'Mobile phone', 1, 'Symphony', 1, 'i10', 'White', 1, '2020-11-01 18:44:09', '2020-10-31 17:23:03'),
(19, 101, 'mobile_phone', '6ea62a82-7528-4ecb-85cd-32e0b8bc60e1', 'i10 Black', 25, 0, 1, 'Mobile phone', 1, 'Symphony', 1, 'i10', 'Black', 1, '2020-11-01 18:15:44', '2020-10-31 17:23:10'),
(20, 101, 'mobile_phone', '1c982ae0-58a7-415d-9b93-56b266865022', 'i10 Red', 0, 0, 1, 'Mobile phone', 1, 'Symphony', 1, 'i10', 'Red', 1, '2020-10-31 17:23:17', '2020-10-31 17:23:17'),
(21, 101, 'mobile_phone', 'dc0a451f-69dd-4a02-84b8-5dbc7530018d', 'i20 Green', 0, 0, 1, 'Mobile phone', 1, 'Symphony', 2, 'i20', 'Green', 1, '2020-10-31 17:23:22', '2020-10-31 17:23:22'),
(22, 101, 'mobile_phone', '4ed959fd-9e2a-44cb-a867-73aa5d7a032b', 'i20 Silver', 0, 0, 1, 'Mobile phone', 1, 'Symphony', 2, 'i20', 'Silver', 1, '2020-10-31 17:23:27', '2020-10-31 17:23:27'),
(23, 101, 'mobile_phone', 'f6a2c1c9-f26a-4889-abb2-8e165bbea22b', 'i20 Ash', 0, 0, 1, 'Mobile phone', 1, 'Symphony', 2, 'i20', 'Ash', 1, '2020-10-31 17:23:34', '2020-10-31 17:23:34'),
(24, 101, 'mobile_phone', '0966e439-ae82-498b-80e1-234a69a2f481', 'i30 Pink', 0, 0, 1, 'Mobile phone', 1, 'Symphony', 3, 'i30', 'Pink', 1, '2020-10-31 17:23:39', '2020-10-31 17:23:39');

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` int(11) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `ledger_reference` varchar(255) NOT NULL,
  `product_reference` varchar(255) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `imei` varchar(255) NOT NULL,
  `unit_price` varchar(255) NOT NULL DEFAULT '0',
  `dealer_price` varchar(255) NOT NULL DEFAULT '0',
  `stock_status` varchar(255) NOT NULL DEFAULT 'stock' COMMENT 'stock,sold,dealer',
  `stock_dealer_reference` varchar(255) DEFAULT NULL,
  `stock_dealer_name` varchar(255) DEFAULT NULL,
  `auth_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`id`, `sku`, `ledger_reference`, `product_reference`, `product_name`, `imei`, `unit_price`, `dealer_price`, `stock_status`, `stock_dealer_reference`, `stock_dealer_name`, `auth_id`, `created_at`, `updated_at`) VALUES
(37, 'SBUUOKW8', '312c6aef-c24f-445b-a505-55611525632f', '3d3c8dc3-efeb-45a4-bd15-962b7b07c373', 'i10 White', '111111', '3450', '0', 'sold', NULL, NULL, 1, '2020-11-01 18:41:36', '2020-11-01 18:17:40'),
(38, 'XPIXWGMY', '88fe05ff-d93f-4847-a4b1-dc785b5b6c9b', '3d3c8dc3-efeb-45a4-bd15-962b7b07c373', 'i10 White', '321122', '3500', '3700', 'sold', NULL, NULL, 1, '2020-11-01 18:44:09', '2020-11-01 18:43:19');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL,
  `supplier_reference` varchar(255) NOT NULL,
  `supplier_name` varchar(255) DEFAULT NULL,
  `supplier_phone` varchar(255) DEFAULT NULL,
  `supplier_address` varchar(255) DEFAULT NULL,
  `sullpier_details` longtext DEFAULT NULL,
  `auth_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `supplier_reference`, `supplier_name`, `supplier_phone`, `supplier_address`, `sullpier_details`, `auth_id`, `created_at`, `updated_at`) VALUES
(2, 'cf2da3bc-a99f-46c6-b31c-481fa65cdde4', 'Supplier 1', '01234567890', '62/B Lichubagan road\nGulshan 2', NULL, 1, '2020-10-23 11:31:26', '2020-10-23 11:31:26'),
(3, '6a4e2fe5-e6f7-4ac6-a117-2f0d3ca77a19', 'Supplier 2', '01234567890', '62/B Lichubagan road, Gulshan 2', NULL, 1, '2020-10-23 11:32:32', '2020-10-23 11:32:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'dealer',
  `parent` int(11) DEFAULT 0 COMMENT 'Parent user id',
  `parent_role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ex: administrator/dealer',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `role`, `parent`, `parent_role`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'admin@shop.com', 'administrator', 0, NULL, NULL, '$2y$10$TISkk5c0MTAHDXn7NMGTUeyyckmZN.aIOl7w5MWcywswGOIH4hIa6', NULL, '2020-10-19 10:24:10', '2020-10-19 10:24:10'),
(2, 'Dealer', 'dealer@shop.com', 'dealer', 0, NULL, NULL, '$2y$10$pEic2UF0P7jEhadCXU01pOKAK.hwZBeZ5mck5GOadZ0ZxjArobwPC', NULL, '2020-10-20 12:33:23', '2020-10-20 12:33:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dealer_reference` (`customer_reference`);

--
-- Indexes for table `dealers`
--
ALTER TABLE `dealers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dealer_reference` (`dealer_reference`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ledger`
--
ALTER TABLE `ledger`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reference` (`ledger_reference`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `models`
--
ALTER TABLE `models`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `object_types`
--
ALTER TABLE `object_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_reference` (`product_reference`),
  ADD UNIQUE KEY `product_name` (`product_name`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `imei` (`imei`),
  ADD UNIQUE KEY `sku` (`sku`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `supplier_reference` (`supplier_reference`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `dealers`
--
ALTER TABLE `dealers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ledger`
--
ALTER TABLE `ledger`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `models`
--
ALTER TABLE `models`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `object_types`
--
ALTER TABLE `object_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
