@extends('administration.layouts.master')

@section('css') @endsection

@section('breadcrumb')
    <h1 class="page-title">Stock</h1>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <template>
                <div class="row">

                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="p-3">
                                    <h4 class="text-muted font-18 m-b-5">Add Stock</h4>
                                    {{--<p class="text-muted text-center">Get your free Admiria account now.</p>--}}

                                    <form @submit.prevent="update_status ? update() : store()" class="form-horizontal m-t-30">
                                        @csrf

                                        <div class="form-group ">
                                            <label for="model">Purchase</label>
                                            <select name="purchase" v-model="purchase" id="purchase" class="form-control form-control-sm" required>
                                                <option value="" selected>Select product...</option>
                                                <option v-for="val in purchases" :value="val.ledger_reference">@{{ val.object_name }} - (@{{ val.stock_in ? val.stock_in : 0 }} piece from @{{ val.consumer_name ? val.consumer_name : '' }})</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="imei">IMEI</label>
                                            <input ref="imei" type="text" class="form-control form-control-sm" v-model="imei" name=imei" id="imei" placeholder="IMEI">
                                        </div>

                                        <div class="form-group row m-t-20">
                                            <div class="col-12 text-right">
                                                <button class="btn btn-sm btn-secondary w-md waves-effect waves-light" type="button" @click="clear_fields()">Cancel</button>
                                                <button class="btn btn-sm btn-danger w-md waves-effect waves-light" type="submit">@{{ update_status ? 'Update' : 'Save' }}</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">

                                <div class="row mb-3">
                                    <div class="col-md-3">
                                        <input type="text" v-model="search_imei" class="form-control form-control-sm" id="search_imei" placeholder="Search by IMEI" v-on:keyup.enter="get_stocks()">
                                    </div>
                                </div>

                                <table width="100%">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th>Product</th>
                                        <th>IMEI</th>
                                        <th>SKU</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="(val, k) in filteredStocks">
                                        <td>
                                            {{--<i class="mdi mdi-pencil text-primary" style="cursor: pointer" @click="edit(val)"></i>--}}
                                        </td>
                                        <td>@{{ k + 1 }}</td>
                                        <td>@{{ val.product_name }}</td>
                                        <td>@{{ val.imei }}</td>
                                        <td>@{{ val.sku }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">
                                        <li class="page-item"  :class="pagination.prev_page_url ? '' : 'disabled'">
                                            <a @click="fetch_paginate_data(pagination.prev_page_url)" class="page-link" href="#" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </li>

                                        <li class="page-item"><a class="page-link" href="javascript:void(0)">@{{ pagination.current_page }} of @{{ pagination.last_page }}</a></li>

                                        <li class="page-item" :class="pagination.next_page_url ? '' : 'disabled'">
                                            <a @click="fetch_paginate_data(pagination.next_page_url)"  class="page-link" href="#" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>

                </div>
            </template>
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection

@section('script')
    <script src="{{asset('js/administration/stocks/index.js')}}"></script>
@endsection