@extends('administration.layouts.master')

@section('css')
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

    <style>
        .select2-selection__rendered {
            line-height: 31px !important;
        }
        .select2-container .select2-selection--single {
            height: 32px !important;
        }
        .select2-selection__arrow {
            height: 32px !important;
        }
    </style>
@endsection

@section('breadcrumb')
    <h1 class="page-title">Product Withdraw</h1>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <template>
                <div class="row">

                    <div class="col-md-6 col-lg-6 col-xl-6">
                        <div class="card m-b-20">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Product Withdraw by DSO</h4>
                                <hr>

                                <form @submit.prevent="store()" method="post">
                                    @csrf

                                    <div class="row mb-3">
                                        <div class="col-md-6">
                                            <select name="dsos" id="dsos" v-model="dso_reference" class="form-control form-control-sm select2" required>
                                                <option value="" selected>Choose DSO ...</option>
                                                <option v-for="(val, index) in dsos" :key="index" :value="val.dso_reference">
                                                    @{{ val.dso_name }} (@{{ val.dso_phone }} : @{{ val.dso_address }})
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-md-2"></div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <input type="date" title="Date" v-model="date" class="form-control form-control-sm" id="date" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <div class="col-md-6">
                                            <label for="imei">IMEI<small><em>(Set IMEI & Press Enter to add product*)</em></small></label>
                                            <input type="text" ref="imei" name="imei" v-model="imei" class="form-control form-control-sm" placeholder="Enter IMEI" @keypress.prevent.enter="addTr(imei)">
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>

                                    <table class="table-sm table-hover table-striped" width="100%">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Product</th>
                                            <th>SKU</th>
                                            <th>IMEI</th>
                                            <th width="10%"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr v-for="(tr, k) in trs">
                                            <td>@{{ k + 1 }}</td>
                                            <td> <span class="font-weight-bold" v-if="tr.product_name">@{{ tr.product_name  }}</span></td>
                                            <td> <span class="font-weight-bold">@{{ tr.sku }}</span></td>
                                            <td> <span class="font-weight-bold">@{{ tr.imei }}</span></td>
                                            <td scope="row" style="color: red; cursor: pointer">
                                                <i class="mdi mdi-close" title="Double click to delete" @dblclick="deleteTr(k, tr)"></i>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <textarea v-model="desc" name="desc" id="desc" class="form-control form-control-sm" placeholder="Enter any comments..."></textarea>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-sm btn-danger" :disabled="btn_form_submit_disabled"> Distribute</button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-6 col-xl-6">
                        <div class="card m-b-20">
                            <div class="card-body table-responsive">

                                <h4 class="mt-0 header-title">Issued Product List : @{{ dso.dso_name }} </h4>

                                <hr>
                                <table class="" width="100%">
                                    <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Date</th>
                                        <th>Product</th>
                                        <th>SKU</th>
                                        <th>IMEI</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="(val, k) in issued_list">
                                        <td>@{{ k + 1 }}</td>
                                        <td>@{{ val.issue_date }}</td>
                                        <td>@{{ val.product_name }}</td>
                                        <td>@{{ val.sku }}</td>
                                        <td>@{{ val.imei }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </template>
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection

@section('script')
    <script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('js/administration/distribution/withdraw.js')}}"></script>
@endsection