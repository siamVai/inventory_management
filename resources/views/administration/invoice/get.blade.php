@extends('administration.layouts.master')

@section('css')@endsection

@section('breadcrumb')
    <h1 class="page-title">Invoice</h1>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-20">
                        <div class="card-body">

                            <div class="row">
                                <div class="col-12">
                                    <div class="invoice-title">
                                        <h4 class="pull-right font-16"><strong>Invoice #@{{ invoice.invoice_id }}</strong></h4>
                                        <h3 class="m-t-0">
                                            <img src="{{ URL::asset('assets/images/logo.png') }}" alt="logo" height="26"/>
                                        </h3>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-6">
                                            <address>
                                                <strong>Billed To:</strong><br>
                                                @{{ invoice.consumer_name }}<br>
                                                <span v-if="invoice.object_type_id = 1">@{{ consumer.supplier_phone }}</span>
                                                <span v-if="invoice.object_type_id = 2">@{{ consumer.dealer_phone }}</span>
                                                <span v-if="invoice.object_type_id = 3">@{{ consumer.customer_phone }}</span>
                                                <span v-if="invoice.object_type_id = 4">@{{ consumer.dso_phone }}</span>
                                                <br>
                                                <span v-if="invoice.object_type_id = 1">@{{ consumer.supplier_address }}</span>
                                                <span v-if="invoice.object_type_id = 2">@{{ consumer.dealer_address }}</span>
                                                <span v-if="invoice.object_type_id = 3">@{{ consumer.customer_address }}</span>
                                                <span v-if="invoice.object_type_id = 4">@{{ consumer.dso_address }}</span>
                                            </address>
                                        </div>
                                        <div class="col-6 text-right">
                                            <address>
                                                <strong>Order Date:</strong><br>
                                                @{{ invoice.created_at | formatDate }}<br><br>
                                            </address>
                                            <address>
                                                <strong>Payment Method:</strong><br>
                                                CASH<br>
                                            </address>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="panel panel-default">
                                        <div class="">
                                            <h3 class="panel-title font-20">@{{ invoice.action_name | uppercase }} </h3>
                                        </div>
                                        <div class="">
                                            <div class="table-responsive">
                                                <table class="table-striped" width="100%">
                                                    <thead>
                                                    <tr>
                                                        <td><strong>Item</strong></td>
                                                        <td v-if="invoice.action_name == 'sale'"><strong>IMEI</strong></td>
                                                        <td class="text-center"><strong>Price</strong></td>
                                                        <td class="text-center"><strong>Quantity</strong>
                                                        </td>
                                                        <td class="text-right" width="20%"><strong>Totals</strong></td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                    <tr v-for="val in details">
                                                        <td>@{{ val.object_name }}</td>
                                                        <td v-if="val.action_name == 'sale'">@{{ val.imei }}</td>
                                                        <td class="text-center">@{{ val.unit_price }}tk</td>
                                                        <td class="text-center">
                                                            <span v-if="val.action_name == 'purchase'">@{{ val.stock_in }}</span>
                                                            <span v-if="val.action_name == 'sale'">@{{ val.stock_out }}</span>
                                                        </td>
                                                        <td class="text-right">
                                                            <span v-if="val.action_name == 'purchase'">@{{ val.debit }}tk</span>
                                                            <span v-if="val.action_name == 'sale'">@{{ val.credit }}tk</span>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td v-if="invoice.action_name == 'purchase'" colspan="2"></td>
                                                        <td v-if="invoice.action_name == 'sale'" colspan="3"></td>
                                                        <td class="text-right">Subtotal:</td>
                                                        <td class="text-right">@{{ subtotal ?? 0 }} tk</td>
                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>

                                            <div class="d-print-none mt-5">
                                                <div class="pull-right">
                                                    <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light"><i class="fa fa-print"></i></a>
                                                    {{--<a href="#" class="btn btn-primary waves-effect waves-light">Send</a>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end row -->
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection

@section('script')
    <script>
        window.invoice_id = '{{$invoice_id ?? ''}}';
    </script>
    <script src="{{asset('js/administration/invoice/get.js')}}"></script>
@endsection