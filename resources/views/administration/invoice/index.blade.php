@extends('administration.layouts.master')

@section('css')@endsection

@section('breadcrumb')
    <h1 class="page-title">Invoice List</h1>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">

                            <div class="row mb-3">
                                <div class="col-md-3">
                                    <input type="text" v-model="input_invoice" class="form-control form-control-sm" id="input_invoice" placeholder="Search by Invoice ID" v-on:keyup.enter="get_invoice()">
                                </div>
                            </div>

                            <table width="100%" class="table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Date</th>
                                    <th>Invoice</th>
                                    <th>Name</th>
                                    <th>Remarks</th>
                                    <th>Transaction Type</th>
                                    <th class="text-right">Payment Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(val, index) in filterInvoiceList">
                                    <td><a :href="'/administration/invoice/get?q='+val.invoice_id"><i class="fa fa-print"></i></a></td>
                                    <td>@{{ val.created_at }}</td>
                                    <td> <strong>#@{{ val.invoice_id }}</strong></td>
                                    <td>@{{ val.consumer_name }}</td>
                                    <td>@{{ val.comment }}</td>
                                    <td>@{{ val.action_name }}</td>
                                    <td class="text-right">@{{ val.price }} tk</td>
                                </tr>

                                </tbody>
                            </table>

                            <nav aria-label="..." style="position: center" class="mt-5">
                                <ul class="pagination">
                                    <li class="page-item" :class="pagination.prev_page_url ? '' : 'disabled'">
                                        <a @click="fetch_paginate_data(pagination.prev_page_url)" class="page-link" href="#">Previous</a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">@{{ pagination.current_page }} of @{{ pagination.last_page }}</a></li>
                                    <li class="page-item"  :class="pagination.next_page_url ? '' : 'disabled'">
                                        <a  @click="fetch_paginate_data(pagination.next_page_url)" class="page-link" href="#">Next</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div> <!-- end row -->
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection

@section('script')
    <script src="{{asset('js/administration/invoice/index.js')}}"></script>
@endsection