@extends('administration.layouts.master')

@section('css')
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

    <style>
        .select2-selection__rendered {
            line-height: 31px !important;
        }
        .select2-container .select2-selection--single {
            height: 32px !important;
        }
        .select2-selection__arrow {
            height: 32px !important;
        }
    </style>
@endsection

@section('breadcrumb')
    <h1 class="page-title">Sale</h1>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <template>
                <div class="row">

                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <div class="card m-b-20">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Sale products to Dealers/Customer</h4>
                                <hr>

                                <form @submit.prevent="store()" method="post">
                                    @csrf

                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="date">Date </label>
                                                <input type="date" title="Date" v-model="date" class="form-control form-control-sm" id="date" required>
                                            </div>
                                        </div>

                                        <div class="col-md-3"></div>
                                        <div class="col-md-2">
                                            <label for="object_type">Consumer Type</label>
                                            <select name="object_type" id="object_type" v-model="object_type" class="form-control form-control-sm" required>
                                                <option value="3">Customer</option>
                                                <option value="4">DSO</option>
                                                <option value="2">Retailer</option>
                                            </select>
                                        </div>

                                        <div class="col-md-5">
                                            <label for="object">Choose Consumer</label>
                                            <select name="object" id="object" v-model="object" :disabled="disable_input_consumer" class="form-control form-control-sm select2">
                                                <option value="" selected>Choose ...</option>
                                                <option v-if="object_type === '3'" v-for="val in objects" :value="val.customer_reference">
                                                    @{{ val.customer_name }} (@{{ val.customer_phone }} : @{{ val.customer_address }})
                                                </option>
                                                <option v-if="object_type === '4'" v-for="val in objects" :value="val.dso_reference">
                                                    @{{ val.dso_name }} (@{{ val.dso_phone }} : @{{ val.dso_address }})
                                                </option>
                                                <option v-if="object_type === '2'" v-for="val in objects" :value="val.dealer_reference">
                                                    @{{ val.dealer_name }} (@{{ val.dealer_phone }} : @{{ val.dealer_address }})
                                                </option>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="row mb-3">
                                        <div class="col-md-3">
                                            <label for="imei">Enter IMEI <small><em>(Press Enter to add product*)</em></small></label>
                                            <input type="text" ref="imei" name="imei" v-model="imei" class="form-control form-control-sm" placeholder="Enter IMEI" @keypress.prevent.enter="addTr(imei)">
                                        </div>
                                        <div class="col-md-1"></div>

                                        <div class="col-md-3">
                                            <label for="consumer_name">Consumer</label>
                                            <input type="text" id="consumer_name" name="consumer_name" v-model="consumer_name" :readonly="readonly_input_consumer_fields" class="form-control form-control-sm" placeholder="Customer/DSO/Retailer name">
                                        </div>
                                        <div class="col-md-2">
                                            <label for="consumer_phone">Consumer Phone</label>
                                            <input type="text" id="consumer_phone" name="consumer_phone" v-model="consumer_phone" :readonly="readonly_input_consumer_fields" class="form-control form-control-sm" placeholder="017XXXXXXXX">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="consumer_address">Consumer Address</label>
                                            <input type="text" id="consumer_address" name="consumer_address" v-model="consumer_address" :readonly="readonly_input_consumer_fields" class="form-control form-control-sm" placeholder="House, Road, Area, City">
                                        </div>

                                    </div>

                                    <table class="table-sm table-hover table-striped" width="100%">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th>Product</th>
                                            <th>QTY</th>
                                            <th>IMEI</th>
                                            <th class="text-right">Unit Price</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr v-for="(tr, k) in trs">
                                            <td scope="row" style="color: red; cursor: pointer">
                                                <i class="mdi mdi-close" title="Double click to delete" @dblclick="deleteTr(k, tr)"></i>
                                            </td>
                                            <td>@{{ k + 1 }}</td>
                                            <td>
                                                <span v-if="tr.brand_name">@{{ tr.brand_name }}</span>
                                                <span v-if="tr.product_name">@{{ tr.product_name }}</span>
                                            </td>
                                            <td>@{{ tr.qty }}</td>
                                            <td>
                                                <span class="font-weight-bold">@{{ tr.imei }}</span>
                                            </td>
                                            <td width="20%">
                                                <input v-model="tr.price" title="Unit Price" class="form-control form-control-sm text-right" type="text" placeholder="Enter price" @keyup="calculateTotal()" required/>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="4"></td>
                                            <td style="text-align: right;">Sub total : </td>
                                            <td  width="20%">
                                                <input readonly class="form-control form-control-sm text-right" type="text" v-model="subtotal" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"></td>
                                            <td style="text-align: right;">Discount : </td>
                                            <td  width="20%">
                                                <input class="form-control form-control-sm text-right" type="text" v-model="discount"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"></td>
                                            <td style="text-align: right;">Special Discount : </td>
                                            <td  width="20%">
                                                <input class="form-control form-control-sm text-right" type="text" v-model="special_discount"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"></td>
                                            <td style="text-align: right;">Paid : </td>
                                            <td  width="20%">
                                                <input class="form-control form-control-sm text-right" type="text" :disabled="disable_input_paid" v-model="paid" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"></td>
                                            <td style="text-align: right;">Due : </td>
                                            <td  width="20%">
                                                <input readonly class="form-control form-control-sm text-right" type="text" v-model="due" />
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <textarea v-model="desc" name="desc" id="desc" class="form-control form-control-sm" placeholder="Enter any comments..."></textarea>
                                        </div>
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4"></div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-sm btn-danger" :disabled="btn_form_submit_disabled">Sale Confirm</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </template>
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection

@section('script')
    <script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('js/administration/sale/index2.js')}}"></script>
@endsection