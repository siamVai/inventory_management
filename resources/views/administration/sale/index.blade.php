@extends('administration.layouts.master')

@section('css')
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

    <style>
        .select2-selection__rendered {
            line-height: 31px !important;
        }
        .select2-container .select2-selection--single {
            height: 32px !important;
        }
        .select2-selection__arrow {
            height: 32px !important;
        }
    </style>
@endsection

@section('breadcrumb')
    <h1 class="page-title">Sale</h1>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <template>
                <div class="row">

                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <div class="card m-b-20">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Sale products to Dealers/Customer</h4>
                                <p></p>

                                <form @submit.prevent="store()" method="post">
                                    @csrf

                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-2">
                                            <label for="object_type">Consumer Type</label>
                                            <select name="object_type" id="object_type" v-model="object_type" class="form-control form-control-sm" required>
                                                <option value="3">Customer</option>
                                                <option value="2">Dealer</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="object">Consumer</label>
                                            <select name="object" id="object" v-model="object" class="form-control form-control-sm select2" required>
                                                <option value="" selected>Choose ...</option>
                                                <option v-if="object_type == '3'" v-for="val in objects" :value="val.customer_reference">
                                                    @{{ val.customer_name }} (@{{ val.customer_phone }} : @{{ val.customer_address }})
                                                </option>
                                                <option v-else-if="object_type == '2'" v-for="val in objects" :value="val.dealer_reference">
                                                    @{{ val.dealer_name }} (@{{ val.dealer_phone }} : @{{ val.dealer_address }})
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="date">Date </label>
                                                <input type="date" title="Date" v-model="date" class="form-control form-control-sm" id="date" required>
                                            </div>
                                        </div>
                                    </div>

                                    <table class="table-sm table-hover table-striped" width="100%">
                                        <thead>
                                        <tr>
                                            <th style="width: 20%">Item </th>
                                            <th style="width: 20%">IMEI </th>
                                            <th style="width: 25%">Description </th>
                                            <th>Qty </th>
                                            <th>Price</th>
                                            <th style="text-align: right;">Total</th>
                                            <th style=""></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr v-for="(tr, k) in trs">

                                            <td>
                                                <select v-model="tr.product_reference" id="products" class="form-control form-control-sm" style="width: 100%" required @change="get_imeis(k, tr.product_reference)">
                                                    <option value="" selected>Select product...</option>
                                                    <option v-for="(val, index) in products" :value="val.product_reference"> @{{val.product_name}} (@{{ val.stock_qty }}) </option>
                                                </select>
                                            </td>
                                            <td>
                                                <select v-model="tr.imei" id="imei" class="form-control form-control-sm" style="width: 100%" required>
                                                    <option value="" selected>Select IMEI...</option>
                                                    <option v-for="(val, index) in tr.imeis" :key="index" :value="val.imei"> @{{val.imei}} </option>
                                                </select>
                                            </td>

                                            <td><input v-model="tr.desc" class="form-control form-control-sm" type="text"/></td>
                                            <td><input v-model="tr.qty" min="1" class="form-control form-control-sm" type="number" @change="calculateLineTotal(tr)"/></td>
                                            <td><input v-model="tr.price" min="1" class="form-control form-control-sm" type="text" @change="calculateLineTotal(tr)"/></td>
                                            <td style="text-align: right;"><input v-model="tr.line_total" min="1" class="form-control form-control-sm text-right" type="text"/></td>
                                            <td scope="row" style="color: red; cursor: pointer">
                                                <i class="mdi mdi-close" title="Double click to delete" @dblclick="deleteTr(k, tr)"></i>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <button type="button" class="btn btn-sm btn-outline-secondary" @click="addTr()">+ New </button>
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td style="text-align: right;">Sub total : </td>
                                            <td>
                                                <input readonly class="form-control form-control-sm text-right" type="text" v-model="subtotal" />
                                            </td>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td style="text-align: right;">Paid : </td>
                                            <td>
                                                <input class="form-control form-control-sm text-right" type="text" v-model="paid" />
                                            </td>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td style="text-align: right;">Due : </td>
                                            <td>
                                                <input readonly class="form-control form-control-sm text-right" type="text" v-model="due" />
                                            </td>
                                            <th></th>
                                        </tr>

                                        </tbody>
                                    </table>

                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-12">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<input type="checkbox" id="payment_status" name="payment_status" value="paid">--}}
                                                {{--<label for="payment_status">Paid ? </label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="row">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-sm btn-danger" :disabled="btn_form_submit_disabled">Sale Confirm</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </template>
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection

@section('script')
    <script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('js/administration/sale/index.js')}}"></script>
@endsection