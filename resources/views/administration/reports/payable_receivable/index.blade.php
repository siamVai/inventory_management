@extends('administration.layouts.master')

@section('css')@endsection

@section('breadcrumb')
    <h1 class="page-title">Payable/Receivable</h1>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <h6 class="text-danger">Payable</h6>
                                        </div>
                                    </div>

                                    <table width="100%">
                                        <thead>
                                        <tr>
                                            <th>Consumer type</th>
                                            <th>Consumer Name</th>
                                            <th>Amount</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr v-for="val in filterReport" v-if="val.amount > 0">
                                            <td>
                                                <span v-if="val.object_type_id == '1'">Supplier</span>
                                                <span v-if="val.object_type_id == '2'">Retailer</span>
                                                <span v-if="val.object_type_id == '3'">Customer</span>
                                                <span v-if="val.object_type_id == '4'">DSO</span>
                                            </td>
                                            <td>
                                                <a v-if="val.object_type_id == '1'" :href="'/administration/reports/ledger/supplier?supplier='+val.object_reference">@{{ val.object_name }}</a>
                                                <a v-if="val.object_type_id == '2'" :href="'/administration/reports/ledger/retailer?retailer='+val.object_reference">@{{ val.object_name }}</a>
                                                <a v-if="val.object_type_id == '3'" :href="'/administration/reports/ledger/customer?customer='+val.object_reference">@{{ val.object_name }}</a>
                                                <a v-if="val.object_type_id == '4'" :href="'/administration/reports/ledger/dso?dso='+val.object_reference">@{{ val.object_name }}</a>
                                            </td>
                                            <td>@{{ val.amount ?? 0 }}TK</td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        </tfoot>
                                    </table>

                                </div>
                                <div class="col-md-6">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <h6 class="text-success">Receivable</h6>
                                        </div>
                                    </div>

                                    <table width="100%">
                                        <thead>
                                        <tr>
                                            <th>Consumer type</th>
                                            <th>Consumer Name</th>
                                            <th>Amount</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr v-for="val in filterReport" v-if="val.amount < 0">
                                            <td>
                                                <span v-if="val.object_type_id == '1'">Supplier</span>
                                                <span v-if="val.object_type_id == '2'">Retailer</span>
                                                <span v-if="val.object_type_id == '3'">Customer</span>
                                                <span v-if="val.object_type_id == '4'">DSO</span>
                                            </td>
                                            <td>
                                                <a v-if="val.object_type_id == '1'" :href="'/administration/reports/ledger/supplier?supplier='+val.object_reference">@{{ val.object_name }}</a>
                                                <a v-if="val.object_type_id == '2'" :href="'/administration/reports/ledger/retailer?retailer='+val.object_reference">@{{ val.object_name }}</a>
                                                <a v-if="val.object_type_id == '3'" :href="'/administration/reports/ledger/customer?customer='+val.object_reference">@{{ val.object_name }}</a>
                                                <a v-if="val.object_type_id == '4'" :href="'/administration/reports/ledger/dso?dso='+val.object_reference">@{{ val.object_name }}</a>
                                            </td>
                                            <td>@{{ val.amount ?? 0 }}TK</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item"  :class="pagination.prev_page_url ? '' : 'disabled'">
                                        <a @click="fetch_paginate_data(pagination.prev_page_url)" class="page-link" href="#" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                    </li>

                                    <li class="page-item"><a class="page-link" href="javascript:void(0)">@{{ pagination.current_page }} of @{{ pagination.last_page }}</a></li>

                                    <li class="page-item" :class="pagination.next_page_url ? '' : 'disabled'">
                                        <a @click="fetch_paginate_data(pagination.next_page_url)"  class="page-link" href="#" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection

@section('script')
    <script src="{{asset('js/administration/reports/payable_receivable/index.js')}}"></script>
@endsection