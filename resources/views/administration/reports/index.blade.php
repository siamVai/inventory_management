@extends('administration.layouts.master')

@section('css')@endsection

@section('breadcrumb')
    <h1 class="page-title">Reports</h1>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="container-fluid">
            {{--<template>--}}
            <div class="row">
                <div class="col-md-6 col-lg-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white">
                        <a href="{{route('ad.reports.sale')}}">
                            <span class="mini-stat-icon bg-primary"><i class="mdi mdi-cart-outline"></i></span>
                            <div class="mini-stat-info text-right">
                                <span class="counter text-primary">Sales</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white">
                        <a href="{{route('ad.reports.purchase')}}">
                            <span class="mini-stat-icon bg-success"><i class="mdi mdi-currency-usd"></i></span>
                            <div class="mini-stat-info text-right">
                                <span class="counter text-success">Purchase</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white">
                        <a href="{{route('ad.reports.cashbook')}}">
                            <span class="mini-stat-icon bg-warning"><i class="mdi mdi-cube-outline"></i></span>
                            <div class="mini-stat-info text-right">
                                <span class="counter text-warning">Cashbook</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#modal_report_ledger">
                            <span class="mini-stat-icon bg-pink"><i class="mdi mdi-currency-btc"></i></span>
                            <div class="mini-stat-info text-right">
                                <span class="counter text-pink">Ledger</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-lg-3 col-xl-3">
                    <div class="mini-stat clearfix bg-white">
                        <a href="{{route('ad.reports.stock')}}">
                            <span class="mini-stat-icon bg-purple"><i class="mdi mdi-buffer"></i></span>
                            <div class="mini-stat-info text-right">
                                <span class="counter text-purple">Stock</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-xl-3">
                    <div class="mini-stat clearfix bg-white">
                        <a href="{{route('ad.reports.payable-receivable')}}">
                            <span class="mini-stat-icon bg-purple"><i class="mdi mdi-buffer"></i></span>
                            <div class="mini-stat-info text-right">
                                <span class="counter text-purple">Pay/Receive</span>
                                Payable/Receivable
                            </div>
                        </a>
                    </div>
                </div>

            </div>
            {{--</template>--}}
        </div>
    </div>
@endsection

@section('script')@endsection