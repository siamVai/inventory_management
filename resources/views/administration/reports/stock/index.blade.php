@extends('administration.layouts.master')

@section('css')@endsection

@section('breadcrumb')
    <h1 class="page-title">Stock</h1>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <table width="100%" class="table-striped">
                                <thead>
                                <tr class="text-right">
                                    <th class="text-left">Name of Item</th>
                                    <th>Purchase Qty</th>
                                    <th>Sale Qty</th>
                                    <th>Current stock</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="val in stocks"  class="text-right">
                                        <td class="text-left"><a href="#">@{{ val.product_name }}</a></td>
                                        <td>@{{ val.total_purchased }}</td>
                                        <td>@{{ val.total_sold }}</td>
                                        <td>@{{ val.total_stock }} <small v-if="val.total_returned > 0">( returned : @{{ val.total_returned }})</small></td>
                                    </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection

@section('script')
    <script>
        window.dealer_reference = '{{$dealer_reference ?? ''}}';
    </script>
    <script src="{{asset('js/administration/reports/stock/index.js')}}"></script>
@endsection