@extends('administration.layouts.master')

@section('css')@endsection

@section('breadcrumb')
    <h1 class="page-title">Ledger</h1>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="profile-widget ">
                                <h5>@{{ product.product_name }}</h5>
                                {{--<span><i class="fa fa-mobile-phone mr-2"></i> @{{ product.product_phone }}</span>--}}
                                {{--<br>--}}
                                {{--<span><i class="fa fa-map-marker mr-2"></i> @{{ product.product_address }}</span>--}}
                                {{--<br>--}}
                                <span>@{{ product.product_details }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <table width="100%" class="table-striped">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>product Name</th>
                                    <th>Remarks</th>
                                    <th class="text-success text-right">Credit/Joma</th>
                                    <th class="text-danger text-right">Debit/Khoroch</th>
                                    {{--<th class="text-right">Extra</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(val, index) in ledger">
                                    <td>@{{ val.date }}</td>
                                    <td>@{{ val.object_name }}</td>
                                    <td>@{{ val.comment }}</td>
                                    <td class="text-success text-right">@{{ val.credit }} tk</td>
                                    <td class="text-danger text-right">@{{ val.debit }} tk</td>
                                    {{--<td class="text-right">--}}
                                        {{--<span> @{{ (val.credit - val.debit).toFixed(2) }} tk </span>--}}
                                    {{--</td>--}}
                                </tr>

                                <tr>
                                    <th colspan="5" class="text-center"> - </th>
                                </tr>

                                <tr>
                                    <th>TOTAL</th>
                                    <th colspan="2"></th>
                                    <th class="text-right">@{{ total_credit.toFixed(2) }} tk</th>
                                    <th class="text-right">@{{ total_debit.toFixed(2) }} tk</th>
                                </tr>

                                <tr>
                                    <th>Extra</th>
                                    <th colspan="3"></th>
                                    <th class="text-right" :class="(total_credit - total_debit) > 0 ? 'text-success' : 'text-danger'">@{{ (total_credit - total_debit).toFixed(2) }} tk</th>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection

@section('script')
    <script>
        window.product_reference = '{{$product_reference ?? ''}}';
    </script>
    <script src="{{asset('js/administration/reports/ledger/product.js')}}"></script>
@endsection