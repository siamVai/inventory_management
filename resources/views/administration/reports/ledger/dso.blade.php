@extends('administration.layouts.master')

@section('css')@endsection

@section('breadcrumb')
    <h1 class="page-title">Ledger</h1>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="profile-widget ">
                                <h5>@{{ dso.dso_name }}</h5>
                                <span><i class="fa fa-mobile-phone mr-2"></i> @{{ dso.dso_phone }}</span>
                                <br>
                                <span><i class="fa fa-map-marker mr-2"></i> @{{ dso.dso_address }}</span>
                                <br>
                                <span>@{{ dso.dso_details }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <table width="100%" class="table-striped">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Customer Name</th>
                                    <th>Remarks</th>
                                    <th class="text-success text-right">Credit/Joma/Received</th>
                                    <th class="text-danger text-right">Debit/Khoroch/Receivable</th>
                                    <th class="text-right">Extra</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(val, index) in ledger">
                                    <td>@{{ val.date }}</td>
                                    <td>@{{ val.object_name }}</td>
                                    <td>@{{ val.comment }}</td>
                                    <td class="text-success text-right">@{{ val.credit }} tk</td>
                                    <td class="text-danger text-right">@{{ val.debit }} tk</td>
                                    <td class="text-right">

                                        <span> @{{ (val.credit - val.debit).toFixed(2) }} tk </span>
                                        {{--<span v-else-if="index  == 1"> @{{ (val.debit - val.credit + (ledger[index - 1].debit -  ledger[index - 1].credit)).toFixed(2) }} tk </span>--}}
                                        {{--<span v-else-if="index  > 1">--}}
                                        {{--@{{ (val.debit - val.credit + (ledger[index - 1].debit -  ledger[index - 1].credit)).toFixed(2) }} tk--}}
                                        {{--</span>--}}
                                    </td>
                                </tr>

                                <tr>
                                    <th colspan="6" class="text-center"> - </th>
                                </tr>

                                <tr>
                                    <th colspan="3"></th>
                                    <th class="text-right">@{{ total_credit.toFixed(2) }} tk</th>
                                    <th class="text-right">@{{ total_debit.toFixed(2) }} tk</th>
                                    <th class="text-right" :class="(total_credit - total_debit) > 0 ? 'text-danger' : 'text-success'">@{{ (total_credit - total_debit).toFixed(2) }} tk</th>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection

@section('script')
    <script>
        window.dso_reference = '{{$dso_reference ?? ''}}';
    </script>
    <script src="{{asset('js/administration/reports/ledger/dso.js')}}"></script>
@endsection