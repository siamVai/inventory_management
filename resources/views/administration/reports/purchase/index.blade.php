@extends('administration.layouts.master')

@section('css')@endsection

@section('breadcrumb')
    <h1 class="page-title">Purchase</h1>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <table width="100%" class="table-striped">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Name</th>
                                    <th>Remarks</th>
                                    <th class="text-danger text-right">Price</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(val, index) in purchase">
                                    <td>@{{ val.date }}</td>
                                    <td>@{{ val.object_name }}</td>
                                    <td>@{{ val.comment }}</td>
                                    <td class="text-danger text-right">@{{ val.debit }} tk</td>
                                </tr>

                                <tr>
                                    <th colspan="4" class="text-center"> - </th>
                                </tr>

                                <tr>
                                    <th colspan="3"></th>
                                    <th class="text-right">@{{ total_debit.toFixed(2) }} tk</th>
                                </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection

@section('script')
    <script src="{{asset('js/administration/reports/purchase/index.js')}}"></script>
@endsection