@extends('administration.layouts.master')

@section('css')@endsection

@section('breadcrumb')
    <h1 class="page-title">Cashbook</h1>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <table width="100%" class="table-striped">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Name</th>
                                    <th>Remarks</th>
                                    <th class="text-success text-right">Credit/Joma</th>
                                    <th class="text-danger text-right">Debit/Khoroch</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(val, index) in cashbook">
                                    <td>@{{ val.date }}</td>
                                    <td>@{{ val.object_name }}</td>
                                    <td>@{{ val.comment }}</td>
                                    <td class="text-success text-right">@{{ val.credit }} tk</td>
                                    <td class="text-danger text-right">@{{ val.debit }} tk</td>
                                </tr>

                                <tr>
                                    <th colspan="5" class="text-center"> - </th>
                                </tr>

                                <tr>
                                    <th colspan="3"></th>
                                    <th class="text-right">@{{ total_credit.toFixed(2) }} tk</th>
                                    <th class="text-right">@{{ total_debit.toFixed(2) }} tk</th>
                                </tr>
                                <tr>
                                    <th colspan="4"></th>
                                    <th class="text-right" :class="(total_credit - total_debit) > 0 ? 'text-success' : 'text-danger'">@{{ (total_credit - total_debit).toFixed(2) }} tk</th>
                                </tr>

                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection

@section('script')
    <script>
        window.dealer_reference = '{{$dealer_reference ?? ''}}';
    </script>
    <script src="{{asset('js/administration/reports/cashbook/index.js')}}"></script>
@endsection