<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>@yield('title') Inventory Management </title>
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('administration.layouts.head')
</head>
<body class="fixed-left">
<!-- Loader -->
<div id="preloader"><div id="status"><div class="spinner"></div></div></div>

@include('administration.layouts.modals')

<div id="wrapper">
    <div id="app">
    @include('administration.layouts.leftbar')
    <!-- Start right Content here -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                @include('administration.layouts.topbar')
                @yield('content')
            </div>
            @include('administration.layouts.footer')
        </div>
    </div>
</div>
@include('administration.layouts.footer-script')
</body>
</html>