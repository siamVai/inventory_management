<!-- Ledger modal -->
<div id="modal_report_ledger" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_report_ledger" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">Search Ledger</h5>
                {{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
            </div>
            <div class="modal-body">
                @php
                    $suppliers = \App\Services\Resource::suppliers();
                    $retailers = \App\Services\Resource::retailers();
                    $customers = \App\Services\Resource::customers();
                    $dsos = \App\Services\Resource::dsos();
                    $products = \App\Services\Resource::products();
                @endphp

                <form action="{{route('ad.reports.ledger.supplier')}}" method="get">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-10">
                            <select name="supplier" id="supplier" class="form-control form-control-sm">
                                <option value="">Select supplier...</option>
                                @if(count($suppliers) > 0)
                                    @foreach($suppliers as $val)
                                        <option value="{{$val->supplier_reference ?? ''}}">{{$val->supplier_name ?? ''}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-sm btn-dark pull-right"><i class="mdi mdi-magnify"></i></button>
                        </div>
                    </div>
                </form>

                <form action="{{route('ad.reports.ledger.dso')}}" method="get">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-10">
                            <select name="dso" id="dso" class="form-control form-control-sm">
                                <option value="">Select DSO...</option>
                                @if(count($dsos) > 0)
                                    @foreach($dsos as $val)
                                        <option value="{{$val->dso_reference ?? ''}}">{{$val->dso_name ?? ''}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-sm btn-dark pull-right"><i class="mdi mdi-magnify"></i></button>
                        </div>
                    </div>
                </form>

                <form action="{{route('ad.reports.ledger.retailer')}}" method="get">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-10">
                            <select name="retailer" id="retailer" class="form-control form-control-sm">
                                <option value="">Select retailer...</option>
                                @if(count($retailers) > 0)
                                    @foreach($retailers as $val)
                                        <option value="{{$val->dealer_reference ?? ''}}">{{$val->dealer_name ?? ''}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-sm btn-dark pull-right"><i class="mdi mdi-magnify"></i></button>
                        </div>
                    </div>
                </form>

                <form action="{{route('ad.reports.ledger.customer')}}" method="get">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-10">
                            <select name="customer" id="customer" class="form-control form-control-sm">
                                <option value="">Select customer...</option>
                                @if(count($customers) > 0)
                                    @foreach($customers as $val)
                                        <option value="{{$val->customer_reference ?? ''}}">{{$val->customer_name ?? ''}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-sm btn-dark pull-right"><i class="mdi mdi-magnify"></i></button>
                        </div>
                    </div>
                </form>

                <form action="{{route('ad.reports.ledger.product')}}" method="get">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-10">
                            <select name="product" id="product" class="form-control form-control-sm">
                                <option value="">Select product...</option>
                                @if(count($products) > 0)
                                    @foreach($products as $val)
                                        <option value="{{$val->product_reference ?? ''}}">{{$val->product_name ?? ''}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-sm btn-dark pull-right"><i class="mdi mdi-magnify"></i></button>
                        </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sm waves-effect" data-dismiss="modal">Close</button>
                {{--<button type="button" class="btn btn-primary btn-sm waves-effect waves-light">Save changes</button>--}}
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->