<footer class="footer">
  ©  {{ date("Y",strtotime("-1 year")) }} - {{date('Y')}} DataFleep <span class="text-muted hidden-xs-down pull-right">ADMINISTRATION</span>
</footer>