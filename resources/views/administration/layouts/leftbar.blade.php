<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">

<!-- LOGO -->
<div class="topbar-left">
    <div class="">
        <!--<a href="index" class="logo text-center">Admiria</a>-->
        <a href="{{route('administration.home')}}" class="logo"><img src="{{ URL::asset('assets/images/logo-sm.png') }}" height="36" alt="logo"></a>
    </div>
</div>
<div class="sidebar-inner slimscrollleft">
    <div id="sidebar-menu">
        <ul>
            <li class="menu-title">Main </li>
            <li>
                <a href="{{route('administration.home')}}" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span>  Dashboard </span> </a>
            </li>
            <li>
                <a href="{{route('administration.purchase')}}" class="waves-effect"><i class="mdi mdi-shopping"></i><span> Purchase </span></a>
            </li>
            <li>
                <a href="{{route('administration.sale')}}" class="waves-effect"><i class="mdi mdi-cube-outline"></i><span> Sale </span></a>
            </li>

            <li>
                <a href="{{route('administration.receive')}}" class="waves-effect"><i class="mdi mdi-cash-multiple"></i><span> Receive Cash </span></a>
            </li>
            <li>
                <a href="{{route('administration.payment')}}" class="waves-effect"><i class="mdi mdi-cash"></i><span> Payment </span></a>
            </li>

            <li>
                <a href="{{route('administration.invoice')}}" class="waves-effect"><i class="mdi mdi-file-document"></i><span> INVOICEs </span></a>
            </li>

            <li>
                <a href="{{route('administration.stock')}}" class="waves-effect"><i class="mdi mdi-stocking"></i><span> Inventory / Stock </span></a>
            </li>

            <li class="menu-title">Issue/Withdraw</li>
            <li>
                <a href="{{route('administration.distribution.issue')}}" class="waves-effect"><i class="mdi mdi-stocking"></i><span> Product Issue </span></a>
            </li>
            <li>
                <a href="{{route('administration.distribution.withdraw')}}" class="waves-effect"><i class="mdi mdi-stocking"></i><span> Product Withdraw </span></a>
            </li>

            <li class="menu-title">Return</li>
            <li>
                <a href="{{route('administration.return.sale')}}" class="waves-effect"><i class="mdi mdi-keyboard-return"></i><span> Return Product </span></a>
            </li>

            <li>
                <a href="{{route('administration.return.purchase')}}" class="waves-effect"><i class="mdi mdi-keyboard-return"></i><span> Return Purchase </span></a>
            </li>

            <li class="menu-title">Reports</li>
            <li>
                <a href="{{route('ad.reports')}}" class="waves-effect"><i class="mdi mdi-note"></i><span> Reports</span></a>
            </li>

            {{--<li class="has_sub">--}}
                {{--<a href="{{route('ad.reports')}}" class="waves-effect"><i class="mdi mdi-email-outline"></i><span> Reports <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span> </span></a>--}}
                {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="{{route('ad.reports.stock')}}">Stock</a></li>--}}
                    {{--<li><a href="{{route('ad.reports.sale')}}">Sale</a></li>--}}
                    {{--<li><a href="{{route('ad.reports.purchase')}}">Purchase</a></li>--}}
                    {{--<li><a href="{{route('ad.reports.cashbook')}}">Cashbook</a></li>--}}
                    {{--<li><a href="javascript:void(0);" data-toggle="modal" data-target="#modal_report_ledger">Ledger</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            <li class="menu-title">Add</li>
            <li>
                <a href="{{route('administration.product.models')}}" class="waves-effect"><i class="mdi mdi-dots-horizontal"></i><span> Product Models</span></a>
            </li>
            <li>
                <a href="{{route('administration.products')}}" class="waves-effect"><i class="mdi mdi-cellphone"></i><span> Products </span></a>
            </li>

            <li class="menu-title">Consumers</li>

            <li>
                <a href="{{route('administration.suppliers')}}" class="waves-effect"><i class="mdi mdi-import"></i><span> Suppliers </span></a>
            </li>
            <li>
                <a href="{{route('administration.retailers')}}" class="waves-effect"><i class="mdi mdi-export"></i><span> Retailer </span></a>
            </li>
            <li>
                <a href="{{route('administration.dso')}}" class="waves-effect"><i class="mdi mdi-account-check"></i><span> DSO </span></a>
            </li>
            <li>
                <a href="{{route('administration.customers')}}" class="waves-effect"><i class="mdi mdi-account-box"></i><span> Customers </span></a>
            </li>

            <li class="menu-title">Logout</li>
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="dripicons-exit"></i> <span>{{ __('Logout') }}</span>
                </a>
                {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">--}}
                    {{--@csrf--}}
                {{--</form>--}}
            </li>
        </ul>
    </div>
    <div class="clearfix"></div>
</div> <!-- end sidebarinner -->
</div>
<!-- Left Sidebar End -->
