<link rel="shortcut icon" href="{{ URL::asset('assets/images/favicon.ico') }}">
@yield('css')

<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
<script src="{{asset('js/axios.js')}}"></script>
<script src="{{asset('js/moment.js')}}"></script>

<!-- Alertify css -->
<link href="{{ URL::asset('assets/plugins/alertify/css/alertify.css') }}" rel="stylesheet" type="text/css">

<!-- Basic Css files -->
<link href="{{ URL::asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('assets/css/style.css') }}" rel="stylesheet" type="text/css">
