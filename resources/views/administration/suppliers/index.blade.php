@extends('administration.layouts.master')

@section('css') @endsection

@section('breadcrumb')
    <h1 class="page-title">Suppliers</h1>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <template>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">

                                <div class="p-3">
                                    <h4 class="text-muted font-18 m-b-5">Add Suppliers</h4>
                                    {{--<p class="text-muted text-center">Get your free Admiria account now.</p>--}}

                                    <form @submit.prevent="update_status ? update() : store()" class="form-horizontal m-t-30">
                                        @csrf

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="supplier_name">Name</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control form-control-sm" v-model="supplier_name" name=supplier_name" id="supplier_name" placeholder="Supplier name">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="supplier_phone">Phone</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control form-control-sm" v-model="supplier_phone" name=supplier_phone" id="supplier_phone" placeholder="Supplier phone">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="supplier_address">Address</label>
                                            </div>
                                            <div class="col-md-8">
                                                <textarea v-model="supplier_address" name="supplier_address" id="supplier_address" class="form-control form-control"></textarea>
                                            </div>
                                        </div>


                                        <div class="form-group row m-t-20">
                                            <div class="col-12 text-right">
                                                <button class="btn btn-sm btn-secondary w-md waves-effect waves-light" type="button" @click="clear_fields()">Cancel</button>
                                                <button class="btn btn-sm btn-danger w-md waves-effect waves-light" type="submit">@{{ update_status ? 'Update' : 'Save' }}</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <table width="100%">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Supplier Name</th>
                                        <th>Phone</th>
                                        <th>Address</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="val in suppliers">
                                        <td>
                                            <i class="mdi mdi-pencil text-primary" style="cursor: pointer" @click="edit()"></i>
                                            <i class="mdi mdi-close text-danger" style="cursor: pointer" @click="remove()"></i>
                                        </td>
                                        <td>@{{ val.supplier_name }}</td>
                                        <td>@{{ val.supplier_phone }}</td>
                                        <td>@{{ val.supplier_address }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </template>
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection

@section('script')
    <script src="{{asset('js/administration/suppliers/index.js')}}"></script>
@endsection