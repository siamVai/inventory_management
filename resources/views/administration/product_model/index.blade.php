@extends('administration.layouts.master')

@section('css')

@endsection

@section('breadcrumb')
    <h1 class="page-title">Product Models</h1>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <template>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">

                                <div class="p-3">
                                    <h4 class="text-muted font-18 m-b-5">Add Models</h4>
                                    {{--<p class="text-muted text-center">Get your free Admiria account now.</p>--}}

                                    <form @submit.prevent="update_status ? update() : store()" class="form-horizontal m-t-30">
                                        @csrf

                                        <div class="form-group row">
                                            <div class="col-md-4"><label for="brand">Brand</label></div>
                                            <div class="col-md-8">
                                                <select name="brand" v-model="brand" id="brand" class="form-control form-control-sm" required>
                                                    <option v-for="brand in brands" :value="brand.brand_id">@{{ brand.brand_name }}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="name">Name</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control form-control-sm" v-model="name" name=name" id="name" placeholder="Name">
                                            </div>
                                        </div>

                                        <div class="form-group row m-t-20">
                                            <div class="col-12 text-right">
                                                <button class="btn btn-sm btn-secondary w-md waves-effect waves-light" type="button" @click="clear_fields()">Cancel</button>
                                                <button class="btn btn-sm btn-danger w-md waves-effect waves-light" type="submit">@{{ update_status ? 'Update' : 'Save' }}</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <table width="100%">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Brand Name</th>
                                        <th>Product Model Name</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="val in models">
                                        <td>
                                            <i class="mdi mdi-pencil text-primary" style="cursor: pointer" @click="edit()"></i>
                                            <i class="mdi mdi-close text-danger" style="cursor: pointer" @click="remove()"></i>
                                        </td>
                                        <td>@{{ val.brand_name }}</td>
                                        <td>@{{ val.model_name }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </template>
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection

@section('script')
    <script src="{{asset('js/administration/product_model/index.js')}}"></script>
@endsection