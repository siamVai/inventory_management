@extends('administration.layouts.master')

@section('css')
    <!-- C3 charts css -->
    {{--    <link href="{{ URL::asset('assets/plugins/c3/c3.min.css') }}" rel="stylesheet" type="text/css" />--}}
@endsection

@section('breadcrumb')
    <h1 class="page-title">Dashboard</h1>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <template>
                <div class="row">
                    <div class="col-md-6 col-lg-6 col-xl-3">

                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <div class="mini-stat clearfix bg-white">
                                    <span class="mini-stat-icon bg-purple mr-0 float-right"><i class="mdi mdi-buffer"></i></span>
                                    <div class="mini-stat-info">
                                        <span class="counter text-purple">@{{ total_stock ?? 0 }}</span>
                                        Current Stock
                                    </div>
                                    <div class="clearfix"></div>
                                    {{--<p class=" mb-0 m-t-20 text-muted">Total income: $22506 <span class="pull-right"><i class="fa fa-caret-up m-r-5"></i>10.25%</span></p>--}}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <div class="mini-stat clearfix bg-white">
                                    <div class="mini-stat-info">
                                        <table width="100%" class="table-striped">
                                            <tr v-for="val in stocks_by_product">
                                                <td><a href="#">@{{ val.product_name }}</a></td>
                                                <td>@{{ val.qty }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-3">
                        <div class="mini-stat clearfix bg-white">
                            <span class="mini-stat-icon bg-blue-grey mr-0 float-right"><i class="mdi mdi-black-mesa"></i></span>
                            <div class="mini-stat-info">
                                <span class="counter text-blue-grey">65241</span>
                                New Orders
                            </div>
                            <div class="clearfix"></div>
                            {{--<p class="text-muted mb-0 m-t-20">Total income: $22506 <span class="pull-right"><i class="fa fa-caret-up m-r-5"></i>10.25%</span></p>--}}
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-6">
                        <div class="card m-b-20">
                            <div class="card-body">
                                <h4 class="mt-0 header-title m-b-30">This Month</h4>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="mini-stat clearfix bg-white">
                                            <div class="mini-stat-info">
                                                <span class="counter text-purple">@{{ total_purchase ?? 0 }}Tk</span>
                                                Total Purchase
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="mini-stat clearfix mb-0 bg-white">
                                            <div class="mini-stat-info">
                                                <span class="counter text-purple">@{{ total_sold ?? 0 }}Tk</span>
                                                Total Sales
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




            </template>
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection

@section('script')

    <!--C3 Chart-->
    {{--<script type="text/javascript" src="{{ URL::asset('assets/plugins/d3/d3.min.js') }}"></script>--}}
    {{--<script type="text/javascript" src="{{ URL::asset('assets/plugins/c3/c3.min.js') }}"></script>--}}

    <script src="/js/administration/home.js"></script>

    <!-- Widget init JS -->
    {{--    <script src="{{ URL::asset('assets/pages/widget-init.js') }}"></script>--}}
@endsection