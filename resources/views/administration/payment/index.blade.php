@extends('administration.layouts.master')

@section('css')
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

    <style>
        .select2-selection__rendered {
            line-height: 31px !important;
        }
        .select2-container .select2-selection--single {
            height: 32px !important;
        }
        .select2-selection__arrow {
            height: 32px !important;
        }
    </style>
@endsection

@section('breadcrumb')
    <h1 class="page-title">Payment</h1>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <form @submit.prevent="store()">
                                @csrf
                                <div class="form-group">
                                    <label for="date">Date</label>
                                    <input type="date" name="date" v-model="date" class="form-control form-control-sm">
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <label for="payment_to">Payment to</label>
                                        <select name="payment_to" id="payment_to" v-model="payment_to" class="form-control form-control-sm">
                                            <option value="supplier">Supplier</option>
                                            <option value="dealer">Retailers</option>
                                        </select>
                                    </div>
                                     <div class="col-md-8">
                                         <label for="object_reference">Name</label>
                                         <select name="object_reference" id="object_reference" v-model="object_reference" class="form-control form-control-sm select2">
                                             <option value="" selected>Select</option>
                                             <option v-if="payment_to=='supplier'" v-for="val in objects" :value="val.supplier_reference">@{{ val.supplier_name }}</option>
                                             <option v-if="payment_to=='dealer'" v-for="val in objects" :value="val.dealer_reference">@{{ val.dealer_name }}</option>
                                         </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="amount">Amount</label>
                                    <input type="text" name="amount" v-model="amount" class="form-control form-control-sm">
                                </div>

                                <div class="form-group">
                                    <label for="desc">Desc</label>
                                    <input type="text" name="desc" v-model="desc" class="form-control form-control-sm">
                                </div>

                                <div class="form-group">
                                    <label for="payment_type">Payment type</label>
                                    <select name="payment_type" id="payment_type" v-model="payment_type" class="form-control form-control-sm">
                                        <option value="cash">Cash</option>
                                    </select>
                                </div>

                                <button class="btn btn-sm btn-danger">Payment</button>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection

@section('script')
    <script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('js/administration/payment/index.js')}}"></script>
@endsection