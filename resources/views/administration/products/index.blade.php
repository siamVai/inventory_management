@extends('administration.layouts.master')

@section('css')

@endsection

@section('breadcrumb')
    <h1 class="page-title">Products</h1>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <template>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">

                                <div class="p-3">
                                    <h4 class="text-muted font-18 m-b-5 text-center">Add Product</h4>
                                    {{--<p class="text-muted text-center">Get your free Admiria account now.</p>--}}

                                    <form @submit.prevent="update_status ? update() : store()" class="form-horizontal m-t-30">
                                        @csrf

                                        <div class="form-group row">
                                            <div class="col-md-4"><label for="brand">Brand</label></div>
                                            <div class="col-md-8">
                                                <select name="brand" v-model="brand" id="brand" class="form-control form-control-sm" required>
                                                    <option v-for="brand in brands" :value="brand.brand_id">@{{ brand.brand_name }}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4"><label for="model">Model</label></div>
                                            <div class="col-md-8">
                                                <select name="model" v-model="model" id="model" class="form-control form-control-sm" required>
                                                    <option value="" selected>Select model...</option>
                                                    <option v-for="model in models" :value="model.model_name">@{{ model.model_name }}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4"><label for="color">Color</label></div>
                                            <div class="col-md-8">
                                                <select name="color" v-model="color" id="color" class="form-control form-control-sm">
                                                    <option value="" selected>Select color...</option>
                                                    <option v-for="val in colors" :value="val.color_name">@{{ val.color_name }}</option>
                                                </select>
                                            </div>
                                        </div>

                                        {{--<div class="form-group row">--}}
                                            {{--<div class="col-md-4"><label for="ram">Storage(RAM)</label></div>--}}
                                            {{--<div class="col-md-8">--}}
                                                {{--<select name="ram" v-model="ram" id="ram" class="form-control form-control-sm">--}}
                                                    {{--<option value="" selected>Select storage...</option>--}}
                                                    {{--<option value="512 MB">512 MB</option>--}}
                                                    {{--<option value="1 GB">1 GB</option>--}}
                                                    {{--<option value="2 GB">2 GB</option>--}}
                                                    {{--<option value="4 GB">4 GB</option>--}}
                                                    {{--<option value="6 GB">6 GB</option>--}}
                                                    {{--<option value="8 GB">8 GB</option>--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        {{--<div class="form-group row">--}}
                                            {{--<div class="col-md-4"><label for="rom">Storage(ROM)</label></div>--}}
                                            {{--<div class="col-md-8">--}}
                                                {{--<select name="rom" v-model="rom" id="rom" class="form-control form-control-sm">--}}
                                                    {{--<option value="" selected>Select storage...</option>--}}
                                                    {{--<option value="8 GB">8 GB</option>--}}
                                                    {{--<option value="16 GB">16 GB</option>--}}
                                                    {{--<option value="32 GB">32 GB</option>--}}
                                                    {{--<option value="64 GB">64 GB</option>--}}
                                                    {{--<option value="128 GB">128 GB</option>--}}
                                                    {{--<option value="256 GB">256 GB</option>--}}
                                                    {{--<option value="512 GB">512 GB</option>--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="name">Name</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control form-control-sm" v-model="name" name=name" id="name" placeholder="Name">
                                            </div>
                                        </div>

                                        <div class="form-group row m-t-20">
                                            <div class="col-12 text-right">
                                                <button class="btn btn-sm btn-secondary w-md waves-effect waves-light" type="button" @click="clear_fields()">Cancel</button>
                                                <button class="btn btn-sm btn-danger w-md waves-effect waves-light" type="submit">@{{ update_status ? 'Update' : 'Save' }}</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <table width="100%">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Product Name</th>
                                        <th>Model</th>
                                        <th>Brand</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="val in products">
                                        <td>
                                            <i class="mdi mdi-pencil text-primary" style="cursor: pointer" @click="edit()"></i>
                                            <i class="mdi mdi-close text-danger" style="cursor: pointer" @click="remove()"></i>
                                        </td>
                                        <td>@{{ val.product_name }}</td>
                                        <td>@{{ val.model_name }}</td>
                                        <td>@{{ val.brand_name }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </template>
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection

@section('script')
    <script src="{{asset('js/administration/products/index.js')}}"></script>
@endsection