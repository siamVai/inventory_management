@extends('administration.layouts.master')

@section('css')
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

    <style>
        .select2-selection__rendered {
            line-height: 31px !important;
        }
        .select2-container .select2-selection--single {
            height: 32px !important;
        }
        .select2-selection__arrow {
            height: 32px !important;
        }
    </style>
@endsection

@section('breadcrumb')
    <h1 class="page-title">Return Purchase</h1>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <template>
                <div class="row">

                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <div class="card m-b-20">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Return products to Supplier</h4>
                                <hr>

                                <form @submit.prevent="store()" method="post">
                                    @csrf

                                    <div class="row mb-3">
                                        <div class="col-md-3">
                                            <label for="imei">Enter IMEI</label>
                                            <input type="text" ref="imei" name="imei" v-model="imei" class="form-control form-control-sm" placeholder="Enter IMEI" @keypress.prevent.enter="addTr(imei)">
                                        </div>
                                        <div class="col-md-1"></div>
                                        <div class="col-md-2">
                                            {{--<label for="object_type">Consumer Type</label>--}}
                                            {{--<select name="object_type" id="object_type" v-model="object_type" class="form-control form-control-sm" required>--}}
                                            {{--<option value="3">Customer</option>--}}
                                            {{--<option value="2">Dealer</option>--}}
                                            {{--</select>--}}
                                        </div>
                                        <div class="col-md-4">
                                            {{--<label for="object">Consumer</label>--}}
                                            {{--<select name="object" id="object" v-model="object" class="form-control form-control-sm select2" required>--}}
                                            {{--<option value="" selected>Choose ...</option>--}}
                                            {{--<option v-if="object_type === '3'" v-for="val in objects" :value="val.customer_reference">--}}
                                            {{--@{{ val.customer_name }} (@{{ val.customer_phone }} : @{{ val.customer_address }})--}}
                                            {{--</option>--}}
                                            {{--<option v-else-if="object_type === '2'" v-for="val in objects" :value="val.dealer_reference">--}}
                                            {{--@{{ val.dealer_name }} (@{{ val.dealer_phone }} : @{{ val.dealer_address }})--}}
                                            {{--</option>--}}
                                            {{--</select>--}}
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="date">Date </label>
                                                <input type="date" title="Date" v-model="date" class="form-control form-control-sm" id="date" required>
                                            </div>
                                        </div>
                                    </div>

                                    <table class="table-sm table-hover table-striped" width="100%">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th>Purchase amount</th>
                                            <th>Return amount</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr v-for="(tr, k) in trs">
                                            <td scope="row" style="color: red; cursor: pointer">
                                                <i class="mdi mdi-close" title="Double click to delete" @dblclick="deleteTr(k, tr)"></i>
                                            </td>
                                            <td>@{{ k + 1 }}</td>
                                            <td> <span class="font-weight-bold">@{{ tr.imei }}</span> <span v-if="tr.product_name">(@{{ tr.product_name }})</span></td>
                                            <td> @{{ tr.consumer_name }}</td>
                                            <td>QTY : @{{ tr.qty }}</td>
                                            <td width="20%">
                                                <input v-model="tr.price" title="Unit Price" class="form-control form-control-sm text-right" type="text" placeholder="Enter price" @keyup="calculateTotal()" required readonly="readonly"/>
                                            </td>
                                            <td width="20%">
                                                <input v-model="tr.returns" title="Return Amount" class="form-control form-control-sm text-right" type="text" placeholder="Enter return amount" @keyup="calculateReturned()" required/>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="4"></td>
                                            <td></td>
                                            <td  width="20%">
                                                <input class="form-control form-control-sm text-right" type="text" v-model="paid" readonly/>
                                            </td>
                                            <td  width="20%">
                                                <input class="form-control form-control-sm text-right" type="text" v-model="returned" />
                                            </td>
                                        </tr>


                                        <tr>
                                            <td colspan="5"></td>
                                            <td style="text-align: right;">Due : </td>
                                            <td  width="20%">
                                                <input readonly class="form-control form-control-sm text-right" type="text" v-model="due" />
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <textarea v-model="desc" name="desc" id="desc" class="form-control form-control-sm" placeholder="Enter any comments..."></textarea>
                                        </div>
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4"></div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-sm btn-danger" :disabled="btn_form_submit_disabled"><i class="mdi mdi-keyboard-return"></i> Return Confirm</button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>

                </div>
            </template>
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection

@section('script')
    <script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('js/administration/return/purchase/index.js')}}"></script>
@endsection