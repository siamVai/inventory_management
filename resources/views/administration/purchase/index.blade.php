@extends('administration.layouts.master')

@section('css')

@endsection

@section('breadcrumb')
    <h1 class="page-title">Purchase</h1>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <template>
                <div class="row">

                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <div class="card m-b-20">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Purchase products form SUPPLIERS</h4>
                                <p></p>

                                <form @submit.prevent="store()" method="post">
                                    @csrf

                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-3"></div>
                                        <div class="col-md-3">
                                            <label for="customer">Suppliers</label>
                                            <select name="" id="customer" v-model="supplier" class="form-control form-control-sm" required>
                                                <option value="" selected>Choose ...</option>
                                                <option v-for="(val, index) in suppliers" :key="index" v-bind:value="val.supplier_reference">
                                                    @{{ val.supplier_name }} (@{{ val.supplier_phone }} : @{{ val.supplier_address }})
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="date">Date </label>
                                                <input type="date" title="Date" v-model="date" class="form-control form-control-sm" id="date" required>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-sm table-hover table-striped">
                                        <thead>
                                        <tr>
                                            <th style="width: 25%">Item </th>
                                            <th style="width: 25%">Description </th>
                                            <th>Qty </th>
                                            <th>Price</th>
                                            <th>Dealer Price</th>
                                            <th style="text-align: right;">Total</th>
                                            <th style=""></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr v-for="(tr, k) in trs" :key="k">
                                            <td>
                                                <select v-model="tr.product_reference" id="products" class="form-control form-control-sm" style="width: 100%" required>
                                                    <option value="" selected>Select product...</option>
                                                    <option v-for="(val, index) in products" :value="val.product_reference"> @{{val.product_name}} </option>
                                                </select>
                                            </td>
                                            <td><input v-model="tr.desc" class="form-control form-control-sm" type="text"/></td>
                                            <td><input v-model="tr.qty" min="1" class="form-control form-control-sm" type="number" @change="calculateLineTotal(tr)"/></td>
                                            <td><input v-model="tr.price" min="1" class="form-control form-control-sm" type="text" @change="calculateLineTotal(tr)"/></td>
                                            <td><input v-model="tr.dealer_price" min="1" class="form-control form-control-sm" type="text"/></td>
                                            <td style="text-align: right;"><input v-model="tr.line_total" min="1" class="form-control form-control-sm text-right" type="text"/></td>
                                            <td scope="row" style="color: red; cursor: pointer">
                                                <i class="mdi mdi-close" title="Double click to delete" @dblclick="deleteTr(k, tr)"></i>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <button type="button" class="btn btn-sm btn-outline-secondary" @click="addTr()">+ New </button>
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td style="text-align: right;">Sub total : </td>
                                            <td>
                                                <input readonly class="form-control form-control-sm text-right" type="text" v-model="subtotal" />
                                            </td>
                                            <th></th>
                                        </tr>

                                        </tbody>
                                    </table>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-sm btn-success" :disabled="btn_form_submit_disabled">Purchase Confirm</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </template>
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection

@section('script')
    <script src="{{asset('js/administration/purchase/index.js')}}"></script>
@endsection