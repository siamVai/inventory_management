@extends('layouts.master-without-nav')

@section('content')
    <!-- Begin page -->
    {{--<div class="accountbg"></div>--}}
    <div class="wrapper-page">
        <div class="card">
            <div class="card-body">
                {{--<h3 class="text-center m-0">--}}
                    {{--<a href="index" class="logo logo-admin"><img src="{{ URL::asset('assets/images/logo.png') }}" height="30" alt="logo"></a>--}}
                {{--</h3>--}}

                <div class="p-3">
                    <h4 class="text-muted font-18 m-b-5 text-center">Welcome Back !</h4>
                    <p class="text-muted text-center">Sign in to continue session.</p>

                    <form class="form-horizontal m-t-30" action="{{route('login')}}" method="POST">
                        @csrf
                        <div class="form-group ">
                            <label for="email" class="col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror

                        </div>

                        <div class="form-group">
                            <label for="password" class="col-form-label text-md-right">{{ __('Password') }}</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group row m-t-20">
                            <div class="col-sm-6">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customControlInline">
                                    <label class="custom-control-label" for="customControlInline">Remember me</label>
                                </div>
                            </div>
                            <div class="col-sm-6 text-right">
                                <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Log In</button>
                            </div>
                        </div>

                        <div class="form-group m-t-10 mb-0 row">
                            <div class="col-12 m-t-20">
                                <a href="#" class="text-muted"><i class="mdi mdi-lock"></i> Forgot your password?</a>
                                {{--<a href="{{route('register')}}" class="font-500 font-14 font-secondary"> Sign-up Now </a>--}}
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection


