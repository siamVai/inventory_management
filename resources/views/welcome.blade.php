@extends('layouts.master-without-nav')

@section('content')
    <section class="mt-5 mb-5">
        <div class="container-alt container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="home-wrapper m-t-40">
                        <img src="{{ URL::asset('assets/images/logo.png') }}" alt="logo" height="30" />
                        <h3 class="m-t-30" >Inventory Management by DataFleep</h3>
                        <p>Make your inventory easier.</p>
                        <p>
                            @if (Route::has('login'))
                                <div class="top-right links">
                                    @auth
                                        <a class="btn btn-sm btn-primary" href="{{ url('/home') }}">Home</a>
                                    @else
                                        <a class="btn btn-sm btn-primary" href="{{ route('login') }}">Login</a>
                                        {{--@if (Route::has('register'))--}}
                                            {{--<a href="{{ route('register') }}">Register</a>--}}
                                        {{--@endif--}}
                                    @endauth
                                </div>
                            @endif
                        </p>

                        <div class="row">
                                <div class="text-center col-md-4">
                                    <div class="card mt-4">
                                        <div class="card-body">
                                            <i class="mdi mdi-airplane-takeoff font-40 m-b-15"></i>
                                            <h6 class="text-uppercase">STOCK</h6>
                                            <p class="text-muted m-t-20">STOCK as much as you want.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center col-md-4">
                                    <div class="card mt-4">
                                        <div class="card-body">
                                            <i class="mdi mdi-clock-alert font-40 m-b-15"></i>
                                            <h6 class="text-uppercase">
                                                SALE</h6>
                                            <p class="text-muted m-t-20">Easier SALE process.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center col-md-4">
                                    <div class="card mt-4">
                                        <div class="card-body">
                                            <i class="mdi mdi-email font-40 m-b-15"></i>
                                            <h6 class="text-uppercase">
                                                REPORT</h6>
                                            <p class="text-muted m-t-20">
                                                REPORTS gives better view of business.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')

@endsection

