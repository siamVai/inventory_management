<link rel="shortcut icon" href="{{ URL::asset('assets_dealer/images/favicon.ico') }}">
@yield('css')
<!-- Basic Css files -->
<link href="{{ URL::asset('assets_dealer/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('assets_dealer/css/icons.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('assets_dealer/css/style.css') }}" rel="stylesheet" type="text/css">
