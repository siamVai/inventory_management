<script src="{{ URL::asset('assets_dealer/js/jquery.min.js') }}"></script>
<script src="{{ URL::asset('assets_dealer/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ URL::asset('assets_dealer/js/modernizr.min.js') }}"></script>
<script src="{{ URL::asset('assets_dealer/js/jquery.slimscroll.js') }}"></script>
<script src="{{ URL::asset('assets_dealer/js/waves.js') }}"></script>
<script src="{{ URL::asset('assets_dealer/js/jquery.nicescroll.js') }}"></script>
<script src="{{ URL::asset('assets_dealer/js/jquery.scrollTo.min.js') }}"></script>

 @yield('script')

 <!-- App js -->
 <script src="{{ URL::asset('assets_dealer/js/app.js') }}"></script>
 
@yield('script-bottom')


