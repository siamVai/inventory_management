var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',

        id: '',
        customer_name: '',
        customer_phone: '',
        customer_address: '',

        customers: [],

        update_status : false,
    },
    created () {
        this.get_customers();
    },
    methods : {
        get_customers: function(){
            var _this = this;
            axios.get('/api/administration/customers/get')
                .then(
                    (response) => {
                        _this.customers = response.data;
                    }
                );
        },
        store : function(){
            axios.post('/api/administration/customers/store', {
                consumer_name : this.customer_name,
                consumer_phone : this.customer_phone,
                consumer_address : this.customer_address,
            })
                .then(
                    (response) => {
                        if (response.status == 201) {
                            this.get_customers();
                            alertify.success('Customer added successfully', function (ev) {
                                ev.preventDefault();
                            });
                            this.clear_fields();
                        }else{
                            alertify.error(response.message, function (ev) {
                                ev.preventDefault();
                            });
                        }
                    }
                );
        },
        edit : function(){

        },
        update : function(){

        },
        remove : function(){

        },

        clear_fields : function () {
            this.customer_name = '';
            this.customer_phone = '';
            this.customer_address = '';

            this.update_status = false;
        }
    },
})