var app = new Vue({
    el: '#app',
    data: {
        date: moment().format('YYYY-MM-DD'),
        payment_from : 'customer',
        object_reference : '',
        amount : '',
        desc : '',
        payment_type : 'cash',

        objects : [],

        disable_select_object: false,

    },
    mounted: function() {
        $('#object_reference').select2().on('change', function() {
            app.object_reference = $(this).val();
        });
    },
    created () {
        // this.get_dealers();
        this.get_customers();
    },
    methods : {
        get_customers: function(){
            var _this = this;
            _this.disable_select_object = true;
            axios.get('/api/administration/customers/get')
                .then(
                    (response) => {
                        _this.objects = response.data;
                        _this.disable_select_object = false;
                        if (response.data.length > 0){
                            // _this.object_reference = response.data[0].dealer_reference;
                        }
                    }
                );
        },
        get_dealers: function(){
            var _this = this;
            _this.disable_select_object = true;
            axios.get('/api/administration/retailers/get')
                .then(
                    (response) => {
                        _this.objects = response.data;
                        _this.disable_select_object = false;
                        if (response.data.length > 0){
                            // _this.object_reference = response.data[0].dealer_reference;
                        }
                    }
                );
        },
        get_dsos: function(){
            var _this = this;
            _this.disable_select_object = true;
            axios.get('/api/administration/dsos/get')
                .then(
                    (response) => {
                        _this.objects = response.data;
                        _this.disable_select_object = false;
                        if (response.data.length > 0){
                            // _this.object_reference = response.data[0].dealer_reference;
                        }
                    }
                );
        },
        get_suppliers: function(){
            var _this = this;
            _this.disable_select_object = true;
            axios.get('/api/administration/suppliers/get')
                .then(
                    (response) => {
                        _this.objects = response.data;
                        _this.disable_select_object = false;
                        if (response.data.length > 0){
                            // _this.object_reference = response.data[0].supplier_reference;
                        }
                    }
                );
        },
        store : function(){

            if (!confirm('Are you sure about this CASH Receive?')) {
                return false;
            }

            var self = this;
            axios.post('/api/administration/receive/store', {
                date : self.date,
                object_reference : self.object_reference,
                amount : self.amount,
                desc : self.desc,
                payment_from : self.payment_from,
            })
                .then(
                    (response) => {
                        if (response.status == 201) {
                            alertify.success('Cash Received successfully', function (ev) {
                                ev.preventDefault();
                            });
                            this.clear_fields();
                        }else{
                            alertify.error(response.data, function (ev) {
                                ev.preventDefault();
                            });
                        }
                    }
                );
        },
        clear_fields : function () {
            var self = this;
            self.date = moment().format('YYYY-MM-DD');
            self.amount = '';
            self.desc = '';
            self.payment_from  = 'customer';
        }
    },
    watch : {
        payment_from : function (val) {
            var self = this;
            if (val == 'dso'){
                self.object_reference = '';
                self.get_dsos();
            } else if (val == 'supplier'){
                self.object_reference = '';
                self.get_suppliers();
            } else if (val == 'dealer'){
                self.object_reference = '';
                self.get_dealers();
            } else if (val == 'customer'){
                self.object_reference = '';
                self.get_customers();
            }
        }
    }

});