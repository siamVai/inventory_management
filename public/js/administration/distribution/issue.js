var app = new Vue({
    el: '#app',
    data: {

        dsos: [],
        dso : '',
        date : moment().format("YYYY-MM-DD"),

        imei : '',
        stock : [],
        trs: [],
        issued_list: [],

        desc : '',

        btn_form_submit_disabled : false,
    },
    mounted: function() {
        $("#dsos").select2().on('change', function() {
            app.dso = $(this).val();
        });
        this.get_dsos();
    },
    created () {
        this.get_issued_products();
        this.focusIMEI();
    },
    methods : {
        focusIMEI() {
            var self = this;
            Vue.nextTick(function () {
                self.$refs.imei.focus();
            });
        },
        get_issued_products: function(){
            var _this = this;
            axios.get('/api/administration/distribution/issued-product-list')
                .then(
                    (response) => {
                        _this.issued_list = response.data.data;
                    }
                );
        },
        get_dsos: function(){
            var _this = this;
            axios.get('/api/administration/dsos/get')
                .then(
                    (response) => {
                        _this.dsos = response.data;
                    }
                );
        },

        addTr(imei) {
            var _this = this;

            if (!_this.dso) {
                alertify.error('Choose DSO', function (ev) {
                    ev.preventDefault();
                });
                return false
            }

            var exist_in_table = false;
            _this.trs.forEach(val => {
                if (val.imei === imei){
                    exist_in_table = true;
                    return false;
                }
            });

            //if in table
            if (exist_in_table == true){
                alertify.error('Already exist in table', function (ev) {
                    ev.preventDefault();
                });
                return false
            }

            axios.get('/api/administration/stock/imei/sold-to-dso', {
                params : {
                    imei : imei,
                    dso_reference : _this.dso
                }
            }).then(
                (response) => {
                    if (response.status === 200){
                        _this.stock = [];
                        _this.stock = response.data;

                        if (_this.stock.imei === imei) {
                            _this.trs.push({
                                imei: imei,
                                product_name: _this.stock.product_name,
                                sku: _this.stock.sku,
                            });
                            _this.imei = '';
                            _this.focusIMEI();
                            //_this.calculateTotal();
                        }else{
                            alertify.error('This IMEI not exist in DSO sold list', function (ev) {
                                ev.preventDefault();
                            });
                        }

                    }
                }
            );
        },
        deleteTr(index, tr) {
            var idx = this.trs.indexOf(tr);
            this.trs.splice(idx, 1);
            //this.calculateTotal();
        },

        clear_fields : function () {
            var _this = this;
            _this.date = moment().format("YYYY-MM-DD");
            _this.trs = [];
            _this.desc = '';
        },

        store : function () {
            var _this = this;
            if (_this.trs.length < 1){
                alertify.error('No IMEI added!', function (ev) { ev.preventDefault(); });
                _this.focusIMEI();
                return false;
            }

            if (!confirm('Are you sure about this Distribution?')) { return false; }

            _this.btn_form_submit_disabled = true; // make btn disabled when click
            axios.post('/api/administration/distribution/issue', {
                date : _this.date,
                dso_reference : _this.dso,
                products : _this.trs,
                desc : _this.desc,
            }).then(
                (response) => {
                    console.log(response.data);
                    if (response.status === 201){
                        alertify.success('Distribution successful', function (ev) {
                            ev.preventDefault();
                        });
                        _this.get_issued_products();
                        _this.clear_fields();
                        _this.btn_form_submit_disabled = false; // make btn enabled when data saved and field are cleared
                    }else{
                        alertify.error(response.data, function (ev) { ev.preventDefault(); });
                    }
                }
            ).catch((e) => {
                console.log(e)
            })
            ;
        },

    },
    watch : {
        dso : function (val) {
            var self = this;
            if (val){
                self.trs = [];
            }
        },
        // returned : function () {
        //     var self = this;
        //     self.due = (self.subtotal - self.returned).toFixed(2);
        // },
    }
});