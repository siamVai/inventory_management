var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',

        id: '',
        supplier_name: '',
        supplier_phone: '',
        supplier_address: '',

        suppliers: [],

        update_status : false,
    },
    created () {
        this.get_suppliers();
    },
    methods : {
        get_suppliers: function(){
            var _this = this;
            axios.get('/api/administration/suppliers/get')
                .then(
                    (response) => {
                        _this.suppliers = response.data;
                    }
                );
        },
        store : function(){
            axios.post('/api/administration/suppliers/store', {
                supplier_name : this.supplier_name,
                supplier_phone : this.supplier_phone,
                supplier_address : this.supplier_address,
            })
                .then(
                    (response) => {
                        console.log(response.status);
                        if (response.status == 201) {
                            this.get_suppliers();
                            alertify.success('Supplier added successfully', function (ev) {
                                ev.preventDefault();
                            });
                            this.clear_fields();
                        }else{
                            alertify.error(response.message, function (ev) {
                                ev.preventDefault();
                            });
                        }
                    }
                );
        },
        edit : function(){

        },
        update : function(){

        },
        remove : function(){

        },

        clear_fields : function () {
            this.supplier_name = '';
            this.supplier_phone = '';
            this.supplier_address = '';

            this.update_status = false;
        }
    },
})