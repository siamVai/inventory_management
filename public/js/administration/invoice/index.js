Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('DD MMM YYYY')
    }
});
var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',

        invoices: [],

        paginate : "25",
        url : '/api/administration/invoice/all',
        pagination : [],

        //Searching
        input_invoice: '',
    },
    created () {
        this.get_invoice();
    },
    methods : {
        get_invoice: function(){
            var _this = this;
            axios.get('/api/administration/invoice/all', {
                params: {
                    invoice_id: _this.input_invoice
                }
            })
                .then(
                    (response) => {
                        _this.invoices = response.data;
                        _this.make_pagination(_this.invoices)
                    }
                );
        },
        make_pagination: function(data){
            this.url = '/api/administration/invoice/all';
            const pagination = {
                current_page : data.current_page,
                last_page : data.last_page,
                next_page_url : data.next_page_url,
                prev_page_url : data.prev_page_url,
            };
            this.pagination = pagination;
        },
        fetch_paginate_data: function(url){
            this.url = url;
            this.get_invoice();
        },
    },
    computed: {
        filterInvoiceList: function () {
            return this.invoices.data;
        }
    },
    watch : {
        input_invoice: function (val) {
            if (!val)
            {
                this.get_invoice();
            }
        }
    }
});