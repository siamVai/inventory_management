Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('DD MMM YYYY')
    }
});
Vue.filter('uppercase', function(value) {
    if (value) {
        return value.toUpperCase()
    }
});
var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',

        invoice_id: window.invoice_id,
        invoice: {},
        details: [],

        subtotal: 0,

        consumer: {},
        consumer_type: '',
        consumer_reference: '',
        str_consumer_type: '',
    },
    created () {
        this.get_invoice();
    },
    methods : {
        get_invoice: function(){
            var _this = this;
            axios.get('/api/administration/invoice/get', {
                params: {
                    invoice_id: _this.invoice_id
                }
            })
                .then(
                    (response) => {
                        _this.invoice = response.data;
                        _this.consumer_reference = response.data.consumer_reference;
                        _this.consumer_type = response.data.object_type_id;
                        if (_this.consumer_type == 1){
                            _this.str_consumer_type = 'suppliers';
                        }else if (_this.consumer_type == 2){
                            _this.str_consumer_type = 'retailers';
                        }else if (_this.consumer_type == 3){
                            _this.str_consumer_type = 'customers';
                        }else if (_this.consumer_type == 2){
                            _this.str_consumer_type = 'dsos';
                        }
                        this.get_consumer();
                        this.get_ledger();
                    }
                );
        },
        get_consumer: function(){
            var _this = this;
            axios.get('/api/administration/'+_this.str_consumer_type+'/show/'+_this.consumer_reference)
                .then(
                    (response) => {
                        _this.consumer = response.data;
                    }
                );
        },
        get_ledger: function () {
            var _this = this;
            axios.get('/api/administration/invoice/details', {
                params: {
                    ps_reference: _this.invoice.ps_reference
                }
            })
                .then(
                    (response) => {
                        _this.details = response.data;
                        let total_price = 0;
                        _this.details.forEach(val => {
                            if (_this.invoice.action_name == 'purchase'){
                                total_price += parseFloat(val.debit);
                            } else{
                                total_price += parseFloat(val.credit);
                            }
                        });

                        _this.subtotal = total_price.toFixed(2);
                    }
                );
        }
    },
});