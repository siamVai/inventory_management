var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        products: [],
        suppliers: [],

        supplier : '',
        date : moment().format("YYYY-MM-DD"),
        trs: [{
            product_reference: '',
            desc: '',
            qty: 1,
            price: '',
            line_total: 0
        }],
        subtotal : 0,

        btn_form_submit_disabled : false,
    },
    created () {
        this.get_suppliers();
        this.get_products();
    },
    methods : {
        get_suppliers: function(){
            var _this = this;
            axios.get('/api/administration/suppliers/get')
                .then(
                    (response) => {
                        _this.suppliers = response.data;
                    }
                );
        },
        get_products: function(){
            var _this = this;
            axios.get('/api/administration/products')
                .then(
                    (response) => {
                        _this.products = response.data;
                    }
                );
        },

        addTr() {
            this.trs.push({
                product_reference: '',
                desc: '',
                qty: 1,
                price: '',
                line_total: 0
            });
        },
        deleteTr(index, tr) {
            var idx = this.trs.indexOf(tr);
            if (idx > 0) {
                this.trs.splice(idx, 1);
            }
        },
        calculateLineTotal(tr) {
            var total = parseFloat(tr.qty) * parseFloat(tr.price);
            if (!isNaN(total)) {
                tr.line_total = total.toFixed(2);
            }
            this.calculateTotal();
        },
        calculateTotal() {
            var subtotal;
            subtotal = this.trs.reduce(function (sum, product) {
                var lineTotal = parseFloat(product.line_total);
                if (!isNaN(lineTotal)) {
                    return sum + lineTotal;
                }
            }, 0);
            this.subtotal = subtotal.toFixed(2);
        },
        clear_fields : function () {
            var _this = this;

            _this.supplier = '';
            _this.date = moment().format("YYYY-MM-DD");
            _this.trs = [{
                product_reference: '',
                desc: '',
                qty: 1,
                price: 0,
                dealer_price: 0,
                line_total: 0
            }];
            _this.subtotal = 0;
        },

        store : function () {
            var _this = this;
            if (!_this.subtotal > 0) {
                alertify.error('Enter Price properly', function (ev) {
                    ev.preventDefault();
                });
                return false
            }
            if (!confirm('Are you sure about this PURCHASE?')) return false;

            _this.btn_form_submit_disabled = true; // make btn disabled when click
            axios.post('/api/administration/purchase/store', {
                supplier : _this.supplier,
                date : _this.date,
                products : _this.trs,
                subtotal : _this.subtotal,
            })
                .then(
                    (response) => {
                        if (response.status === 201){
                            alertify.success('Purchased successfully', function (ev) {
                                ev.preventDefault();
                            });
                            _this.clear_fields()
                            _this.btn_form_submit_disabled = false; // make btn enabled when data saved and field are cleared
                            window.open('/administration/invoice/get?q='+response.data, '_blank');
                        }else{
                            alertify.error(response.status, function (ev) {
                                ev.preventDefault();
                            });
                        }
                    }
                );
        },
    }
});