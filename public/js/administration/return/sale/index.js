var app = new Vue({
    el: '#app',
    data: {

        object_type: '3', //Customer
        objects: [], // Customers/Dealers
        object : '',
        date : moment().format("YYYY-MM-DD"),

        imei : '',
        stock : [],
        trs: [],

        subtotal : 0,
        paid : 0,
        returned : 0,
        due : 0,
        desc : '',

        btn_form_submit_disabled : false,

        //Customer add
        customer_name: '',
        customer_phone: '',
        customer_address: '',

    },
    mounted: function() {
        $('#object').select2().on('change', function() {
            app.object = $(this).val();
        });
    },
    created () {
        this.focusIMEI();
    },
    methods : {

        focusIMEI() {
            var self = this;
            Vue.nextTick(function () {
                self.$refs.imei.focus();
            });
        },
        addTr(imei) {

            var _this = this;

            var exist_in_table = false;

            _this.trs.forEach(val => {
                if (val.imei === imei){
                    exist_in_table = true;
                    return false;
                }
            });

            //if in table
            if (exist_in_table == true){
                alertify.error('Already exist in table', function (ev) {
                    ev.preventDefault();
                });
                return false
            }

            axios.get('/api/administration/stock/imei/sold', {
                params : {
                    imei : imei
                }
            }).then(
                    (response) => {
                        if (response.status === 200){
                            _this.stock = [];
                            _this.stock = response.data;

                            if (_this.stock.imei === imei) {
                                _this.trs.push({
                                    sku: _this.stock.sku,
                                    product_name: _this.stock.product_name,
                                    consumer_name: _this.stock.consumer_name,
                                    imei: imei,
                                    qty: 1,
                                    price: _this.stock.price,
                                    returns: _this.stock.price,
                                    ledger_reference: _this.stock.ledger_reference,

                                });
                                _this.imei = '';
                                _this.focusIMEI();
                                _this.calculateTotal();
                            }else{
                                alertify.error('This IMEI not exist ins sold list', function (ev) {
                                    ev.preventDefault();
                                });
                            }

                        }
                    }
                );



        },
        deleteTr(index, tr) {
            var idx = this.trs.indexOf(tr);
            this.trs.splice(idx, 1);
            this.calculateTotal();
        },
        calculateTotal() {
            var subtotal = 0;
            subtotal = this.trs.reduce(function (sum, product) {
                var price = parseFloat(product.price);
                if (!isNaN(price)) {
                    return sum + price;
                }
            }, 0);
            this.subtotal = subtotal.toFixed(2);
            this.paid = subtotal.toFixed(2);
            this.returned = subtotal.toFixed(2);
            this.due = (this.returned - this.paid).toFixed(2);
        },
        calculateReturned() {
            var subtotal = 0;
            subtotal = this.trs.reduce(function (sum, product) {
                var price = parseFloat(product.returns);
                if (!isNaN(price)) {
                    return sum + price;
                }
            }, 0);
            this.returned = subtotal.toFixed(2);
            this.due = (this.paid - this.returned).toFixed(2);
        },

        clear_fields : function () {
            var _this = this;

            // _this.supplier = '';
            _this.date = moment().format("YYYY-MM-DD");
            _this.trs = [];
            _this.subtotal = 0;
            _this.paid = 0;
            _this.returned = 0;
            _this.due = 0;
            _this.desc = '';
        },

        store : function () {
            var _this = this;
            if (_this.trs.length < 1){
                alertify.error('No IMEI added!', function (ev) {
                    ev.preventDefault();
                });
                _this.focusIMEI();
                return false;
            }

            if (_this.subtotal < 1){
                alertify.error('Total amount cannot be 0', function (ev) {
                    ev.preventDefault();
                });
                return false;
            }

            if (!confirm('Are you sure about this Sale Return?')) {
                return false;
            }

            _this.btn_form_submit_disabled = true; // make btn disabled when click

            axios.post('/api/administration/return/sale/store', {
                // object_type : _this.object_type,
                // object : _this.object,
                date : _this.date,
                products : _this.trs,
                subtotal : _this.subtotal,
                paid : _this.paid,
                returned : _this.returned,
                desc : _this.desc,
            }).then(
                    (response) => {
                        if (response.status === 201){
                            alertify.success('Return successful', function (ev) {
                                ev.preventDefault();
                            });
                            _this.clear_fields()
                            _this.btn_form_submit_disabled = false; // make btn enabled when data saved and field are cleared
                        }else{
                            alertify.error(response.data, function (ev) {
                                ev.preventDefault();
                            });
                        }
                    }
                ).catch((e) => {
                    console.log(e)
            })
            ;
        },

    },
    watch : {
        // object_type : function (val) {
        //     var self = this;
        //     if (val){
        //         self.object = "";
        //         self.get_objects();
        //     }
        // },
        returned : function () {
            var self = this;
            self.due = (self.subtotal - self.returned).toFixed(2);
        },
    }
});