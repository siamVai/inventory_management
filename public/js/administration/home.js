var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        stock: 0,
        stocks_by_product: [],

        total_sold: 1,
        total_purchase: 0,

        total_stock: 0,
    },
    mounted () {

    },
    created () {
        this.get_stock();
        this.get_groupby();
        this.get_total_purchase();
        this.get_total_sold();
    },
    methods : {
        get_stock: function(){
            var _this = this;
            axios.get('/api/administration/stock/all')
                .then(
                    (response) => {
                        _this.total_stock = response.data;
                    }
                );
        },
        get_groupby: function(){
            var _this = this;
            axios.get('/api/administration/stock/groupby')
                .then(
                    (response) => {
                        _this.stocks_by_product = response.data;
                    }
                );
        },
        get_total_purchase: function () {
            var _this = this;
            axios.get('/api/administration/purchase/total_purchased')
                .then(
                    (response) => {
                        _this.total_purchase = response.data;
                    }
                );
        },
        get_total_sold: function () {
            var _this = this;
            axios.get('/api/administration/sale/total_sold')
                .then(
                    (response) => {
                        _this.total_sold = response.data;
                    }
                );
        }
    },
})