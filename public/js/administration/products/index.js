var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',

        id: '',
        name: '',

        brand: '1',
        model: '',
        color: '',
        // ram: '',
        // rom: '',

        brands: [],
        models: [],
        colors: [],
        products: [],

        update_status : false,
    },
    created () {
        this.get_brands();
        this.get_models();
        this.get_colors();
        this.get_products();
    },
    methods : {
        get_brands: function(){
            var _this = this;
            axios.get('/api/administration/brands/get')
                .then(
                    (response) => {
                        _this.brands = response.data;
                    }
                );
        },
        get_models: function(){
            var _this = this;
            axios.get('/api/administration/models/get')
                .then(
                    (response) => {
                        _this.models = response.data;
                    }
                );
        },
        get_colors: function(){
            var _this = this;
            axios.get('/api/colors/get')
                .then(
                    (response) => {
                        _this.colors = response.data;
                    }
                );
        },

        get_products: function(){
            var _this = this;
            axios.get('/api/administration/products')
                .then(
                    (response) => {
                        _this.products = response.data;
                    }
                );
        },

        store : function(){
            axios.post('/api/administration/products/store', {
                name : this.name,
                brand : this.brand,
                model : this.model,
                color : this.color,
                // ram : this.ram,
                // rom : this.rom,
            })
                .then(
                    (response) => {
                        if (response.status == 201) {
                            this.get_products();
                            alertify.success('Product added successfully', function (ev) {
                                ev.preventDefault();
                            });
                            this.clear_fields();
                        }else{
                            alertify.error(response.data, function (ev) {
                                ev.preventDefault();
                            });
                        }
                    }
                );
        },
        edit : function(){

        },
        update : function(){

        },
        remove : function(){

        },

        clear_fields : function () {
            this.name = '';
            this.model = '';
            this.color = '';
            // this.ram = '';
            // this.rom = '';
            this.update_status = false;
        }
    },
    computed : {
        combine () {
            return [this.model, this.color].join()
        }
    },
    watch : {
        combine : function () {
            // let storage = '';
            // if (this.rom && this.ram){
            //     storage = this.ram+'/'+this.rom;
            // } else if (this.rom && !this.ram){
            //     storage = this.rom;
            // }else if (!this.rom && this.ram){
            //     storage = this.ram;
            // }
            this.name = this.model +' '+this.color;
        }
    }
})