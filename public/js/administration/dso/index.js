var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',

        id: '',
        dso_name: '',
        dso_phone: '',
        dso_address: '',

        dsos: [],

        update_status : false,
    },
    created () {
        this.get_dsos();
    },
    methods : {
        get_dsos: function(){
            var _this = this;
            axios.get('/api/administration/dsos/get')
                .then(
                    (response) => {
                        _this.dsos = response.data;
                    }
                );
        },
        store : function(){
            axios.post('/api/administration/dsos/store', {
                consumer_name : this.dso_name,
                consumer_phone : this.dso_phone,
                consumer_address : this.dso_address,
            })
                .then(
                    (response) => {
                        if (response.status == 201) {
                            this.get_dsos();
                            alertify.success('DSO added successfully', function (ev) {
                                ev.preventDefault();
                            });
                            this.clear_fields();
                        }else{
                            alertify.error(response.message, function (ev) {
                                ev.preventDefault();
                            });
                        }
                    }
                );
        },
        edit : function(){

        },
        update : function(){

        },
        remove : function(){

        },

        clear_fields : function () {
            this.dso_name = '';
            this.dso_phone = '';
            this.dso_address = '';

            this.update_status = false;
        }
    },
})