var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        purchase : [],

        total_credit : 0,
        total_debit : 0,
    },
    created () {
        this.get_purchase();
    },
    methods : {
        get_purchase: function(){
            var _this = this;
            axios.get('/api/administration/reports/purchase/get')
            .then(
                (response) => {
                    _this.purchase = response.data;

                    var total_payable = 0;
                    var total_paid = 0;
                    _this.purchase.forEach((value) => {
                        total_payable += parseFloat(value.credit);
                        total_paid += parseFloat(value.debit);
                    });
                    _this.total_credit = total_payable;
                    _this.total_debit = total_paid;
                }
            );
        }
    },
})