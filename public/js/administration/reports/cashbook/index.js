var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        cashbook : [],

        total_credit : 0,
        total_debit : 0,
    },
    created () {
        this.get_cashbook();
    },
    methods : {
        get_cashbook: function(){
            var _this = this;
            axios.get('/api/administration/reports/cashbook/get')
            .then(
                (response) => {
                    _this.cashbook = response.data;

                    var total_payable = 0;
                    var total_paid = 0;
                    _this.cashbook.forEach((value) => {
                        total_payable += parseFloat(value.credit);
                        total_paid += parseFloat(value.debit);
                    });
                    _this.total_credit = total_payable;
                    _this.total_debit = total_paid;
                }
            );
        }
    },
})