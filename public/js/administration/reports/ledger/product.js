var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        product_reference: window.product_reference,
        product : [],
        ledger : [],

        total_credit : 0,
        total_debit : 0,
    },
    created () {
        this.get_product_info();
        this.get_product_ledger();
    },
    methods : {
        get_product_ledger: function(){
            var _this = this;
            axios.get('/api/administration/reports/ledger/product/get', {
                params : {
                    product_reference: _this.product_reference
                }
            })
            .then(
                (response) => {
                    _this.ledger = response.data;

                    var total_payable = 0;
                    var total_paid = 0;
                    _this.ledger.forEach((value) => {
                        total_payable += parseFloat(value.credit);
                        total_paid += parseFloat(value.debit);
                    });
                    _this.total_credit = total_payable;
                    _this.total_debit = total_paid;
                }
            );
        },
        get_product_info: function(){
            var _this = this;
            axios.get('/api/administration/products/show/'+ _this.product_reference)
                .then(
                    (response) => {
                        _this.product = response.data;
                    }
                );
        }
    },
})