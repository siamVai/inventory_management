var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        customer_reference: window.customer_reference,
        customer : [],
        ledger : [],

        total_credit : 0,
        total_debit : 0,
    },
    created () {
        this.get_customer_info();
        this.get_customer_ledger();
    },
    methods : {
        get_customer_ledger: function(){
            var _this = this;
            axios.get('/api/administration/reports/ledger/customer/get', {
                params : {
                    customer_reference: _this.customer_reference
                }
            })
            .then(
                (response) => {
                    _this.ledger = response.data;

                    var total_payable = 0;
                    var total_paid = 0;
                    _this.ledger.forEach((value) => {
                        total_payable += parseFloat(value.credit);
                        total_paid += parseFloat(value.debit);
                    });
                    _this.total_credit = total_payable;
                    _this.total_debit = total_paid;
                }
            );
        },
        get_customer_info: function(){
            var _this = this;
            axios.get('/api/administration/customers/show/'+ _this.customer_reference)
                .then(
                    (response) => {
                        _this.customer = response.data;
                    }
                );
        }
    },
})