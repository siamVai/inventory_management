var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        supplier_reference: window.supplier_reference,
        supplier : [],
        ledger : [],

        total_credit : 0,
        total_debit : 0,
    },
    created () {
        this.get_supplier_info();
        this.get_supplier_ledger();
    },
    methods : {
        get_supplier_ledger: function(){
            var _this = this;
            axios.get('/api/administration/reports/ledger/supplier/get', {
                params : {
                    supplier_reference: _this.supplier_reference
                }
            })
            .then(
                (response) => {
                    _this.ledger = response.data;

                    var total_payable = 0;
                    var total_paid = 0;
                    _this.ledger.forEach((value) => {
                        total_payable += parseFloat(value.credit);
                        total_paid += parseFloat(value.debit);
                    });
                    _this.total_credit = total_payable;
                    _this.total_debit = total_paid;
                }
            );
        },
        get_supplier_info: function(){
            var _this = this;
            axios.get('/api/administration/suppliers/show/'+ _this.supplier_reference)
                .then(
                    (response) => {
                        _this.supplier = response.data;
                    }
                );
        }
    },
})