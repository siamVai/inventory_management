var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        retailer_reference: window.retailer_reference,
        retailer : [],
        ledger : [],

        total_credit : 0,
        total_debit : 0,
    },
    created () {
        this.get_retailer_info();
        this.get_retailer_ledger();
    },
    methods : {
        get_retailer_ledger: function(){
            var _this = this;
            axios.get('/api/administration/reports/ledger/retailer/get', {
                params : {
                    retailer_reference: _this.retailer_reference
                }
            })
            .then(
                (response) => {
                    _this.ledger = response.data;

                    var total_payable = 0;
                    var total_paid = 0;
                    _this.ledger.forEach((value) => {
                        total_payable += parseFloat(value.credit);
                        total_paid += parseFloat(value.debit);
                    });
                    _this.total_credit = total_payable;
                    _this.total_debit = total_paid;
                }
            );
        },
        get_retailer_info: function(){
            var _this = this;
            axios.get('/api/administration/retailers/show/'+ _this.retailer_reference)
                .then(
                    (response) => {
                        _this.retailer = response.data;
                    }
                );
        }
    },
})