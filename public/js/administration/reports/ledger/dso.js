var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        dso_reference: window.dso_reference,
        dso : [],
        ledger : [],

        total_credit : 0,
        total_debit : 0,
    },
    created () {
        this.get_dso_info();
        this.get_dso_ledger();
    },
    methods : {
        get_dso_ledger: function(){
            var _this = this;
            axios.get('/api/administration/reports/ledger/dso/get', {
                params : {
                    dso_reference: _this.dso_reference
                }
            })
            .then(
                (response) => {
                    _this.ledger = response.data;

                    var total_payable = 0;
                    var total_paid = 0;
                    _this.ledger.forEach((value) => {
                        total_payable += parseFloat(value.credit);
                        total_paid += parseFloat(value.debit);
                    });
                    _this.total_credit = total_payable;
                    _this.total_debit = total_paid;
                }
            );
        },
        get_dso_info: function(){
            var _this = this;
            axios.get('/api/administration/dsos/show/'+ _this.dso_reference)
                .then(
                    (response) => {
                        _this.dso = response.data;
                    }
                );
        }
    },
})