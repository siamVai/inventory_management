var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        report : [],

        total_credit : 0,
        total_debit : 0,

        paginate : "50",  //Searching
        url : '/api/administration/reports/payable_receivable/get',
        pagination : [],
    },
    created () {
        this.get_rep();
    },
    methods : {
        get_rep: function(){
            var _this = this;
            axios.get('/api/administration/reports/payable_receivable/get', {
                params: {
                    paginate: this.paginate
                }
            })
            .then(
                (response) => {
                    _this.report = response.data;

                    _this.make_pagination(_this.report)
                }
            );
        },
        make_pagination: function(data){
            this.url = '/api/administration/reports/payable_receivable/get';
            const pagination = {
                current_page : data.current_page,
                last_page : data.last_page,
                next_page_url : data.next_page_url,
                prev_page_url : data.prev_page_url,
            }
            this.pagination = pagination;
        },
        fetch_paginate_data: function(url){
            this.url = url;
            this.get_rep();
        },
    },
    computed: {
        filterReport: function () {
            return this.report.data;
        }
    },
})