var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        stocks : [],
    },
    created () {
        this.get_stock();
    },
    methods : {
        get_stock: function(){
            var _this = this;
            axios.get('/api/administration/reports/stock/get')
            .then(
                (response) => {
                    console.log(response.data);
                    _this.stocks = response.data;
                    var total_purchsed = 0;
                    var total_sold = 0;
                }
            );
        }
    },
})