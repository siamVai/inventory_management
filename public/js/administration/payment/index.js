var app = new Vue({
    el: '#app',
    data: {
        date: moment().format('YYYY-MM-DD'),
        payment_to : 'supplier',
        object_reference : '',
        amount : '',
        desc : '',
        payment_type : 'cash',

        objects : [],

    },
    mounted: function() {
        $('#object_reference').select2().on('change', function() {
            app.object_reference = $(this).val();
        });
    },
    created () {
        // this.get_dealers();
        this.get_suppliers();
    },
    methods : {
        get_dealers: function(){
            var _this = this;
            axios.get('/api/administration/retailers/get')
                .then(
                    (response) => {
                        _this.objects = response.data;
                        if (response.data.length > 0){
                            // _this.object_reference = response.data[0].dealer_reference;
                        }
                    }
                );
        },
        get_suppliers: function(){
            var _this = this;
            axios.get('/api/administration/suppliers/get')
                .then(
                    (response) => {
                        _this.objects = response.data;
                        if (response.data.length > 0){
                            // _this.object_reference = response.data[0].supplier_reference;
                        }
                    }
                );
        },
        store : function(){
            if (!confirm('Are you sure about this PAYMENT?')) {
                return false;
            }
            var self = this;
            axios.post('/api/administration/payment/store', {
                date : self.date,
                object_reference : self.object_reference,
                amount : self.amount,
                desc : self.desc,
                payment_to : self.payment_to,
            })
                .then(
                    (response) => {
                        if (response.status == 201) {
                            alertify.success('Payment successfully paid', function (ev) {
                                ev.preventDefault();
                            });
                            this.clear_fields();
                        }else{
                            alertify.error(response.data, function (ev) {
                                ev.preventDefault();
                            });
                        }
                    }
                );
        },
        clear_fields : function () {
            var self = this;
            self.date = moment().format('YYYY-MM-DD');
            self.amount = '';
            self.desc = '';
            self.payment_to  = 'supplier';
        }
    },
    watch : {
        payment_to : function (val) {
            var self = this;
            if (val == 'supplier'){
                self.object_reference = '';
                self.get_suppliers();
            } else if (val == 'dealer'){
                self.object_reference = '';
                self.get_dealers();
            }
        }
    }

});