var app = new Vue({
    el: '#app',
    data: {

        date : moment().format("YYYY-MM-DD"),

        imei : '',
        imeis : [],
        trs: [],
        stock: [],

        subtotal : 0,
        discount : '',
        special_discount: '',
        paid : 0,
        due : 0,
        desc : '',

        btn_form_submit_disabled : false,
        disable_input_paid : false,
        disable_input_consumer : true,
        readonly_input_consumer_fields : false,

        //Consumer Section
        object_type: '3', //Customer
        objects: [], // Customers/Dealers
        object : '',

        consumer_name: '',
        consumer_phone: '',
        consumer_address: '',

    },
    mounted: function() {
        $('#object').select2().on('change', function() {
            app.object = $(this).val();
        });

        if (this.object_type == 3 || this.object_type == 2){
            this.disable_input_paid = true;
        } else{
            this.disable_input_paid = false;
        }
    },
    created () {
        this.focusIMEI();
        this.get_objects();
    },
    methods : {

        focusIMEI() {
            var self = this;
            Vue.nextTick(function () {
                self.$refs.imei.focus();
            });
        },
        get_objects: function(){
            var _this = this;
            axios.get('/api/administration/objects/show', {
                params : {
                    object_type_id : _this.object_type
                }
            })
                .then(
                    (response) => {
                        _this.objects = response.data;
                        _this.disable_input_consumer = false;
                    }
                );
        },
        
        addTr(imei) {

            var _this = this;
            var exist_in_table = false;
            _this.trs.forEach(val => {
                if (val.imei === imei){
                    exist_in_table = true;
                    return false;
                }
            });
            //if in table
            if (exist_in_table == true){
                alertify.error('Already exist in table', function (ev) {
                    ev.preventDefault();
                });
                return false
            }

            axios.get('/api/administration/stock/imei/purchased', {
                params : {
                    imei : imei
                }
            }).then(
                (response) => {
                    if (response.status === 200){
                        _this.stock = [];
                        _this.stock = response.data;

                        if (_this.stock.imei === imei) {
                            _this.trs.push({
                                sku: _this.stock.sku,
                                brand_name: _this.stock.brand_name,
                                product_name: _this.stock.product_name,
                                imei: imei,
                                qty: 1,
                                price: _this.stock.dealer_price,
                                line_total: 0
                            });
                            _this.imei = '';
                            _this.focusIMEI();
                            _this.calculateTotal();
                        }else{
                            alertify.error('This IMEI not exist in stock list', function (ev) {
                                ev.preventDefault();
                            });
                        }
                    }
                }
            );

        },
        deleteTr(index, tr) {
            var idx = this.trs.indexOf(tr);
            this.trs.splice(idx, 1);
            this.calculateTotal();
        },

        store: function(){
            var _this = this;
            if (_this.trs.length < 1){
                console.log(!_this.trs)
                alertify.error('No IMEI selected!', function (ev) {
                    ev.preventDefault();
                });
                _this.focusIMEI();
                return false;
            }

            if (_this.subtotal < 1){
                alertify.error('Total amount cannot be 0', function (ev) {
                    ev.preventDefault();
                });
                return false;
            }

            if (!confirm('Are you sure about this SALE?')) {
                return false;
            }
            _this.btn_form_submit_disabled = true; // make btn disabled when click

            // If customer is new we need to store the customer information first, then will proceed to sale.
            if (!_this.object) {
                var self = this;

                var consumer_type = null;
                if (_this.object_type === '3'){
                    consumer_type = 'customers';
                }else if (_this.object_type === '2'){
                    consumer_type = 'retailers';
                }else if (_this.object_type === '4'){
                    consumer_type = 'dsos';
                }
                axios.post('/api/administration/'+consumer_type+'/store', {
                    consumer_name : this.consumer_name,
                    consumer_phone : this.consumer_phone,
                    consumer_address : this.consumer_address,
                })
                    .then(
                        (response) => {
                            if (response.status === 201) {
                                self.object = response.data;
                                self.objects = [];
                                self.get_objects();
                                self.clear_customer_fields();
                                _this.store_sale();
                                // alertify.success('Customer added successfully', function (ev) {
                                //     ev.preventDefault();
                                // });

                            }else{
                                alertify.error(response.message, function (ev) {
                                    ev.preventDefault();
                                });
                            }
                        }
                    );

            }else{
                // If customer chosen from select option : nothing will change
                _this.store_sale();
            }


        },
        store_sale : function () {
            var _this = this;
            axios.post('/api/administration/sale/store', {
                object_type : _this.object_type,
                object : _this.object,
                date : _this.date,
                products : _this.trs,
                subtotal : _this.subtotal,
                discount : _this.discount,
                special_discount : _this.special_discount,
                paid : _this.paid,
                desc : _this.desc,
            })
                .then(
                    (response) => {
                        if (response.status === 201){
                            alertify.success('Sale successful', function (ev) {
                                ev.preventDefault();
                            });
                            _this.clear_fields();
                            _this.btn_form_submit_disabled = false; // make btn enabled when data saved and field are cleared
                            window.open('/administration/invoice/get?q='+response.data, '_blank');
                        }else{
                            alertify.error(response.status, function (ev) {
                                ev.preventDefault();
                            });
                        }
                    }
                );
        },

        calculateTotal() {
            var subtotal = 0;
            subtotal = this.trs.reduce(function (sum, product) {
                var price = parseFloat(product.price);
                if (!isNaN(price)) {
                    return sum + price;
                }
            }, 0);
            this.subtotal = subtotal.toFixed(2);

            this.paid = (subtotal - this.discount - this.special_discount).toFixed(2);
            this.due = (this.subtotal - this.discount - this.special_discount - this.paid).toFixed(2);

        },

        clear_fields : function () {
            var _this = this;

            // _this.supplier = '';
            _this.date = moment().format("YYYY-MM-DD");
            _this.trs = [];
            _this.subtotal = 0;
            _this.paid = 0;
            _this.due = 0;
            _this.desc = '';
        },
        clear_customer_fields : function () {
            this.consumer_name = '';
            this.consumer_phone = '';
            this.consumer_address = '';
        }
    },
    watch : {
        object_type : function (val) {
            var self = this;
            self.disable_input_consumer = true; //make the consumer disabled before load
            self.clear_customer_fields();

            if (val){
                self.object = "";
                self.get_objects();
            }
            this.disable_input_paid = val === '3' || val === '2';
            if (this.trs.length > 0) {
                this.calculateTotal();
            }
        },
        paid : function () {
            this.due = (this.subtotal - this.discount - this.special_discount - this.paid).toFixed(2);
        },
        discount : function () {
            this.calculateTotal();
        },
        special_discount : function () {
            this.calculateTotal();
        },
        object: function (val) {
            var _this = this;
            if (val){

                _this.readonly_input_consumer_fields = true;

                let obj;
                if (_this.object_type === '3'){ // Customer
                    obj = _this.objects.find(o => o.customer_reference === val);
                    if (obj){
                        _this.consumer_name = obj.customer_name;
                        _this.consumer_phone = obj.customer_phone;
                        _this.consumer_address = obj.customer_address;
                    }
                }else if (_this.object_type === '2'){ //Retailer
                    obj = _this.objects.find(o => o.dealer_reference === val);
                    if (obj){
                        _this.consumer_name = obj.dealer_name;
                        _this.consumer_phone = obj.dealer_phone;
                        _this.consumer_address = obj.dealer_address;
                    }
                }else if (_this.object_type === '4'){ //DSO
                    obj = _this.objects.find(o => o.dso_reference === val);
                    if (obj){
                        _this.consumer_name = obj.dso_name;
                        _this.consumer_phone = obj.dso_phone;
                        _this.consumer_address = obj.dso_address;
                    }
                }
            }else{
                _this.readonly_input_consumer_fields = false;

                _this.consumer_name = '';
                _this.consumer_phone = '';
                _this.consumer_address = '';
            }
        }
    }
});