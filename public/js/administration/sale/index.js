var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',

        object_type: '3', //Customer
        objects: [], // Customers/Dealers
        object : '',

        date : moment().format("YYYY-MM-DD"),

        products: [],

        trs: [{
            product_reference: '',
            imei: '',
            imeis: [],
            desc: '',
            qty: 1,
            price: '',
            line_total: 0
        }],
        subtotal : 0,
        paid : 0,
        due : 0,

        btn_form_submit_disabled : false,
    },
    mounted: function() {
        $('#object').select2().on('change', function() {
            app.object = $(this).val();
        });
        // $(".product_select2").select2().on('change', function() {
        //     console.log(this)
        //     app.trs.product_reference = $(this).val();
        // });
        // $(".imei_select_2").select2().on('change', function() {
        //     app.trs.imei = $(this).val();
        // });
    },
    created () {
        this.get_objects();
        this.get_products();
    },
    methods : {
        get_objects: function(){
            var _this = this;
            axios.get('/api/administration/objects/show', {
                params : {
                    object_type_id : _this.object_type
                }
            })
                .then(
                    (response) => {
                        _this.objects = response.data;
                    }
                );
        },
        get_products: function(){
            var _this = this;
            axios.get('/api/administration/products')
                .then(
                    (response) => {
                        _this.products = response.data;
                    }
                );
        },
        get_imeis: function(index, product_reference){

            console.log(index, product_reference)
            var _this = this;
            axios.get('/api/administration/stock/get/product', {
                params : {
                    product_reference : product_reference
                }
            })
                .then(
                    (response) => {
                        _this.trs[index].imeis = [];
                        _this.trs[index].imeis = response.data;
                    }
                );
        },

        addTr() {
            this.trs.push({
                product_reference: '',
                imei: '',
                imeis: [],
                desc: '',
                qty: 1,
                price: '',
                line_total: 0
            });
        },
        deleteTr(index, tr) {
            var idx = this.trs.indexOf(tr);
            if (idx > 0) {
                this.trs.splice(idx, 1);
            }
        },
        calculateLineTotal(tr) {
            var total = parseFloat(tr.qty) * parseFloat(tr.price);
            if (!isNaN(total)) {
                tr.line_total = total.toFixed(2);
            }
            this.calculateTotal();
        },
        calculateTotal() {
            var subtotal;
            subtotal = this.trs.reduce(function (sum, product) {
                var lineTotal = parseFloat(product.line_total);
                if (!isNaN(lineTotal)) {
                    return sum + lineTotal;
                }
            }, 0);
            this.subtotal = subtotal.toFixed(2);
            this.due = (this.subtotal - this.paid).toFixed(2);
        },

        clear_fields : function () {
            var _this = this;

            _this.supplier = '';
            _this.date = moment().format("YYYY-MM-DD");
            _this.trs = [{
                product_reference: '',
                imei: '',
                imeis: [],
                desc: '',
                qty: 1,
                price: '',
                line_total: 0
            }];
            _this.subtotal = 0;
            _this.paid = 0;
            _this.due = 0;
        },

        store : function () {
            if (!confirm('Are you sure about this SALE?')) {
                return false;
            }
            var _this = this;
            _this.btn_form_submit_disabled = true; // make btn disabled when click

            axios.post('/api/administration/sale/store', {
                object_type : _this.object_type,
                object : _this.object,
                date : _this.date,
                products : _this.trs,
                subtotal : _this.subtotal,
                paid : _this.paid,
            })
                .then(
                    (response) => {
                        if (response.status === 201){
                            alertify.success('Sale successful', function (ev) {
                                ev.preventDefault();
                            });
                            _this.clear_fields()
                            _this.btn_form_submit_disabled = false; // make btn enabled when data saved and field are cleared

                        }else{
                            alertify.error(response.status, function (ev) {
                                ev.preventDefault();
                            });
                        }
                    }
                );
        },
    },
    watch : {
        object_type : function (val) {
            var self = this;
            if (val){
                self.object = '';
                self.get_objects();
            }
        },
        paid : function () {
            this.due = (this.subtotal - this.paid).toFixed(2);
        }
    }
});