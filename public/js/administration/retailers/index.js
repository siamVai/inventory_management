var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',

        id: '',
        retailer_name: '',
        retailer_phone: '',
        retailer_address: '',

        retailers: [],

        update_status : false,
    },
    created () {
        this.get_retailers();
    },
    methods : {
        get_retailers: function(){
            var _this = this;
            axios.get('/api/administration/retailers/get')
                .then(
                    (response) => {
                        _this.retailers = response.data;
                    }
                );
        },
        store : function(){
            axios.post('/api/administration/retailers/store', {
                consumer_name : this.retailer_name,
                consumer_phone : this.retailer_phone,
                consumer_address : this.retailer_address,
            })
                .then(
                    (response) => {
                        console.log(response.status);
                        if (response.status == 201) {
                            this.get_retailers();
                            alertify.success('Dealers added successfully', function (ev) {
                                ev.preventDefault();
                            });
                            this.clear_fields();
                        }else{
                            alertify.error(response.message, function (ev) {
                                ev.preventDefault();
                            });
                        }
                    }
                );
        },
        edit : function(){

        },
        update : function(){

        },
        remove : function(){

        },

        clear_fields : function () {
            this.retailer_name = '';
            this.retailer_phone = '';
            this.retailer_address = '';

            this.update_status = false;
        }
    },
})