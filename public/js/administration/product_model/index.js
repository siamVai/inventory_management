var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',

        brands: [],
        brand: '1',

        id: '',
        name: '',
        models: [],

        update_status : false,
    },
    created () {
        this.get_brands();
        this.get_models();
    },
    methods : {
        get_brands: function(){
            var _this = this;
            axios.get('/api/administration/brands/get')
                .then(
                    (response) => {
                        _this.brands = response.data;
                    }
                );
        },
        get_models: function(){
            var _this = this;
            axios.get('/api/administration/models/get')
                .then(
                    (response) => {
                        _this.models = response.data;
                    }
                );
        },

        store : function(){
            axios.post('/api/administration/models/store', {
                brand : this.brand,
                name : this.name,
            })
                .then(
                    (response) => {
                        if (response.status == 201) {
                            this.get_models();
                            alertify.success('Model added successfully', function (ev) {
                                ev.preventDefault();
                            });
                            this.clear_fields();
                        }else{
                            alertify.error(response.data, function (ev) {
                                ev.preventDefault();
                            });
                        }
                    }
                );
        },
        edit : function(){

        },
        update : function(){

        },
        remove : function(){

        },

        clear_fields : function () {
            this.name = '';
            this.update_status = false;
        }
    }
})