var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        id: '',
        purchase: '',
        imei: '',
        search_imei: '',

        purchases: [],

        stocks: [],
        paginate : "25",  //Searching
        url : '/api/administration/stock/get',
        pagination : [],

        update_status : false,
    },
    created () {
        this.focusIMEI();
        this.get_purchases();
        this.get_stocks();
    },
    methods : {

        focusIMEI() {
            var self = this;
            Vue.nextTick(function () {
                self.$refs.imei.focus();
            });
        },
        get_purchases: function(){
            var _this = this;
            axios.get('/api/administration/purchase/get')
                .then(
                    (response) => {
                        _this.purchases = response.data;
                    }
                );
        },
        get_stocks: function(){
            var _this = this;
            axios.get(_this.url, {
                params : {
                    item_qty : _this.paginate,
                    imei : _this.search_imei,
                }
            })
                .then(
                    (response) => {
                        _this.stocks = response.data;
                        _this.make_pagination(_this.stocks);
                    }
                );
        },
        make_pagination: function(data){
            //this.url = '/api/administration/purchase/get';
            const pagination = {
                current_page : data.current_page,
                last_page : data.last_page,
                next_page_url : data.next_page_url,
                prev_page_url : data.prev_page_url,
            }
            this.pagination = pagination;
        },
        fetch_paginate_data: function(url){
            this.url = url;
            this.get_stocks();
        },

        store : function(){
            axios.post('/api/administration/stock/store', {
                purchase : this.purchase,
                imei : this.imei,
            })
                .then(
                    (response) => {
                        if (response.status === 201) {
                            this.get_stocks();
                            alertify.success( this.imei +' added successfully', function (ev) {
                                ev.preventDefault();
                            });
                            this.focusIMEI();
                            this.clear_fields();
                        }else{
                            alertify.error(response.data, function (ev) {
                                ev.preventDefault();
                            });
                            this.clear_fields();
                            this.focusIMEI();
                        }
                    }
                );
        },
        edit : function(stock){
            console.log(stock);
            this.update_status = true;
            this.id = stock.id;
            this.imei = stock.imei;
            this.purchase = stock.ledger_reference;
        },
        update : function(){
            axios.post('/api/administration/stock/update', {
                stock_id : this.id,
                purchase : this.purchase,
                imei : this.imei,
            })
                .then(
                    (response) => {
                        if (response.status === 202) {
                            this.get_stocks();
                            alertify.success( this.imei +' updated successfully', function (ev) {
                                ev.preventDefault();
                            });
                            this.clear_fields();
                        }else{
                            alertify.error(response.data, function (ev) {
                                ev.preventDefault();
                            });
                        }
                        this.focusIMEI();
                    }
                );
        },
        remove : function(){

        },

        clear_fields : function () {
            this.update_status = false;
            this.imei = '';
            this.focusIMEI();
        }
    },
    computed: {
        filteredStocks() {
            var _this = this;
            return _this.stocks.data;
        },
    },
    watch: {
        search_imei: function (val) {
            if (!val){
                this.get_stocks();
            }
        }
    }
});