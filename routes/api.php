<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\Administration\Stock;
use App\Http\Controllers\API\Administration\Distribution;

use App\Http\Controllers\API\Administration\Reports\Sale\Sale;
use App\Http\Controllers\API\Administration\Reports\Purchase\Purchase;
use App\Http\Controllers\API\Administration\Reports\PayableReceivable\PayableReceivable;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Passport@login');
Route::post('register', 'Passport@register');

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//ADMINISTRATION
Route::group(['middleware'=> 'auth:api'], function () {

    //All Colors
    Route::group(['prefix' => 'colors'], function () {
        Route::get('/get', 'API\\Colors@get');
    });

    Route::group(['prefix' => 'administration'], function () {

        //Objects
        Route::group(['prefix' => '/objects'], function () {
            Route::get('/show', 'API\\Administration\\Objects@show_objects');
        });

        //Suppliers
        Route::group(['prefix' => '/suppliers'], function () {
            Route::get('/get', 'API\\Administration\\Suppliers@get');
            Route::get('/show/{supplier_reference}', 'API\\Administration\\Suppliers@show');
            Route::post('/store', 'API\\Administration\\Suppliers@store');
        });

        //retailers
        Route::group(['prefix' => '/retailers'], function () {
            Route::get('/get', 'API\\Administration\\Retailers@get');
            Route::get('/show/{retailer_reference}', 'API\\Administration\\Retailers@show');
            Route::post('/store', 'API\\Administration\\Retailers@store');
        });

        //Customers
        Route::group(['prefix' => '/customers'], function () {
            Route::get('/get', 'API\\Administration\\Customers@get');
            Route::get('/show/{customer_reference}', 'API\\Administration\\Customers@show');
            Route::post('/store', 'API\\Administration\\Customers@store');
        });

        //DSOs
        Route::group(['prefix' => '/dsos'], function () {
            Route::get('/get', 'API\\Administration\\DSOs@get');
            Route::get('/show/{dso_reference}', 'API\\Administration\\DSOs@show');
            Route::post('/store', 'API\\Administration\\DSOs@store');
        });

        //products
        Route::group(['prefix' => '/products'], function () {
            Route::get('/', 'API\\Administration\\Products@get');
            Route::get('/show/{product_reference}', 'API\\Administration\\Products@show');
            Route::post('/store', 'API\\Administration\\Products@store');
        });

        //brands
        Route::group(['prefix' => 'brands'], function () {
            Route::get('/get', 'API\\Administration\\Brands@get');
        });

        //models
        Route::group(['prefix' => 'models'], function () {
            Route::get('/get', 'API\\Administration\\Models@get');
            Route::post('/store', 'API\\Administration\\Models@store');
        });

        //PURCHASE
        Route::group(['prefix' => '/purchase'], function () {
            Route::get('/get', 'API\\Administration\\Purchase@get');
            Route::get('/total_purchased', 'API\\Administration\\Purchase@total_purchased');
            Route::post('/store', 'API\\Administration\\Purchase@store');
        });

        //SALE
        Route::group(['prefix' => '/sale'], function () {
            Route::get('/get', 'API\\Administration\\Sale@get');
            Route::get('/total_sold', 'API\\Administration\\Sale@total_sold');

            Route::post('/store', 'API\\Administration\\Sale@store');
        });

        //STOCKS
        Route::group(['prefix' => '/stock'], function () {
            Route::get('/get', 'API\\Administration\\Stock@get');
            Route::get('/imei/sold', 'API\\Administration\\Stock@imei_sold');
            Route::get('/imei/sold-to-dso', [Stock::class, 'imei_sold_to_dso']);
            Route::get('/imei/purchased', 'API\\Administration\\Stock@imei_purchased');
            Route::get('/get/product', 'API\\Administration\\Stock@get_by_product');
            Route::post('/store', 'API\\Administration\\Stock@store');
            Route::post('/update', 'API\\Administration\\Stock@update');

            Route::get('/all', 'API\\Administration\\Stock@all');
            Route::get('/groupby', 'API\\Administration\\Stock@group');
        });

        //PAYMENT
        Route::group(['prefix' => '/payment'], function () {
            Route::post('/store', 'API\\Administration\\Payment@store');
        });

        //RECEIVE CASH
        Route::group(['prefix' => '/receive'], function () {
            Route::post('/store', 'API\\Administration\\Receive@store');
        });

        //RETURN
        Route::group(['prefix' => '/return'], function () {
            Route::post('/sale/store', 'API\\Administration\\ReturnProduct@store_sale_return');
            Route::post('/purchase/store', 'API\\Administration\\ReturnProduct@store_purchase_return');
        });

        //DISTRIBUTION
        Route::group(['prefix' => '/distribution'], function () {
            Route::get('/issued-product', [Distribution::class, 'issued']);
            Route::get('/issued-product-list', [Distribution::class, 'issued_all']);
            Route::get('/issued-product-list/dso', [Distribution::class, 'issued_dso']);
            Route::post('/issue', [Distribution::class, 'issue']);
            Route::post('/withdraw', [Distribution::class, 'withdraw']);
        });

        //REPORTS
        Route::group(['prefix' => '/reports'], function () {

            //Ledger
            Route::group(['prefix' => '/ledger'], function () {

                //Supplier
                Route::group(['prefix' => '/supplier'], function () {
                    Route::get('/get', 'API\\Administration\\Reports\\Ledger\\Supplier@get');
                });

                //Retailer
                Route::group(['prefix' => '/retailer'], function () {
                    Route::get('/get', 'API\\Administration\\Reports\\Ledger\\Retailer@get');
                });

                //Customer
                Route::group(['prefix' => '/customer'], function () {
                    Route::get('/get', 'API\\Administration\\Reports\\Ledger\\Customer@get');
                });

                //DSO
                Route::group(['prefix' => '/dso'], function () {
                    Route::get('/get', 'API\\Administration\\Reports\\Ledger\\DSO@get');
                });

                //Product
                Route::group(['prefix' => '/product'], function () {
                    Route::get('/get', 'API\\Administration\\Reports\\Ledger\\Product@get');
                });

            });

            //Cashbook
            Route::get('/cashbook/get', 'API\\Administration\\Reports\\Cashbook\\Cashbook@get');

            Route::group(['prefix' => '/stock'], function () {
                Route::get('/get', 'API\\Administration\\Reports\\Stock\\Stock@get');
            });

            Route::group(['prefix' => '/sale'], function () {
                Route::get('/get', [Sale::class, 'get']);
            });
            Route::group(['prefix' => '/purchase'], function () {
                Route::get('/get', [Purchase::class, 'get']);
            });

            Route::group(['prefix' => '/payable_receivable'], function () {
                Route::get('/get', [PayableReceivable::class, 'get']);
            });

        });

        Route::group(['prefix' => '/invoice'], function () {
            Route::get('/all', [\App\Http\Controllers\API\Administration\Invoice\Invoice::class, 'all']);
            Route::get('/get', [\App\Http\Controllers\API\Administration\Invoice\Invoice::class, 'get']);
            Route::get('/details', [\App\Http\Controllers\API\Administration\Invoice\Invoice::class, 'details']);
        });

    });
});


