<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Artisan;

//Administration
use App\Http\Controllers\Administration\Administration;
use App\Http\Controllers\Administration\ProductModel;
use App\Http\Controllers\Administration\Products;
use App\Http\Controllers\Administration\Purchase;
use App\Http\Controllers\Administration\Sale;
use App\Http\Controllers\Administration\Suppliers;
use App\Http\Controllers\Administration\Retailers;
use App\Http\Controllers\Administration\Customer;
use App\Http\Controllers\Administration\DSO;
use App\Http\Controllers\Administration\Stock;
use App\Http\Controllers\Administration\Payment;
use App\Http\Controllers\Administration\Receive;

use App\Http\Controllers\Administration\Invoice;

use App\Http\Controllers\Administration\Reports\Ledger;
use App\Http\Controllers\Administration\Reports\Cashbook;
use App\Http\Controllers\Administration\Reports\Sale as SaleReport;
use App\Http\Controllers\Administration\Reports\Purchase as PurchaseReport;
use App\Http\Controllers\Administration\Reports\Stock as StockReport;
use App\Http\Controllers\Administration\Reports\PayableReceivable;

//returns
use App\Http\Controllers\Administration\ReturnProduct;

//Distribution
use App\Http\Controllers\Administration\Distribution;

//Dealer
use App\Http\Controllers\Dealer\Dealer;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/config-cache', function() { Artisan::call('config:cache'); return 'Config cached';});
Route::get('/route-cache', function() { Artisan::call('route:cache'); return 'Route cached'; });
Route::get('/view-cache', function() { Artisan::call('view:cache'); return 'View cached'; });
Route::get('/clear-cache', function() { Artisan::call('cache:clear'); return 'Application cache cleared'; });
Route::get('/view-clear', function() { Artisan::call('view:clear'); return 'View cache cleared'; });

Route::get('/', function () { return view('auth.login'); })->middleware('guest');

Auth::routes(['register' => false]);

Route::group(['middleware' => 'auth'], function () {

    Route::get('/home', [HomeController::class, 'index'])->name('home');

    //ADMINISTRATION
    Route::group(['prefix' => 'administration/', 'middleware' => 'can:admin,admin_employee'], function () {

        Route::get('/home', [Administration::class, 'index'])->name('administration.home');
        Route::get('/purchase', [Purchase::class, 'index'])->name('administration.purchase');
        Route::get('/sale', [Sale::class, 'index'])->name('administration.sale');
        Route::get('/products', [Products::class, 'index'])->name('administration.products');
        Route::get('/product/models', [ProductModel::class, 'index'])->name('administration.product.models');
        Route::get('/suppliers', [Suppliers::class, 'index'])->name('administration.suppliers');
        Route::get('/retailers', [Retailers::class, 'index'])->name('administration.retailers');
        Route::get('/customers', [Customer::class, 'index'])->name('administration.customers');
        Route::get('/dso', [DSO::class, 'index'])->name('administration.dso');
        Route::get('/stock', [Stock::class, 'index'])->name('administration.stock');

        //Invoice
        Route::group(['prefix' => '/invoice'], function () {
            Route::get('/', [Invoice::class, 'index'])->name('administration.invoice');
            Route::get('/get', [Invoice::class, 'get'])->name('administration.get');
        });
        //Payment
        Route::group(['prefix' => '/payment'], function () {
            Route::get('/', [Payment::class, 'index'])->name('administration.payment');
        });
        //Receive Cash
        Route::group(['prefix' => '/receive'], function () {
            Route::get('/', [Receive::class, 'index'])->name('administration.receive');
        });

        //Return Sale/Purchase
        Route::group(['prefix' => '/return'], function () {
            Route::get('/sale', [ReturnProduct::class, 'return_sale'])->name('administration.return.sale');
            Route::get('/purchase', [ReturnProduct::class, 'return_purchase'])->name('administration.return.purchase');
        });

        //SR/Dealer issue/re-issue product
        Route::group(['prefix' => '/distribution'], function () {
            Route::get('/issue-product', [Distribution::class, 'issue'])->name('administration.distribution.issue');
            Route::get('/withdraw-product', [Distribution::class, 'withdraw'])->name('administration.distribution.withdraw');
        });

        //Reports
        Route::group(['prefix' => '/reports'], function () {
            Route::get('/', [\App\Http\Controllers\Administration\Reports\Dashboard::class, 'index'])->name('ad.reports');

            Route::group(['prefix' => 'ledger/'], function () {
                Route::get('/supplier', [Ledger::class, 'supplier'])->name('ad.reports.ledger.supplier');
                Route::get('/customer', [Ledger::class, 'customer'])->name('ad.reports.ledger.customer');
                Route::get('/retailer', [Ledger::class, 'retailer'])->name('ad.reports.ledger.retailer');
                Route::get('/dso', [Ledger::class, 'dso'])->name('ad.reports.ledger.dso');
                Route::get('/product', [Ledger::class, 'product'])->name('ad.reports.ledger.product');
            });
            Route::get('/cashbook', [Cashbook::class, 'index'])->name('ad.reports.cashbook');
            Route::group(['prefix' => '/stock'], function () {
                Route::get('/', [StockReport::class, 'index'])->name('ad.reports.stock');
            });
            Route::get('/sale', [SaleReport::class, 'index'])->name('ad.reports.sale');
            Route::get('/purchase', [PurchaseReport::class, 'index'])->name('ad.reports.purchase');
            Route::get('/payable-receivable', [PayableReceivable::class, 'index'])->name('ad.reports.payable-receivable');

        });

    });

    //DEALER
    Route::group(['prefix' => 'dealer/'], function () {
        Route::get('/home', [Dealer::class, 'index'])->name('dealer.home');
    });

});