<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class Resource {

    public static function suppliers (){
        return DB::table('suppliers')->select('supplier_reference', 'supplier_name')->get();
    }
    public static function retailers (){
        return DB::table('dealers')->select('dealer_reference', 'dealer_name')->get();
    }
    public static function customers (){
        return DB::table('customers')->select('customer_reference', 'customer_name')->get();
    }
    public static function dsos (){
        return DB::table('dsos')->select('dso_reference', 'dso_name')->get();
    }
    public static function products (){
        return DB::table('products')->select('product_reference', 'product_name')->get();
    }

}