<?php

namespace App\Services;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class Generator {

    public function reference (){
        return (string) Str::uuid();
    }
    public function sku (){
        $str = Str::random('8');
        $str = strtoupper($str);
        $validator = Validator::make(
            ['str'=>$str],
            ['str'=>'unique:stocks,sku']
        );
        if($validator->fails()){
            return $this->sku();
        }
        return $str;
    }
    public function invoice (){
        $str = Str::random('8');
        $str = strtoupper($str);
        $validator = Validator::make(
            ['str'=>$str],
            ['str'=>'unique:invoice,invoice_id']
        );
        if($validator->fails()){
            return $this->invoice();
        }
        return $str;
    }

}