<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        Gate::define('admin', function ($user) {
            return $user->role == 'administrator';
        });
        Gate::define('dealer', function ($user) {
            return $user->role == 'dealer';
        });
        Gate::define('admin_employee', function ($user) {
            return $user->role == 'employee' && $user->parent_role == 'administrator';
        });
        Gate::define('dealer_employee', function ($user) {
            return $user->role == 'employee' && $user->parent_role == 'dealer';
        });
    }
}
