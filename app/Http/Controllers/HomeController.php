<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Gate::allows('admin') || Gate::allows('admin_employee')){
            return redirect('/administration/home');
        }
        if (Gate::allows('dealer') || Gate::allows('dealer_employee')){
            return redirect('/dealer/home');
        }
        return view('welcome');
    }
}
