<?php

namespace App\Http\Controllers\Administration\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Dashboard extends Controller
{
    public function index(){
        return view('administration.reports.index');
    }
}
