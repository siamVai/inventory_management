<?php

namespace App\Http\Controllers\Administration\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PayableReceivable extends Controller
{
    public function index(){
        return view('administration.reports.payable_receivable.index');
    }
}
