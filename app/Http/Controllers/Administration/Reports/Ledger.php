<?php

namespace App\Http\Controllers\Administration\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Ledger extends Controller
{
    public function supplier(Request $request){
        return view('administration.reports.ledger.supplier')->with('supplier_reference', $request->supplier ?? '');
    }
    public function retailer(Request $request){
        return view('administration.reports.ledger.retailer')->with('retailer_reference', $request->retailer ?? '');
    }
    public function customer(Request $request){
        return view('administration.reports.ledger.customer')->with('customer_reference', $request->customer ?? '');
    }
    public function dso(Request $request){
        return view('administration.reports.ledger.dso')->with('dso_reference', $request->dso ?? '');
    }
    public function product(Request $request){
        return view('administration.reports.ledger.product')->with('product_reference', $request->product ?? '');
    }
}
