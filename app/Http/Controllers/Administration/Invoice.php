<?php

namespace App\Http\Controllers\Administration;

use App\Http\Controllers\Controller;
use App\Services\Generator;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class Invoice extends Controller
{
    public function index(){
        return view('administration.invoice.index');
    }
    public function get(Request $request){
        return view('administration.invoice.get')->with('invoice_id', $request->q);
    }
}
