<?php

namespace App\Http\Controllers\Administration;

use App\Http\Controllers\Controller;
use App\Services\Generator;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class Distribution extends Controller
{
    public function issue(){
        return view('administration.distribution.issue');
    }
    public function withdraw(){
        return view('administration.distribution.withdraw');
    }
}
