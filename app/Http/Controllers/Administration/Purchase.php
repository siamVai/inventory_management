<?php

namespace App\Http\Controllers\Administration;

use App\Http\Controllers\Controller;
use App\Services\Generator;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class Purchase extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        #$generate = new Generator();
        #dd($generate->reference());
        if (Gate::allows('admin') || Gate::allows('admin_employee')){
            return view('administration.purchase.index');
        }
        return redirect('/');
    }
}
