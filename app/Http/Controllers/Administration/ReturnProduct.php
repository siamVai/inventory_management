<?php

namespace App\Http\Controllers\Administration;

use App\Http\Controllers\Controller;
use App\Services\Generator;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class ReturnProduct extends Controller
{
    public function return_sale(){
        return view('administration.return.sale.index');
    }
    public function return_purchase(){
        return view('administration.return.purchase.index');
    }
}
