<?php

namespace App\Http\Controllers\Administration;

use App\Http\Controllers\Controller;
use App\Services\Generator;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class Payment extends Controller
{
    public function index(){
        if (Gate::allows('admin') || Gate::allows('admin_employee')){
            return view('administration.payment.index');
        }
        return redirect('/');
    }
}
