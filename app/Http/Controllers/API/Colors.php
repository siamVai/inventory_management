<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Colors extends Controller
{
    public function get()
    {
        $colors = DB::table('colors')->get();
        return response()->json($colors, 200);
    }
}
