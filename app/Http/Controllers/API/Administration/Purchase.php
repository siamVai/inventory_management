<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\Generator;

class Purchase extends Controller
{
    public function get()
    {
        $purchase = DB::table('ledger')
            ->join('object_types', 'object_types.id', '=', 'ledger.object_type_id')
            ->where('object_types.product', '=', 1) // if Product
            ->where('ledger.stock_in', '>', 0) // if product purchased / Stock IN
            ->whereColumn('ledger.stock_added', '<', 'ledger.stock_in') // if stock_in qty not added in stock yet
            ->select('ledger.*')
            ->orderByDesc('created_at')
            ->get();
        return response()->json($purchase, 200);
    }

    public function total_purchased()
    {
        $purchase = DB::table('ledger')
            ->join('object_types', 'object_types.id', '=', 'ledger.object_type_id')
            ->where('object_types.product', '=', 1) // if Product
            ->where('ledger.action_name', '=', 'purchase') // if product purchased
            ->sum('debit');
        return response()->json($purchase, 200);
    }

    public function store(Request $request){
        $this->validate($request, [
            'supplier' => 'required',
            'products' => 'required',
            'date' => 'required',
        ]);

        try {

            $generate = new Generator();
            $ps_reference = $generate->reference();

            DB::beginTransaction();

            $supplier = DB::table('suppliers')->where('supplier_reference', '=', $request->supplier)->first();
            if ($supplier == null){
                return response()->json('Supplier unavailable', 404);
            }

            $total_qty = 0;
            $subtotal = $request->subtotal ?? 0;

            //Product End
            foreach ($request->products as $val){

                $product = DB::table('products')->where('product_reference', '=', $val['product_reference'])->first();
                if ($product == null){
                    return response()->json('Product unavailable', 404);
                }

                if (isset($val['desc'])){
                    $product_comment = 'Purchased from '.$supplier->supplier_name.' ('. 'Qty '.$val['qty'].'; Price '. $subtotal. 'Tk) - ('.$val['desc'].')';
                }else{
                    $product_comment = 'Purchased from '.$supplier->supplier_name.' ('. 'Qty '.$val['qty'].'; Price '. $subtotal. 'Tk)';
                }

                DB::table('ledger')->insert([
                    'ledger_reference' => $generate->reference() ?? '',
                    'ps_reference' => $ps_reference,
                    'object_type_id' => 101, //Mobile Phone
                    'object_reference' => $val['product_reference'] ?? '',
                    'object_name' => $product->product_name ?? '',
                    'credit' => 0,
                    'debit' => $val['line_total'] ?? 0,
                    'stock_out' => 0,
                    'stock_in' => $val['qty'] ?? 0,
                    'unit_price' => $val['price'] ?? 0,
                    'dealer_price' => $val['dealer_price'] ?? 0,
                    'comment' => $product_comment,
                    'consumer_reference' => $supplier->supplier_reference ?? '',
                    'consumer_name' => $supplier->supplier_name ?? '',
                    'date' => $request['date'] ?? Carbon::now()->format('Y-m-d'),
                    'action_name' => 'purchase',
                    'auth_id' => Auth::id(),
                ]);

                DB::table('products')->where('product_reference', '=', $val['product_reference'])->increment('purchase_qty', $val['qty']);

                $total_qty += $val['qty'] ?? 0;
            }

            //Supplier End
            $supplier_ledger_reference = $generate->reference() ?? '';
            DB::table('ledger')->insert([
                'ledger_reference' => $supplier_ledger_reference,
                'ps_reference' => $ps_reference,
                'object_type_id' => 1, //Supplier
                'object_reference' => $supplier->supplier_reference ?? '',
                'object_name' => $supplier->supplier_name ?? '',
                'credit' => $request->subtotal ?? 0,
                'debit' => 0,
                'stock_out' => 0,
                'stock_in' => 0,
                'unit_price' => 0,
                'comment' => 'Purchased Qty '.$total_qty.', Price '. $subtotal. 'Tk',
                'date' => $request->date ?? Carbon::now()->format('Y-m-d'),
                'action_name' => 'purchase',
                'auth_id' => Auth::id(),
            ]);

            //Invoice
            $invoice_reference = $generate->reference() ?? '';
            $invoice_id = $generate->invoice() ?? '';
            DB::table('invoice')->insert([
                'invoice_reference' => $invoice_reference ?? '',
                'ps_reference' => $ps_reference ?? '',
                'invoice_id' => $invoice_id ?? '',
                'consumer_reference' => $supplier->supplier_reference ?? '',
                'consumer_name' => $supplier->supplier_name ?? '',
                'object_type_id' => 1,
                'ledger_reference' => $supplier_ledger_reference ?? '',
                'price' => $request->subtotal ?? 0,
                'comment' => 'Purchased Qty '.$total_qty.', Price '. $subtotal. 'Tk',
                'action_name' => 'purchase',
                'auth_id' => Auth::id(),
                'created_at' => Carbon::now(),
            ]);

            DB::commit();

            //Send the invoice_reference as return data to generate invoice;
            return response()->json($invoice_id, 201);

        }catch (\Exception $exception){
            DB::rollBack();
            return response()->json($exception, 500);
        }
    }
}
