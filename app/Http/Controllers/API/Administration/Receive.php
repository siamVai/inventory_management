<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\Generator;
use Illuminate\Support\Facades\Validator;

class Receive extends Controller
{
    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'payment_from' => 'required',
            'object_reference' => 'required',
            'date' => 'required',
            'amount' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->getMessageBag()->first());
        }

        if ($request->amount < 1){
            return response()->json('Amount must be valid!');
        }

        $amount = $request->amount ?? 0;
        $desc = $request->desc ?? 'No desc';

        try {

            $generate = new Generator();

            DB::beginTransaction();

            $object_type_id = null;

            if ($request->payment_from == 'customer'){
                $object = DB::table('customers')->where('customer_reference', '=', $request->object_reference)->first();
                if ($object == null){
                    return response()->json('Customer unavailable');
                }
                $object_name = $object->customer_name ?? '';
                $object_type_id = 3;

            }elseif ($request->payment_from == 'supplier'){
                $object = DB::table('suppliers')->where('supplier_reference', '=', $request->object_reference)->first();
                if ($object == null){
                    return response()->json('Supplier unavailable');
                }
                $object_name = $object->supplier_name ?? '';
                $object_type_id = 1;

            }elseif ($request->payment_from == 'dealer'){
                $object = DB::table('dealers')->where('dealer_reference', '=', $request->object_reference)->first();
                if ($object == null){
                    return response()->json('Dealer unavailable');
                }
                $object_name = $object->dealer_name ?? '';
                $object_type_id = 2;

            }
            elseif ($request->payment_from == 'dso'){
                $object = DB::table('dsos')->where('dso_reference', '=', $request->object_reference)->first();
                if ($object == null){
                    return response()->json('DSO unavailable');
                }
                $object_name = $object->dso_name ?? '';
                $object_type_id = 2;

            }
            else{
                return response()->json('No Receiver found', 404);
            }

            //Customer/Supplier/Dealer End
            DB::table('ledger')->insert([
                'ledger_reference' => $generate->reference() ?? '',
                'ps_reference' => '',
                'object_type_id' => $object_type_id,
                'object_reference' => $request->object_reference ?? '',
                'object_name' => $object_name ?? '',
                'credit' => $request->amount ??0,
                'debit' =>  0,
                'stock_out' => 0,
                'stock_in' => 0,
                'unit_price' => 0,
                'comment' => 'Received '.$amount.'tk from '.$object_name. ' ('.$desc.')',
                'date' => $request->date ?? Carbon::now()->format('Y-m-d'),
                'action_name' => 'receive',
                'auth_id' => Auth::id(),
            ]);

            DB::commit();
            return response()->json('Created', 201);

        }catch (\Exception $exception){
            DB::rollBack();
            return response()->json($exception);
        }
    }
}
