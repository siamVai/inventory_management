<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\Generator;

class Products extends Controller
{
    public function get()
    {
        $products = DB::table('products')->get();
        return response()->json($products, 200);
    }
    public function show($product_reference)
    {
        $product = DB::table('products')->where('product_reference', '=', $product_reference)->first();
        return response()->json($product, 200);
    }
    public function store(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'model' => 'required',
        ]);

        try {
            $generate = new Generator();
            DB::beginTransaction();

            $brand = DB::table('brands')->where('id', '=', $request->brand)->first();
            if ($brand == null){
                return response()->json('Brand not found');
            }

            $model = DB::table('models')->where('model_name', '=', $request->model)->first();
            if ($model == null){
                return response()->json('Model not found');
            }

            DB::table('products')->insert([
                'object_type_id' => 101,
                'object_type_name' => 'mobile_phone',
                'product_reference' => $generate->reference() ?? '',
                'product_name' => $request->name ?? '',
                'category_id' => 1,
                'category_name' => 'Mobile phone',
                'brand_id' => $request->brand ?? '',
                'brand_name' => $brand->brand_name ?? '',
                'model_id' => $model->id ?? 0,
                'model_name' => $request->model ?? '',
                'color' => $request->color ?? '',
//                'ram' => $request->ram ?? '',
//                'rom' => $request->rom ?? '',
                'auth_id' => Auth::id(),
            ]);
            DB::commit();
            return response()->json('Created', 201);

        }catch (\Exception $exception){
            DB::rollBack();
            return response()->json($exception, 500);
        }
    }
}
