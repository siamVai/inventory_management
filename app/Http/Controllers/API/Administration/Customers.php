<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\Generator;

class Customers extends Controller
{
    public function get()
    {
        $customers = DB::table('customers')->orderByDesc('created_at')->get();
        return response()->json($customers, 200);
    }
    public function show($customer_reference)
    {
        $customer = DB::table('customers')->where('customer_reference', '=', $customer_reference)->first();
        return response()->json($customer, 200);
    }
    public function store(Request $request){
        $this->validate($request, [
            'consumer_name' => 'required'
        ]);

        try {
            $generate = new Generator();
            $reference = $generate->reference() ?? '';
            DB::beginTransaction();

            DB::table('customers')->insert([
                'customer_reference' => $reference,
                'customer_name' => $request->consumer_name ?? '',
                'customer_phone' => $request->consumer_phone ?? '',
                'customer_address' => $request->consumer_address ?? '',
                'auth_id' => Auth::id(),
            ]);
            DB::commit();
            return response()->json($reference, 201);

        }catch (\Exception $exception){
            DB::rollBack();
            return response()->json($exception, 500);
        }
    }
}
