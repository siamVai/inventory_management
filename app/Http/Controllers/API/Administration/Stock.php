<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\Generator;
use Illuminate\Support\Facades\Validator;

class Stock extends Controller
{

    public function get(Request $request)
    {
        $paginate = $request->item_qty ?? 25;
        $stock = DB::table('stocks')
            ->where('stock_status', '=', 'stock')
            ->where('imei', 'LIKE', '%'.$request->imei.'%')
            ->orderByDesc('created_at')
            ->paginate($paginate);
        return response()->json($stock, 200);
    }

    public function imei_sold(Request $request)
    {
        $stock = DB::table('stocks')
            ->join('ledger', 'ledger.sku', '=', 'stocks.sku')
            ->where('stocks.stock_status', '=', 'sold')
            ->where('stocks.imei', '=', $request->imei)
            ->select('stocks.sku', 'stocks.imei', 'stocks.product_name', 'ledger.ledger_reference', 'ledger.consumer_name', 'ledger.credit as price')
            ->first();
        return response()->json($stock, 200);
    }

    public function imei_sold_to_dso(Request $request)
    {
        $stock = DB::table('stocks')
            ->join('ledger', 'ledger.sku', '=', 'stocks.sku')
            ->where('stocks.stock_status', '=', 'sold')
            ->where('stocks.imei', '=', $request->imei)
            ->where('stocks.stock_dso_reference', '=', $request->dso_reference)
            ->select('stocks.sku', 'stocks.imei', 'stocks.product_name', 'ledger.ledger_reference', 'ledger.consumer_name')
            ->first();
        return response()->json($stock, 200);
    }

    public function imei_purchased(Request $request)
    {
        $stock = DB::table('stocks')
            ->join('products', 'products.product_reference', '=','stocks.product_reference')
            ->where('stock_status', '=', 'stock')
            ->where('imei', '=', $request->imei)
            ->select('stocks.*', 'products.brand_name')
            ->first();
        return response()->json($stock, 200);
    }

    public function get_by_product(Request $request)
    {
        $stock = DB::table('stocks')
            ->where('stock_status', '=', 'stock')
            ->where('product_reference', '=', $request->product_reference)
            ->orderByDesc('created_at')
            ->get();
        return response()->json($stock, 200);
    }

    public function store(Request $request){

        //return $request->all();

        $validator = Validator::make($request->all(), [
            'imei' => 'required|unique:stocks',
            'purchase' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->getMessageBag()->first());
        }

        try {

            $generate = new Generator();

            DB::beginTransaction();

            $ledger = DB::table('ledger')->where('ledger_reference', '=', $request->purchase)->first();
            if ($ledger == null) {
                return response()->json('Transaction unavailable', 404);
            }

            $stock = DB::table('stocks')->where('ledger_reference', '=', $request->purchase)->count();
            if ($ledger->stock_in <= $stock){
                return response()->json('Product stock FULL for the selected transaction.');
            }

            DB::table('stocks')->insert([
                'sku' => $generate->sku() ?? '',
                'ledger_reference' => $ledger->ledger_reference ?? '',
                'product_reference' => $ledger->object_reference ?? '',
                'product_name' => $ledger->object_name ?? '',
                'imei' => $request->imei ?? 0,
                'unit_price' => $ledger->unit_price ?? 0,
                'dealer_price' => $ledger->dealer_price ?? 0,
                'stock_status' => 'stock',
                'auth_id' => Auth::id(),
            ]);

            //increment in product and ledger table;
            DB::table('products')->where('product_reference', '=', $ledger->object_reference)->increment('stock_qty');
            DB::table('ledger')->where('ledger_reference', '=', $request->purchase)->increment('stock_added');

            DB::commit();
            return response()->json('Created', 201);

        }catch (\Exception $exception){
            DB::rollBack();
            return response()->json($exception, 500);
        }
    }

    public function update(Request $request){

        $validator = Validator::make($request->all(), [
            'stock_id' => 'required',
            'imei' => 'required',
            'purchase' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->getMessageBag()->first());
        }

        try {

            DB::beginTransaction();
            $ledger = DB::table('ledger')->where('ledger_reference', '=', $request->purchase)->first();
            if ($ledger == null) {
                return response()->json('Product unavailable', 404);
            }
            DB::table('stocks')
                ->where('id', '=', $request->stock_id)
                ->update([
                    'ledger_reference' => $ledger->ledger_reference ?? '',
                    'product_reference' => $ledger->object_reference ?? '',
                    'product_name' => $ledger->object_name ?? '',
                    'imei' => $request->imei ?? 0,
                ]);

            DB::commit();
            return response()->json('Updated', 202);

        }catch (\Exception $exception){
            DB::rollBack();
            return response()->json($exception, 500);
        }
    }

    public function all()
    {
        $stock = DB::table('stocks')
            ->where('stock_status', '=', 'stock')
            ->count();
        return response()->json($stock, 200);
    }
    public function group()
    {
        $stock = DB::table('stocks')
            ->where('stock_status', '=', 'stock')
            ->groupBy('product_name', 'product_reference')
            ->select( 'product_name', 'product_reference',
                DB::raw('COUNT(id) as qty')
            )
            ->orderBy('product_name', 'asc')
            ->get()
        ;
        return response()->json($stock, 200);
    }

}
