<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use App\Services\Generator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Distribution extends Controller
{
    public function issued(Request $request)
    {
        $data = DB::table('product_issue')
            ->where('dso_reference', '=', $request->dso_reference)
            ->where('imei', '=', $request->imei)
            ->where('status', '=', 'issued')
            ->first();
        return response()->json($data, 200);
    }
    public function issued_all()
    {
        $data = DB::table('product_issue')
            ->where('status', '=', 'issued')
            ->paginate(25);
        return response()->json($data, 200);
    }
    public function issued_dso(Request $request)
    {
        $list = DB::table('product_issue')
            ->where('dso_reference', '=', $request->dso_reference)
            ->where('status', '=', 'issued')
            ->get();
        return response()->json($list, 200);
    }
    public function issue(Request $request){

        $this->validate($request, [
            'date' => 'required',
            'products' => 'required',
            'dso_reference' => 'required'
        ]);

        try {
            DB::beginTransaction();

            $dso = DB::table('dsos')->where('dso_reference','=', $request->dso_reference)->first();
            if (!$dso) return response()->json('Unknown DSO');

            foreach ($request->products as $val) {

                $issued = DB::table('product_issue')->where('dso_reference', '=', $dso->dso_reference)->where('imei', '=', $val['imei'])->first();

                //If Product not issued
                if (!$issued){

                    $stock = DB::table('stocks')->where('imei', '=', $val['imei'])->where('stock_status', '=', 'sold')->where('stocks.stock_dso_reference', '=', $request->dso_reference)->first();
                    if ($stock == null) { return response()->json('Stock unavailable'); }

                    DB::table('product_issue')->insert([
                        'dso_reference' => $dso->dso_reference ?? '',
                        'dso_name' => $dso->dso_name ?? '',
                        'product_reference' => $stock->product_reference ?? '',
                        'product_name' => $stock->product_name ?? '',
                        'sku' => $stock->sku ?? '',
                        'imei' => $stock->imei ?? '',
                        'issue_date' => $request->date ?? Carbon::now()->format('Y-m-d'),
                        'status' => 'issued',
                        'auth_id' => Auth::id(),
                        'created_at' => Carbon::now()->format('Y-m-d'),
                    ]);

                }else {

                    DB::table('product_issue')
                        ->where('id', '=', $issued->id)
                        ->update([
                        'issue_date' => $request->date ?? Carbon::now()->format('Y-m-d'),
                        'status' => 'issued',
                        'auth_id' => Auth::id(),
                    ]);

                }

                DB::table('product_issue_followup')->insert([
                    'dso_reference' => $dso->dso_reference ?? '',
                    'dso_name' => $dso->dso_name ?? '',
                    'product_reference' => $stock->product_reference ?? '',
                    'product_name' => $stock->product_name ?? '',
                    'sku' => $stock->sku ?? '',
                    'imei' => $stock->imei ?? '',
                    'date' => $request->date ?? Carbon::now()->format('Y-m-d'),
                    'status' => 'issued',
                    'auth_id' => Auth::id(),
                    'created_at' => Carbon::now()->format('Y-m-d'),
                ]);

            }
            DB::commit();
            return response()->json('Created', 201);

        }catch (\Exception $exception){
            DB::rollBack();
            return response()->json($exception, 500);
        }
    }
    public function withdraw(Request $request){

        $this->validate($request, [
            'date' => 'required',
            'products' => 'required',
            'dso_reference' => 'required'
        ]);

        try {
            DB::beginTransaction();

            $dso = DB::table('dsos')->where('dso_reference','=', $request->dso_reference)->first();
            if (!$dso) return response()->json('Unknown DSO');

            foreach ($request->products as $val) {

                $issued = DB::table('product_issue')->where('dso_reference', '=', $dso->dso_reference)->where('imei', '=', $val['imei'])->first();

                //If Product not issued
                if ($issued){
                    $stock = DB::table('stocks')->where('imei', '=', $val['imei'])->where('stock_status', '=', 'sold')->where('stocks.stock_dso_reference', '=', $request->dso_reference)->first();
                    if ($stock == null) { return response()->json('Stock unavailable'); }

                    DB::table('product_issue')
                        ->where('id', '=', $issued->id)
                        ->update([
                        'withdraw_date' => $request->date ?? Carbon::now(),
                        'status' => 'withdrawn',
                        'auth_id' => Auth::id(),
                    ]);
                }

                DB::table('product_issue_followup')->insert([
                    'dso_reference' => $dso->dso_reference ?? '',
                    'dso_name' => $dso->dso_name ?? '',
                    'product_reference' => $stock->product_reference ?? '',
                    'product_name' => $stock->product_name ?? '',
                    'sku' => $stock->sku ?? '',
                    'imei' => $stock->imei ?? '',
                    'date' => $request->date ?? Carbon::now()->format('Y-m-d'),
                    'status' => 'withdrawn',
                    'auth_id' => Auth::id(),
                    'created_at' => Carbon::now()->format('Y-m-d'),
                ]);
            }
            DB::commit();
            return response()->json('Created', 201);

        }catch (\Exception $exception){
            DB::rollBack();
            return response()->json($exception, 500);
        }
    }
}
