<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\Generator;

class Suppliers extends Controller
{
    public function get()
    {
        $suppliers = DB::table('suppliers')->get();
        return response()->json($suppliers, 200);
    }
    public function show($supplier_reference)
    {
        $suppliers = DB::table('suppliers')->where('supplier_reference', '=', $supplier_reference)->first();
        return response()->json($suppliers, 200);
    }
    public function store(Request $request){
        $this->validate($request, [
            'supplier_name' => 'required'
        ]);

        try {
            $generate = new Generator();
            DB::beginTransaction();

            DB::table('suppliers')->insert([
                'supplier_reference' => $generate->reference() ?? '',
                'supplier_name' => $request->supplier_name ?? '',
                'supplier_phone' => $request->supplier_phone ?? '',
                'supplier_address' => $request->supplier_address ?? '',
                'auth_id' => Auth::id(),
            ]);
            DB::commit();
            return response()->json('Created', 201);

        }catch (\Exception $exception){
            DB::rollBack();
            return response()->json($exception, 500);
        }
    }
}
