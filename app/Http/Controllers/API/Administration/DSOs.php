<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\Generator;

class DSOs extends Controller
{
    public function get()
    {
        $dsos = DB::table('dsos')->orderByDesc('created_at')->get();
        return response()->json($dsos, 200);
    }
    public function show($dso_reference)
    {
        $dso = DB::table('dsos')->where('dso_reference', '=', $dso_reference)->first();
        return response()->json($dso, 200);
    }
    public function store(Request $request){
        $this->validate($request, [
            'consumer_name' => 'required'
        ]);

        try {
            $generate = new Generator();
            $reference = $generate->reference() ?? '';
            DB::beginTransaction();

            DB::table('dsos')->insert([
                'dso_reference' => $reference,
                'dso_name' => $request->consumer_name ?? '',
                'dso_phone' => $request->consumer_phone ?? '',
                'dso_address' => $request->consumer_address ?? '',
                'auth_id' => Auth::id(),
            ]);
            DB::commit();
            return response()->json($reference, 201);

        }catch (\Exception $exception){
            DB::rollBack();
            return response()->json($exception, 500);
        }
    }
}
