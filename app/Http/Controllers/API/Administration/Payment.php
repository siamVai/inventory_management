<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\Generator;
use Illuminate\Support\Facades\Validator;

class Payment extends Controller
{
    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'payment_to' => 'required',
            'object_reference' => 'required',
            'date' => 'required',
            'amount' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->getMessageBag()->first());
        }

        if ($request->amount < 1){
            return response()->json('Amount must be valid!');
        }
        if (!$request->object_reference){
            return response()->json('Object reference invalid!');
        }

        $amount = $request->amount ?? 0;
        $desc = $request->desc ?? 'No desc';

        try {

            $generate = new Generator();

            DB::beginTransaction();

            $object_type_id = null;
            if ($request->payment_to == 'supplier'){
                $object = DB::table('suppliers')->where('supplier_reference', '=', $request->object_reference)->first();
                if ($object == null){
                    return response()->json('Supplier unavailable');
                }
                $object_name = $object->supplier_name ?? '';
                $object_type_id = 1; // Supplier : object_types

            }else if ($request->payment_to == 'dealer'){
                $object = DB::table('dealers')->where('dealer_reference', '=', $request->object_reference)->first();
                if ($object == null){
                    return response()->json('Dealer unavailable');
                }
                $object_name = $object->dealer_name ?? '';
                $object_type_id = 2; // Dealer : object_types

            } else{
                return response()->json('To whom you are giving payment?');
            }

            //Supplier/Dealer End
            DB::table('ledger')->insert([
                'ledger_reference' => $generate->reference() ?? '',
                'ps_reference' => '',
                'object_type_id' => $object_type_id,
                'object_reference' => $request->object_reference ?? '',
                'object_name' => $object_name ?? '',
                'credit' => 0,
                'debit' => $request->amount ?? 0,
                'stock_out' => 0,
                'stock_in' => 0,
                'unit_price' => 0,
                'comment' => 'Paid '.$amount.'tk to '.$object_name. ' ('. $desc.') ' ,
                'date' => $request->date ?? Carbon::now()->format('Y-m-d'),
                'action_name' => 'payment',
                'auth_id' => Auth::id(),
            ]);

            DB::commit();
            return response()->json('Created', 201);

        }catch (\Exception $exception){
            DB::rollBack();
            return response()->json($exception);
        }
    }
}
