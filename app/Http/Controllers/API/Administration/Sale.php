<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\Generator;

class Sale extends Controller
{
    public function get()
    {
        $sale = DB::table('ledger')
            ->join('object_types', 'object_types.object_type_name', '=', 'ledger.object_type')
            ->where('object_types.product', '=', 1) // if Product
            ->select('ledger.*')
            ->orderByDesc('created_at')
            ->get();
        return response()->json($sale, 200);
    }

    public function total_sold()
    {
        $sale = DB::table('ledger')
            ->join('object_types', 'object_types.id', '=', 'ledger.object_type_id')
            ->where('object_types.product', '=', 1) // if Product
            ->where('ledger.action_name', '=', 'sale') // if product purchased
            ->sum('credit');
        return response()->json($sale, 200);
    }

    public function store(Request $request){
        $this->validate($request, [
            'object' => 'required',
            'object_type' => 'required',
            'products' => 'required',
            'date' => 'required',
            'subtotal' => 'required',
        ]);

        //return $request->all();

        try {

            $generate = new Generator();
            $ps_reference = $generate->reference();

            DB::beginTransaction();

            $object = null;
            if ($request->object_type == 2){ // retailer
                $object = DB::table('dealers')->where('dealer_reference', '=', $request->object)
                    ->select('dealer_reference as reference', 'dealer_name as name')
                    ->first();
                if ($object == null){
                    return response()->json('Dealer unavailable');
                }
            }elseif ($request->object_type == 3){ // customer
                $object = DB::table('customers')->where('customer_reference', '=', $request->object)
                    ->select('customer_reference as reference', 'customer_name as name')
                    ->first();
                if ($object == null){
                    return response()->json('Customer unavailable');
                }
            }elseif ($request->object_type == 4){ // DSO
                $object = DB::table('dsos')->where('dso_reference', '=', $request->object)
                    ->select('dso_reference as reference', 'dso_name as name')
                    ->first();
                if ($object == null){
                    return response()->json('DSO unavailable');
                }
            }

            $total_qty = 0;
            $subtotal = $request->subtotal ?? 0;

            //Product End
            foreach ($request->products as $val){

                $total_qty += $val['qty'] ?? 0;

                $stock = DB::table('stocks')->where('imei', '=', $val['imei'])->where('stock_status', '=', 'stock')->first();
                if ($stock == null){
                    return response()->json('Stock unavailable');
                }

                $product = DB::table('products')->where('product_reference', '=', $stock->product_reference)->first();
                if ($product == null){
                    return response()->json('Product unavailable');
                }

                $product_comment = 'Sold to '.$object->name.' '.$val['imei'].' ('. 'Qty '.$val['qty'].'; Price '. $val['price']. 'Tk)';

                DB::table('ledger')->insert([
                    'ledger_reference' => $generate->reference() ?? '',
                    'ps_reference' => $ps_reference,
                    'object_type_id' => 101, //Mobile Phone
                    'object_reference' => $product->product_reference ?? '',
                    'object_name' => $product->product_name ?? '',

                    'sku' => $stock->sku ?? '',
                    'imei' => $val['imei'] ?? '',

                    'credit' => $val['price'] ?? 0,
                    'debit' => 0,
                    'stock_out' => $val['qty'] ?? 0,
                    'stock_in' => 0,
                    'unit_price' => $val['price'] ?? 0,
                    'comment' => $product_comment,
                    'consumer_reference' => $object->reference ?? '',
                    'consumer_name' => $object->name ?? '',
                    'date' => $request->date ?? Carbon::now()->format('Y-m-d'),
                    'action_name' => 'sale',
                    'auth_id' => Auth::id(),
                ]);

                $stock_update = [];
                $stock_update['stock_status'] = 'sold';

                //If DSO
                if ($request->object_type == 4){
                    $stock_update['stock_dso_reference'] = $object->reference ?? '';
                    $stock_update['stock_dso_name'] = $object->name ?? '';
                }

                DB::table('stocks')->where('id', '=', $stock->id)
                    ->update($stock_update);

                //Decrement for updating current stock
                DB::table('products')->where('product_reference', '=', $product->product_reference)->decrement('stock_qty');

            }

            //Customer/DSO/Retailer End
            $desc = $request->desc ?? '';
            $description = $desc ? '('.$desc.')' : '';
            $object_ledger_reference = $generate->reference() ?? '';
            DB::table('ledger')->insert([
                'ledger_reference' => $object_ledger_reference ?? '',
                'ps_reference' => $ps_reference,
                'object_type_id' => $request->object_type ?? 0, // Customer/Retailer/DSO
                'object_reference' => $object->reference ?? '',
                'object_name' => $object->name ?? '',
                'credit' => 0,
                'debit' => $subtotal ?? 0,
                'stock_out' => 0,
                'stock_in' => 0,
                'unit_price' => 0,
                'comment' => 'Qty '.$total_qty.'; Price '. $subtotal. 'Tk '.$description,
                'date' => $request->date ?? Carbon::now()->format('Y-m-d'),
                'action_name' => 'sale',
                'auth_id' => Auth::id(),
            ]);

            //If got payment
            if ($request->paid > 0){
                DB::table('ledger')->insert([
                    'ledger_reference' => $generate->reference() ?? '',
                    'ps_reference' => $ps_reference, //No due : customer payments when they bought product
                    'object_type_id' => $request->object_type ?? 0,
                    'object_reference' => $object->reference ?? '',
                    'object_name' => $object->name ?? '',
                    'credit' => $request->paid ?? 0,
                    'debit' =>  0,
                    'stock_out' => 0,
                    'stock_in' => 0,
                    'unit_price' => 0,
                    'comment' => 'Received '. $subtotal .'tk from '. $object->name,
                    'date' => $request->date ?? Carbon::now()->format('Y-m-d'),
                    'action_name' => 'receive',
                    'auth_id' => Auth::id(),
                ]);
            }

            //Invoice
            $invoice_reference = $generate->reference() ?? '';
            $invoice_id = $generate->invoice() ?? '';
            DB::table('invoice')->insert([
                'invoice_reference' => $invoice_reference ?? '',
                'ps_reference' => $ps_reference ?? '',
                'invoice_id' => $invoice_id ?? '',
                'consumer_reference' => $object->reference ?? '',
                'consumer_name' => $object->name ?? '',
                'object_type_id' => $request->object_type ?? 0, // Customer/Retailer/DSO
                'ledger_reference' => $object_ledger_reference ?? '',
                'price' => $subtotal ?? 0,
                'comment' => 'Sale Qty '.$total_qty.'; Price '. $subtotal. 'Tk '.$description,
                'action_name' => 'sale',
                'auth_id' => Auth::id(),
                'created_at' => Carbon::now(),
            ]);

            DB::commit();
            return response()->json($invoice_id, 201);

        }catch (\Exception $exception){
            DB::rollBack();
            return response()->json($exception, 500);
        }
    }
}
