<?php

namespace App\Http\Controllers\API\Administration\Invoice;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\Generator;
use Illuminate\Support\Facades\Validator;

class Invoice extends Controller
{
    public function all(Request $request){
        $invoice = DB::table('invoice')
            ->where('invoice_id', 'LIKE', '%'.$request->invoice_id.'%')
            ->orderByDesc('id')
            ->paginate(25);
        return response()->json($invoice, 200);
    }

    public function get(Request $request){
        $invoice = DB::table('invoice')->where('invoice_id', '=', $request->invoice_id)->first();
        return response()->json($invoice, 200);
    }

    public function details(Request $request){
        $data = DB::table('ledger')->where('ps_reference', '=', $request->ps_reference)->where('object_type_id', '>', 100)->get();
        return response()->json($data, 200);
    }
}
