<?php

namespace App\Http\Controllers\API\Administration\Reports\PayableReceivable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PayableReceivable extends Controller
{
    public function get(Request $request)
    {
        $rep = DB::table('ledger')
            ->whereBetween('object_type_id', [1,4])
            ->select('object_reference', 'object_name', 'object_type_id', DB::raw('SUM(credit - debit) as amount'))
            ->groupBy('object_reference', 'object_name')
            ->paginate($request->paginate);
        return response()->json($rep, 200);
    }
}
