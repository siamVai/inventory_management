<?php

namespace App\Http\Controllers\API\Administration\Reports\Sale;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Sale extends Controller
{
    public function get()
    {
        $sale = DB::table('ledger')
            ->where('action_name','=','sale')
            ->where('object_type_id', '<', 100) // Product
            ->get();
        return response()->json($sale, 200);
    }
}
