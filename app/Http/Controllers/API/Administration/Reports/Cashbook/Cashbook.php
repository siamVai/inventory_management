<?php

namespace App\Http\Controllers\API\Administration\Reports\Cashbook;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Cashbook extends Controller
{
    public function get()
    {
        $cash = DB::table('ledger')
            ->whereIn('action_name', ['payment', 'receive']) //if payment or Receive it will go as cash
            ->get();
        return response()->json($cash, 200);
    }
}
