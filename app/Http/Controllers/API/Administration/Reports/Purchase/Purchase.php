<?php

namespace App\Http\Controllers\API\Administration\Reports\Purchase;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Purchase extends Controller
{
    public function get()
    {
        $purchase = DB::table('ledger')
            ->where('action_name', '=', 'purchase')
            ->where('object_type_id', '>', 100)
            ->get();
        return response()->json($purchase, 200);
    }
}
