<?php

namespace App\Http\Controllers\API\Administration\Reports\Stock;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Stock extends Controller
{
    public function get()
    {
        $q = DB::table('stocks')
            ->select('product_name',
                DB::raw('COUNT(*) as total_purchased'),
                DB::raw('SUM(CASE WHEN stock_status = "sold" THEN 1 else 0 END) as total_sold'),
                DB::raw('SUM(CASE WHEN stock_status = "stock" THEN 1 else 0 END) as total_stock'),
                DB::raw('SUM(CASE WHEN stock_status = "return_purchase" THEN 1 else 0 END) as total_returned')
            )
            ->groupBy('product_name')
            ->get();

        return response()->json($q, 200);
    }
}
