<?php

namespace App\Http\Controllers\API\Administration\Reports\Ledger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Retailer extends Controller
{
    public function get(Request $request)
    {
        $ledger = DB::table('ledger')
            ->where('object_reference', '=', $request->retailer_reference)
            ->get();
        return response()->json($ledger, 200);
    }
}
