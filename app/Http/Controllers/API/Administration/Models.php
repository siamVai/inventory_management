<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Models extends Controller
{
    public function get()
    {
        $models = DB::table('models')->get();
        return response()->json($models, 200);
    }
    public function store(Request $request){

        $this->validate($request, [
            'brand' => 'required',
            'name' => 'required'
        ]);

        try {
            DB::beginTransaction();

            $brand = DB::table('brands')->find($request->brand);
            if (!$brand) return response()->json('Unknown Brand');

            DB::table('models')->insert([
                'model_name' => $request->name ?? '',
                'brand_id' => $request->brand ?? '',
                'brand_name' => $brand->brand_name ?? '',
            ]);
            DB::commit();
            return response()->json('Created', 201);

        }catch (\Exception $exception){
            DB::rollBack();
            return response()->json($exception, 500);
        }
    }
}
