<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\Generator;

class Retailers extends Controller
{
    public function get()
    {
        $dealers = DB::table('dealers')->get();
        return response()->json($dealers, 200);
    }
    public function show($dealer_reference)
    {
        $dealer = DB::table('dealers')->where('dealer_reference', '=', $dealer_reference)->first();
        return response()->json($dealer, 200);
    }
    public function store(Request $request){
        $this->validate($request, [
            'consumer_name' => 'required'
        ]);

        try {
            $generate = new Generator();
            DB::beginTransaction();

            DB::table('dealers')->insert([
                'dealer_reference' => $generate->reference() ?? '',
                'dealer_name' => $request->consumer_name ?? '',
                'dealer_phone' => $request->consumer_phone ?? '',
                'dealer_address' => $request->consumer_address ?? '',
                'auth_id' => Auth::id(),
            ]);
            DB::commit();
            return response()->json('Created', 201);

        }catch (\Exception $exception){
            DB::rollBack();
            return response()->json($exception, 500);
        }
    }
}
