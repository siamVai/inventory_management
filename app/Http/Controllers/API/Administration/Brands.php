<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Brands extends Controller
{
    public function get()
    {
        $brands = DB::table('brands')->select('id as brand_id', 'brand_name')->get();
        return response()->json($brands, 200);
    }
    public function store(Request $request){
        return $request->all();
    }
}
