<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Objects extends Controller
{
    public function get()
    {
        $object_types = DB::table('object_type')->get();
        return response()->json($object_types, 200);
    }

    public function show_objects(Request $request)
    {

        if ($request->object_type_id == 3) {
            //if customer
            $customer = new Customers();
            return $customer->get();

        }elseif ($request->object_type_id == 2) {
            //if dealer
            $retailer = new Retailers();
            return $retailer->get();
        }elseif ($request->object_type_id == 4) {
            //if dealer
            $dso = new DSOs();
            return $dso->get();
        }

    }

}
