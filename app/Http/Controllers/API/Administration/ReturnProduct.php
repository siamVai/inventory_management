<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\Generator;

class ReturnProduct extends Controller
{
    public function get()
    {
        //
    }

    public function store_sale_return(Request $request){

        $this->validate($request, [
            'products' => 'required',
            'date' => 'required',
            'subtotal' => 'required',
        ]);

        //return $request->all();

        try {

            $generate = new Generator();
            $ps_reference = $generate->reference();

            DB::beginTransaction();

            //Product End
            foreach ($request->products as $val){

                $stock = DB::table('stocks')->where('imei', '=', $val['imei'])->where('stock_status', '=', 'sold')->first();
                if ($stock == null){
                    return response()->json('Device was not sold');
                }

                $ledger = DB::table('ledger')->where('sku', '=', $val['sku'])->where('action_name', '=', 'sale')->first();
                if ($ledger == null){
                    return response()->json('No sale history');
                }
                $comment = 'Return from '.$ledger->consumer_name.' - '.$val['imei'].' ( Price '. $val['price']. 'Tk)';

                $from = [
                    'consumer_reference' => $ledger->consumer_reference,
                    'consumer_name' => $ledger->consumer_name,
                ];
                $return_form = json_encode($from) ;

                DB::table('returns')->insert([
                    'ledger_reference' => $ledger->ledger_reference ?? '',
                    'product_reference' => $ledger->object_reference ?? '',
                    'sku' => $stock->sku ?? '',
                    'imei' => $val['imei'] ?? '',
                    'return_from' => $return_form,
                    'return_to' => null,
                    'credit' => 0,
                    'debit' => $val['returns'] ?? 0,
                    'comments' => $comment. ' ('.$request->desc.')',
                    'created_at' => $request->date ?? Carbon::now()->format('Y-m-d'),
                    'auth_id' => Auth::id(),
                ]);

                DB::table('ledger')->insert([
                    'ledger_reference' => $generate->reference() ?? '',
                    'ps_reference' => $ps_reference,
                    'object_type_id' => 101, //Mobile Phone
                    'object_reference' => $ledger->object_reference ?? '',
                    'object_name' => $ledger->object_name ?? '',

                    'sku' => $stock->sku ?? '',
                    'imei' => $val['imei'] ?? '',

                    'credit' => 0,
                    'debit' => $val['returns'] ?? 0,
                    'stock_out' => 0,
                    'stock_in' => 1, //1 product returned form customer/dealer
                    'unit_price' => $val['price'] ?? 0,
                    'comment' => 'Returned from '.$ledger->consumer_name.' ('.$val['imei'].')',
                    'consumer_reference' => $ledger->consumer_reference ?? '',
                    'consumer_name' => $ledger->consumer_name ?? '',
                    'date' => $request->date ?? Carbon::now()->format('Y-m-d'),
                    'action_name' => 'return',
                    'auth_id' => Auth::id(),
                ]);

                DB::table('stocks')->where('id', '=', $stock->id)
                    ->update([
                        'stock_status' => 'stock'
                    ]);

                DB::table('products')->where('product_reference', '=', $ledger->object_reference)->increment('stock_qty');

                //Customer/Dealer End
                DB::table('ledger')->insert([
                    'ledger_reference' => $generate->reference() ?? '',
                    'ps_reference' => $ps_reference,
                    'object_type_id' => 3, // Customer
                    'object_reference' => $ledger->consumer_reference ?? '',
                    'object_name' => $ledger->consumer_name ?? '',
                    'credit' => $val['returns'] ?? 0,
                    'debit' => 0,
                    'stock_out' => 0,
                    'stock_in' => 0,
                    'unit_price' => 0,
                    'comment' => 'Returned ('.$val['imei'].')',
                    'date' => $request->date ?? Carbon::now()->format('Y-m-d'),
                    'action_name' => 'return',
                    'auth_id' => Auth::id(),
                ]);

                //If amount returned
                if ($val['returns'] > 0){
                    DB::table('ledger')->insert([
                        'ledger_reference' => $generate->reference() ?? '',
                        'ps_reference' => $ps_reference, //No due : customer payments when they bought product
                        'object_type_id' => 3, //Customer
                        'object_reference' => $ledger->consumer_reference,
                        'object_name' => $ledger->consumer_name ?? '',
                        'credit' => 0,
                        'debit' =>  $val['returns'] ?? 0,
                        'stock_out' => 0,
                        'stock_in' => 0,
                        'unit_price' => 0,
                        'comment' => 'Returned cash for ('.$val['imei'].')',
                        'date' => $request->date ?? Carbon::now()->format('Y-m-d'),
                        'action_name' => 'payment',
                        'auth_id' => Auth::id(),
                    ]);
                }
            }

            DB::commit();
            return response()->json('Created', 201);

        }catch (\Exception $exception){
            DB::rollBack();
            return response()->json($exception, 500);
        }
    }
    public function store_purchase_return(Request $request){

        $this->validate($request, [
            'products' => 'required',
            'date' => 'required',
            'subtotal' => 'required',
        ]);

        try {

            $generate = new Generator();
            $ps_reference = $generate->reference();

            DB::beginTransaction();

            //Product End
            foreach ($request->products as $val){

                $stock = DB::table('stocks')->where('imei', '=', $val['imei'])->where('stock_status', '=', 'stock')->first();
                if ($stock == null){
                    return response()->json('Device in not in stock');
                }

                $ledger = DB::table('ledger')->where('ledger_reference', '=', $val['ledger_reference'])->first();
                if ($ledger == null){
                    return response()->json('No purchase history found');
                }
                $comment = 'Return to '.$ledger->consumer_name.' - '.$val['imei'].' ( Return amount '. $val['price']. 'Tk)';

                $to = [
                    'consumer_reference' => $ledger->consumer_reference,
                    'consumer_name' => $ledger->consumer_name,
                ];
                $return_to = json_encode($to) ;


                DB::table('returns')->insert([
                    'ledger_reference' => $ledger->ledger_reference ?? '',
                    'product_reference' => $ledger->object_reference ?? '',
                    'sku' => $stock->sku ?? '',
                    'imei' => $val['imei'] ?? '',
                    'return_from' => null,
                    'return_to' => $return_to, //return_to for purchased product return
                    'credit' => $val['price'] ?? 0,
                    'debit' => 0,
                    'comments' => $comment. ' ('.$request->desc.')',
                    'created_at' => $request->date ?? Carbon::now()->format('Y-m-d'),
                    'auth_id' => Auth::id(),
                ]);



                DB::table('ledger')->insert([
                    'ledger_reference' => $generate->reference() ?? '',
                    'ps_reference' => $ps_reference,
                    'object_type_id' => 101, //Mobile Phone
                    'object_reference' => $ledger->object_reference ?? '',
                    'object_name' => $ledger->object_name ?? '',

                    'sku' => $stock->sku ?? '',
                    'imei' => $val['imei'] ?? '',

                    'credit' => $val['price'] ?? 0,
                    'debit' => 0,
                    'stock_out' => 1,
                    'stock_in' => 0, //1 product returned to supplier
                    'unit_price' => $val['price'] ?? 0,
                    'comment' => 'Returned to '.$ledger->consumer_name.' ('.$val['imei'].')',
                    'consumer_reference' => $ledger->consumer_reference ?? '',
                    'consumer_name' => $ledger->consumer_name ?? '',
                    'date' => $request->date ?? Carbon::now()->format('Y-m-d'),
                    'action_name' => 'return',
                    'auth_id' => Auth::id(),
                ]);

                DB::table('stocks')->where('id', '=', $stock->id)
                    ->update([
                        'stock_status' => 'return_purchase'
                    ]);

                DB::table('products')->where('product_reference', '=', $ledger->object_reference)->decrement('stock_qty');

                //Customer/Dealer End
                DB::table('ledger')->insert([
                    'ledger_reference' => $generate->reference() ?? '',
                    'ps_reference' => $ps_reference,
                    'object_type_id' => 3, // Customer
                    'object_reference' => $ledger->consumer_reference ?? '',
                    'object_name' => $ledger->consumer_name ?? '',
                    'credit' => 0,
                    'debit' => $val['price'] ?? 0,
                    'stock_out' => 0,
                    'stock_in' => 0,
                    'unit_price' => 0,
                    'comment' => 'Returned ('.$val['imei'].')',
                    'date' => $request->date ?? Carbon::now()->format('Y-m-d'),
                    'action_name' => 'return',
                    'auth_id' => Auth::id(),
                ]);

                //If amount returned
                if ($val['returns'] > 0){
                    DB::table('ledger')->insert([
                        'ledger_reference' => $generate->reference() ?? '',
                        'ps_reference' => $ps_reference, //No due : customer payments when they bought product
                        'object_type_id' => 3, //Customer
                        'object_reference' => $ledger->consumer_reference,
                        'object_name' => $ledger->consumer_name ?? '',
                        'credit' => $val['returns'] ?? 0,
                        'debit' =>  0,
                        'stock_out' => 0,
                        'stock_in' => 0,
                        'unit_price' => 0,
                        'comment' => 'Returned cash for ('.$val['imei'].')',
                        'date' => $request->date ?? Carbon::now()->format('Y-m-d'),
                        'action_name' => 'payment',
                        'auth_id' => Auth::id(),
                    ]);
                }
            }

            DB::commit();
            return response()->json('Created', 201);

        }catch (\Exception $exception){
            DB::rollBack();
            return response()->json($exception, 500);
        }
    }

}
